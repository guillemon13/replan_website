package edu.upc.supersede.plan.generator.customsolution;

import org.uma.jmetal.problem.Problem;

public interface ProjectProblem extends Problem<ProjectSolution> {
	Double getLowerBound(int index);
	Double getUpperBound(int index);
}