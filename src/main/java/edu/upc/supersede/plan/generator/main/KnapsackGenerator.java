package edu.upc.supersede.plan.generator.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.uma.jmetal.algorithm.impl.AbstractEvolutionaryAlgorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.SinglePointCrossover;
import org.uma.jmetal.operator.impl.mutation.BitFlipMutation;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.BinarySolution;
import org.uma.jmetal.util.binarySet.BinarySet;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;

import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.plan.generator.algorithm.NSGAIICustomPopulation;

public class KnapsackGenerator {
	private List<Requirement> requirements;
	private List<Requirement> mustRequirements;
	private int limitTime;
	
	public KnapsackGenerator() {
		requirements = new ArrayList<Requirement>();
		mustRequirements = new ArrayList<Requirement>();
	}
	
	public void loadRequirements(List<Requirement> requirements) {
		this.requirements = requirements;
	}
	
	public void loadMustRequirements(List<Requirement> requirements) {
		this.mustRequirements = requirements;
	}

	public void setDeadline(Date deadline, int workingHours) {
		Date now = new Date(System.currentTimeMillis());
		long diffMillis = deadline.getTime() - now.getTime();
		
		int days = 0;
		
		if (diffMillis < 0) {
			this.limitTime = 1500;
		} else {
			this.limitTime = (int) diffMillis / (1000 * 3600);
			
			//For the computation...
			days = limitTime / 8;
		}
			
		//TODO Change it by computation.
		this.limitTime = 1500;
	}
	
	public List<Requirement> generate() {
		AbstractEvolutionaryAlgorithm<BinarySolution, List<BinarySolution>> algorithm;
	    CrossoverOperator<BinarySolution> crossover;
	    MutationOperator<BinarySolution> mutation;
	    SelectionOperator<List<BinarySolution>, BinarySolution> selection;
	    
	    Problem<BinarySolution> problem = new KnapsackProblem(requirements, mustRequirements, limitTime);
	    
	    double crossoverProbability = 0.9;
	    crossover = new SinglePointCrossover(crossoverProbability) ;

	    double mutationProbability = 1.0 / problem.getNumberOfVariables() ;
	    mutation = new BitFlipMutation(mutationProbability) ;

	    selection = new BinaryTournamentSelection<BinarySolution>(new RankingAndCrowdingDistanceComparator<BinarySolution>());
	    
	    NSGAIICustomPopulation<BinarySolution> nsgaII = new NSGAIICustomPopulation<BinarySolution>(problem, 100, 500,
    		      crossover, mutation,
    		      selection, new SequentialSolutionListEvaluator<BinarySolution>());
    	
	    nsgaII.setMustRequirements(mustRequirements);
	    		    
	    nsgaII.run();
	    
	    List<BinarySolution> solutions = nsgaII.getResult();
	    
	    return getAvailableRequirements(solutions.get(0));
	}

	public List<Requirement> getAvailableRequirements(BinarySolution solution) {
		BinarySet bs = solution.getVariableValue(0);
		
		List<Requirement> available = new ArrayList<Requirement>();
		
		for (Requirement req : requirements) {
			if (bs.get(req.getInternalId())) {
				available.add(req);
			}
		}
		
		return available;
	}
	
	// GETTERS
	public List<Requirement> getRequirements() {
		return this.requirements;
	}
	
}
