package edu.upc.supersede.plan.generator.util;

import java.util.Vector;

public class Node {
    private int             name; // node ID, started from 0 to n-1
    private Vector<Integer> preds; // predecessors (String)
    private Vector<Integer> neibs; // neighbors (String)
    private Vector<Integer> backs; // backward edges -node is end vertex (Integer)
    private Vector<Integer> fors; // forward edges -node is start vertex (Integer)
    private int             pNode; // previous node on the augmenting path
    private int             pEdge; // from which edge this node comes on the augmenting path
 
    public Node (int id)
    {
        name = id;
        backs = new Vector<Integer>();
        fors = new Vector<Integer>();
        pNode = -1;
        pEdge = -1;
    }

	public int getName() {
		return name;
	}

	public void setName(int name) {
		this.name = name;
	}

	public Vector<Integer> getPreds() {
		return preds;
	}

	public void setPreds(Vector<Integer> preds) {
		this.preds = preds;
	}

	public Vector<Integer> getNeibs() {
		return neibs;
	}

	public void setNeibs(Vector<Integer> neibs) {
		this.neibs = neibs;
	}

	public Vector<Integer> getBacks() {
		return backs;
	}

	public void setBacks(Vector<Integer> backs) {
		this.backs = backs;
	}

	public Vector<Integer> getFors() {
		return fors;
	}

	public void setFors(Vector<Integer> fors) {
		this.fors = fors;
	}

	public int getpNode() {
		return pNode;
	}

	public void setpNode(int pNode) {
		this.pNode = pNode;
	}

	public int getpEdge() {
		return pEdge;
	}

	public void setpEdge(int pEdge) {
		this.pEdge = pEdge;
	}

	public void addBacks(int node) {
		this.backs.add(node);
	}
	
	public void addFors(int node) {
		this.fors.add(node);
	}
}
