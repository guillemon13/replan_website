package edu.upc.supersede.plan.generator.tester.main;

import java.util.*;

import edu.upc.supersede.app.model.*;

public class ProblemInstanceTranslator {
	
	private Properties problemInstance;
	
	private Map<Integer, Skill> skills;
	private Map<Integer, Requirement> mapRequirements;
	
	public ProblemInstanceTranslator (Properties properties) {
		this.problemInstance = properties;
		this.skills = new HashMap<Integer, Skill>();
	}
	
	public Collection<Employee> getEmployees() {
		int sizeSkills = Integer.valueOf(problemInstance.get("skill.number").toString());
		generateSkills(sizeSkills);
		
		Enumeration<?> enumeration = problemInstance.propertyNames();
		
		Map<Integer, Employee> mapEmployees = new HashMap<Integer, Employee>();
		
		while (enumeration.hasMoreElements()) {
            String element = (String)enumeration.nextElement();
            
            if (!element.startsWith("employee")) continue;
            
            String[] parts = element.split("\\.");
            
            if (parts[1].equals("number")) continue;
            
            int id = Integer.parseInt(parts[1]);
            
            Employee e;

            if (mapEmployees.containsKey(id)) {
            	e = mapEmployees.get(id);
            } else {
            	e = new Employee(id);
            	e.setName("Employee " + id);
            	mapEmployees.put(id, e);
            }
            
            String parameter = parts[2];
            
            if (parameter.equals("skill") && !parts[3].equals("number")) {
            	e.addSkill(skills.get(Integer.valueOf(problemInstance.getProperty(element))));
            }
            
            problemInstance.remove(element);
        }
		
		return mapEmployees.values();
	}
	
	public Collection<Requirement> getTasks() {
		Enumeration<?> enumeration = problemInstance.propertyNames();
		
		this.mapRequirements = new HashMap<Integer, Requirement>();
		
		while (enumeration.hasMoreElements()) {
            String element = (String)enumeration.nextElement();
            
            if (!element.startsWith("task")) continue;
            
            String[] parts = element.split("\\.");
            
            if (parts[1].equals("number")) continue;
            
            int id = Integer.parseInt(parts[1]);
            
            Requirement tsk;

            if (mapRequirements.containsKey(id)) {
            	tsk = mapRequirements.get(id);
            } else {
            	tsk = new Requirement(id);
            	tsk.setName("Feature " + id);
            	mapRequirements.put(id, tsk);
            }
            
            String parameter = parts[2];
            
            if (parameter.equals("skill") && !parts[3].equals("number")) {
            	tsk.addSkill(skills.get(Integer.valueOf(problemInstance.getProperty(element))));
            } else if (parameter.equals("criteria")) {
            	tsk.setCriteria(Integer.valueOf(problemInstance.getProperty(element)));
            } else if (parameter.equals("cost")) {
            	tsk.setEffort(Double.valueOf(problemInstance.getProperty(element)));
            }

            problemInstance.remove(element);
        }
		
		generateArcs();
		
		return mapRequirements.values();
	}
	
	public void generateArcs() {
		Enumeration<?> enumeration = problemInstance.propertyNames();
		
		while (enumeration.hasMoreElements()) {
            String element = (String)enumeration.nextElement();
            
            if (!element.startsWith("graph")) continue;
            
            String[] parts = element.split("\\.");
            
            if (parts[2].equals("number")) continue;
            
            String[] arc = problemInstance.get(element).toString().split(" ");
            
//            mapRequirements.get(Integer.valueOf(arc[0]))
//            		.addSuccessor(mapRequirements.get(Integer.valueOf(arc[1])));
//            
            mapRequirements.get(Integer.valueOf(arc[1])).addPredecessor(mapRequirements.get(Integer.valueOf(arc[0])));
            problemInstance.remove(element);
        }		
	}
	
	public Collection<Skill> getSkills() {
		return skills.values();
	}
	
	
	private void generateSkills(int size) {
		for (int i = 0; i < size; i++) {
			Skill sk = new Skill("Skill " + i, "SK"+ i, "Employee Skill", 1000L);
			skills.put(i, sk);
		}
	}
}
