package edu.upc.supersede.plan.generator.priorprojectsolution;

import java.util.*;

import org.uma.jmetal.problem.IntegerProblem;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.solution.Solution;
import org.uma.jmetal.solution.impl.AbstractGenericSolution;
import org.uma.jmetal.solution.impl.DefaultIntegerSolution;

import edu.upc.supersede.app.model.*;
import edu.upc.supersede.app.model.Requirement.State;


public class PrioritizedProjectSolution extends
		AbstractGenericSolution<Integer, ProjectProblem> implements
		IntegerSolution {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Job> initialJobs;
	private List<Requirement> initialRequirements;
	
	private List<Job> jobs;
	private List<Employee> employees;
	private List<Requirement> requirements;

	/** Constructor */
	public PrioritizedProjectSolution(ProjectProblem problem,
			List<Requirement> requirements, List<Job> jobs,
			List<Employee> employees) {

		super(problem);

		overallConstraintViolationDegree = 0.0;
		numberOfViolatedConstraints = 0;
		
		this.initialRequirements = new ArrayList<Requirement>(requirements);
		this.initialJobs = new ArrayList<Job>(jobs);
		
		for (Requirement req: requirements) {
			if (req.getState().equals(State.IN_PROGRESS)) {
				this.initialJobs.addAll(req.getJobs());
			}
		}
		
		this.requirements = new ArrayList<Requirement>();
		this.jobs = new ArrayList<Job>();
		this.employees = new ArrayList<Employee>(employees);
		
		initializeData(this.initialRequirements, this.initialJobs);
		
		System.out.println("FINAL TEST FOREVER 2:");
		for (Requirement req: this.requirements) {
			if (req.getState().equals(State.IN_PROGRESS)) {
				System.out.println(req.getName());
				System.out.println("Job size: " + req.getJobs().size());
			}
		}
		
		for (Requirement req: requirements) {
			if (req.getState().equals(State.IN_PROGRESS)) {
				this.initialJobs.removeAll(req.getJobs());
			}
		}
		
		initializeIntegerVariables();
		initializeObjectiveValues();
	}

	/** Copy constructor */
	public PrioritizedProjectSolution(PrioritizedProjectSolution solution) {
		super(solution.problem);

		initialRequirements = solution.initialRequirements;
		initialJobs = solution.initialJobs;
		
		this.requirements = new ArrayList<Requirement>();
		this.jobs = new ArrayList<Job>();
		
		for (Requirement req: solution.getRequirements()) {
			if (req.getState().equals(State.IN_PROGRESS)) {
				this.initialJobs.addAll(req.getJobs());
			}
		}
		
		initializeData(initialRequirements, initialJobs);
		
		employees = solution.employees;
		
		for (Requirement req: solution.getRequirements()) {
			if (req.getState().equals(State.IN_PROGRESS)) {
				this.initialJobs.removeAll(req.getJobs());
			}
		}
		
		for (int i = 0; i < problem.getNumberOfVariables(); i++) {
			setVariableValue(i, solution.getVariableValue(i));
		}

		for (int i = 0; i < problem.getNumberOfObjectives(); i++) {
			setObjective(i, solution.getObjective(i));
		}
		
		overallConstraintViolationDegree = solution.overallConstraintViolationDegree;
		numberOfViolatedConstraints = solution.numberOfViolatedConstraints;
		attributes = new HashMap<Object, Object>(solution.attributes);
	}

	public List<Requirement> getRequirements() {
		return this.requirements;
	}

	public List<Job> getJobs() {
		return this.jobs;
	}

	public List<Employee> getEmployees() {
		return this.employees;
	}

	@Override
	public Integer getUpperBound(int index) {
		return problem.getUpperBound(index);
	}

	@Override
	public Integer getLowerBound(int index) {
		return problem.getLowerBound(index);
	}

	@Override
	public String getVariableValueString(int index) {
		return getVariableValue(index).toString();
	}

	private void initializeData(List<Requirement> requirements, List<Job> jobs) {
		Map<Integer, Requirement> mapReqs = new HashMap<Integer,Requirement>();
		Map<Integer, Job> mapJobs = new HashMap<Integer,Job>();
		
		for (Requirement req : requirements) {
			Requirement copiedReq = new Requirement(req);
			this.requirements.add(copiedReq);
			mapReqs.put(req.getInternalId(), copiedReq);
		}
		
		for (Requirement req : requirements) {
			for (Requirement succ : req.getSuccessors()) {
				mapReqs.get(req.getInternalId()).addSuccessor(mapReqs.get(succ.getInternalId()));
			}
			
			for (Requirement pred : req.getPredecessors()) {
				mapReqs.get(req.getInternalId()).addPredecessorRelation(mapReqs.get(pred.getInternalId()));
			}
		}
		
		for (Job job : jobs) {
			Job copiedJob = new Job(job);
			
			copiedJob.setRequirement(mapReqs.get(job.getRequirement().getInternalId()));
			mapReqs.get(job.getRequirement().getInternalId()).addJob(copiedJob);
			
			this.jobs.add(copiedJob);
			
			mapJobs.put(job.getInternalId(), copiedJob);
		}
		
		for (Job job : jobs) {
			for (Job succ : job.getSuccessors()) {
				mapJobs.get(job.getInternalId()).addSuccessor(mapJobs.get(succ.getInternalId()));
			}
			
			for (Job pred : job.getPredecessors()) {
				mapJobs.get(job.getInternalId()).addPredecessor(mapJobs.get(pred.getInternalId()));
			}
		}
		
	}
	
	private void initializeIntegerVariables() {
		for (int i = 0; i < problem.getNumberOfVariables(); i++) {
			Integer value = randomGenerator.nextInt(getLowerBound(i),
					getUpperBound(i));
			setVariableValue(i, value);
		}
	}

	@Override
	public PrioritizedProjectSolution copy() {
		return new PrioritizedProjectSolution(this);
	}
}
