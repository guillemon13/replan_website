package edu.upc.supersede.plan.generator.tester.main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import edu.upc.supersede.app.model.*;
import edu.upc.supersede.plan.generator.main.WebLoader;


public class ATOSUseCase {
	public static WebLoader loadReqs() throws Exception {
		Skill PL = new Skill("Planning", "PL", "Planning", 1000L);
		Skill AR = new Skill("Architecture", "AR", "Architecture", 1000L);
		Skill DE = new Skill("Design", "DE", "Design", 1000L);
		Skill DVF = new Skill("Development Frontend", "DVF", "Development Frontend", 1000L);
		Skill DVB = new Skill("Development Backend", "DVB", "Development Backend", 1000L);
		Skill DVT = new Skill("Development Testing", "DVT", "Development Testing", 1000L);
		Skill DA = new Skill("Data Analytics", "DA", "Data Analytics", 1000L);
		Skill DB = new Skill("Databases", "DB", "Databases", 1000L);
		Skill SC = new Skill("Security", "SC", "Security", 1000L);
		
		SimpleDateFormat sdf = new SimpleDateFormat();
		
		Date M48 = sdf.parse("23/03/2016");
		Date ASAP = sdf.parse("23/03/2016");
		
		double bin = 3;
		
		Requirement u11 = new Requirement("Check visualization issues from Twitter influencer users",
				1 * bin, 2, null, 1, 1000L);
		u11.addSkill(DVF);
		u11.setDeadline(M48);
		
		Requirement u12 = new Requirement("Check iphone video quality for MX users",
				2 * bin, 2, null, 1, 1000L);
		u12.addSkill(DVF);
		u12.addSkill(DVB);
		u12.setDeadline(M48);
		
		Requirement u13 = new Requirement("Check piracy from NZ",
				1 * bin, 1, null, 1, 1000L);
		u13.addSkill(DVB);
		u13.setDeadline(ASAP);
		
		Requirement u15 = new Requirement("Check marketplace trends",
				1 * bin, 2, null, 1, 1000L);
		u15.addSkill(DA);
		u15.setDeadline(M48);
		
		Requirement u16 = new Requirement("Adjust Cloud Settings",
				2 * bin, 1, null, 1, 1000L);
		u16.addSkill(DVB);
		u16.setDeadline(ASAP);
		
		Requirement u17 = new Requirement("Fix web vulnerability",
				2 * bin, 1, null, 1, 1000L);
		u17.addSkill(SC);
		u17.setDeadline(ASAP);
		
		Requirement u18 = new Requirement("Update Player",
				1 * bin, 2, null, 1, 1000L);
		u18.setDeadline(ASAP);
		u18.addSkill(DVF);
		
		Requirement u19 = new Requirement("Add IPs to blacklist file",
				1 * bin, 1, null, 1, 1000L);
		u19.setDeadline(ASAP);
		u19.addSkill(DVB);
		u19.addSkill(SC);
		
		Requirement u110 = new Requirement("Review target encoding profiles in a specific geografical area.",
				2 * bin, 2, null, 1, 1000L);
		u110.setDeadline(M48);
		u110.addSkill(DVB);
		
		
		Employee david = new Employee("David Salama", 20, 1000L);
		david.addSkill(PL);
		david.addSkill(AR);
		david.addSkill(DE);
		david.addSkill(SC);
		
		Employee wenceslao = new Employee("Wenceslao", 10, 1000L);
		wenceslao.addSkill(PL);
		
		Employee antonio = new Employee("Antonio Gomez", 36, 1000L);
		antonio.addSkill(AR);
		antonio.addSkill(SC);
		antonio.addSkill(DE);

		Employee angel = new Employee("Angel Marcos Utset", 40, 1000L);
		angel.addSkill(DVF);
		angel.addSkill(DVB);
		angel.addSkill(DVT);
		
		Employee jose = new Employee("Jose Miguel Garrido", 32, 1000L);
		jose.addSkill(DVF);
		jose.addSkill(DVB);
		jose.addSkill(DVT);
		
		Employee oscar = new Employee("Oscar Alvarez Gil", 40, 1000L);
		oscar.addSkill(DVB);
		oscar.addSkill(DVT);
		
		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { u11, u12, u13, u15, u16, u17,
						u18, u19, u110 }));
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { david, wenceslao, antonio, angel, jose, oscar }));

		WebLoader loader = new WebLoader(1);
		loader.createEmployees(employees);
		loader.createFeatures(reqs);
		loader.createJobs();

		int i = 0;
		for (Requirement req : reqs) {
			req.setInternalId(i);
			i++;
		}

		return loader;
	}
}
