package edu.upc.supersede.plan.generator.customsolution;

import java.util.List;

import org.uma.jmetal.problem.impl.AbstractGenericProblem;
import edu.upc.supersede.app.model.*;

public abstract class AbstractProjectProblem extends
		AbstractGenericProblem<ProjectSolution> implements ProjectProblem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Double> lowerLimit;
	private List<Double> upperLimit;
	
	private List<Requirement> requirements;
	private List<Job> jobs;
	private List<Employee> employees;
	
	/* Getters */
	@Override
	public Double getUpperBound(int index) {
		return upperLimit.get(index);
	}

	@Override
	public Double getLowerBound(int index) {
		return lowerLimit.get(index);
	}

	/* Setters */
	protected void setLowerLimit(List<Double> lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	protected void setUpperLimit(List<Double> upperLimit) {
		this.upperLimit = upperLimit;
	}

	protected void setRequirements(List<Requirement> reqs) {
		this.requirements = reqs;
	}
	
	protected void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}
	
	protected void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	@Override
	public ProjectSolution createSolution() {	
		return new ProjectSolution(this, requirements, jobs, employees);
	}
}
