package edu.upc.supersede.plan.generator.priorprojectsolution;

import org.uma.jmetal.problem.Problem;

public interface ProjectProblem extends Problem<PrioritizedProjectSolution> {
	Integer getLowerBound(int index);
	Integer getUpperBound(int index);
}