package edu.upc.supersede.plan.generator.util;

import java.util.Comparator;

import edu.upc.supersede.app.model.Requirement;


public class RequirementPriorityComparator implements Comparator<Requirement>{

	@Override
	public int compare(Requirement o1, Requirement o2) {
        int criteriaResult = Integer.valueOf(o1.getCriteria())
        		.compareTo(Integer.valueOf(o2.getCriteria()));
        
        if (criteriaResult == 0) {
            // Priorities are Equal. Sort whether they have successors or not.
        	return Boolean.valueOf(o1.getSuccessors().isEmpty()).compareTo(o2.getSuccessors().isEmpty());
        }
        else {
            return criteriaResult;
        }
	}

}
