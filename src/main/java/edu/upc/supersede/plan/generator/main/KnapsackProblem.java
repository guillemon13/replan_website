package edu.upc.supersede.plan.generator.main;

import java.util.*;

import org.uma.jmetal.problem.ConstrainedProblem;
import org.uma.jmetal.problem.impl.AbstractBinaryProblem;
import org.uma.jmetal.solution.BinarySolution;
import org.uma.jmetal.util.JMetalException;
import org.uma.jmetal.util.binarySet.BinarySet;
import org.uma.jmetal.util.solutionattribute.impl.NumberOfViolatedConstraints;
import org.uma.jmetal.util.solutionattribute.impl.OverallConstraintViolation;

import edu.upc.supersede.app.model.Requirement;

public class KnapsackProblem extends AbstractBinaryProblem implements ConstrainedProblem<BinarySolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OverallConstraintViolation<BinarySolution> overallConstraintViolationDegree;
	public NumberOfViolatedConstraints<BinarySolution> numberOfViolatedConstraints;

	private int[] bitsPerVariable;

	private int limitTime;
	
	private List<Requirement> reqs;
	private List<Requirement> mustReqs;
	
	public KnapsackProblem(List<Requirement> reqs, List<Requirement> mustReqs, int deadline) {
		setName("THEME_EVOLVE");
		
		this.reqs = reqs;
		this.mustReqs = mustReqs;
		this.limitTime = deadline;
		
		overallConstraintViolationDegree = new OverallConstraintViolation<BinarySolution>();
		numberOfViolatedConstraints = new NumberOfViolatedConstraints<BinarySolution>();

		setNumberOfVariables(1);
		setNumberOfObjectives(1);
		
		int nConstraints = 0;
		for (Requirement req : reqs) {
			nConstraints += req.getSuccessors().size();
		}
		
		setNumberOfConstraints(1 + nConstraints + mustReqs.size());
		
		//Solutions = binary
		bitsPerVariable = new int[1];
		bitsPerVariable[0] = reqs.size();
	}

	public void evaluate(BinarySolution solution) {
		int[] fx = new int[solution.getNumberOfObjectives()];
		boolean[] x = new boolean[reqs.size()];

		// Maximization problem: multiply by -1 to minimize;
		for (Requirement req : reqs) {
			x[req.getInternalId()] = solution.getVariableValue(0).get(req.getInternalId());
		}
		
		// Express evaluation functions.
		for (Requirement req : reqs) {
			fx[0] += x[req.getInternalId()] ? req.getCriteria() : 0 ;
		}

		for (int i = 0; i < solution.getNumberOfObjectives(); i++) {
			solution.setObjective(i, -1.0*fx[i]);
		}
	}

	public void evaluateConstraints(BinarySolution solution) {
		boolean[] x = new boolean[reqs.size()];
		double[] constraint = new double[this.getNumberOfConstraints()];
		int numConstraint = 0;
		
		//Counting solution variables.
		for (Requirement req : reqs) {
			x[req.getInternalId()] = solution.getVariableValue(0).get(req.getInternalId());
		}
		
		int sumEfforts = 0;
		for (Requirement req : reqs) {
			sumEfforts += x[req.getInternalId()] ? req.getEffort() : 0;
		}
		
		constraint[numConstraint] = limitTime - sumEfforts;
		numConstraint++;
		
		for (Requirement req : reqs) {
			for (Requirement successor : req.getSuccessors()) {
				int successorValue = x[successor.getInternalId()] ? 1 : 0;
				int reqValue = x[req.getInternalId()] ? 1 : 0;
				constraint[numConstraint] = -(successorValue - reqValue);
				numConstraint++;
			}
		}

		//Re-Plan idea -> Force a requirement to be present in the next release.
		for (Requirement req : mustReqs) {
			if (!x[req.getInternalId()]) {
				constraint[numConstraint] = -req.getCriteria();
			}
			numConstraint++;
		}
		
		
		double overallConstraintViolation = 0.0;
		int violatedConstraints = 0;
		for (int i = 0; i < getNumberOfConstraints(); i++) {
			if (constraint[i] < 0.0) {
				overallConstraintViolation += constraint[i];
				violatedConstraints++;
			}
		}
		
		overallConstraintViolationDegree.setAttribute(solution, (double) overallConstraintViolation);
		numberOfViolatedConstraints.setAttribute(solution, violatedConstraints);
	}
	
	
	@Override
	protected int getBitsPerVariable(int index) {
		if ((index < 0) || (index >= this.getNumberOfVariables())) {
			throw new JMetalException("Index value is incorrect: " + index);
		}
		return bitsPerVariable[index];
	}
}
