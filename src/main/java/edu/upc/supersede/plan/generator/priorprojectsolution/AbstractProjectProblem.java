package edu.upc.supersede.plan.generator.priorprojectsolution;

import java.util.List;

import org.uma.jmetal.problem.impl.AbstractGenericProblem;

import edu.upc.supersede.app.model.*;

public abstract class AbstractProjectProblem extends
		AbstractGenericProblem<PrioritizedProjectSolution> implements ProjectProblem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Integer> lowerLimit;
	private List<Integer> upperLimit;
	
	private List<Requirement> requirements;
	private List<Job> jobs;
	private List<Employee> employees;
	
	/* Getters */
	@Override
	public Integer getUpperBound(int index) {
		return upperLimit.get(index);
	}

	@Override
	public Integer getLowerBound(int index) {
		return lowerLimit.get(index);
	}

	/* Setters */
	protected void setLowerLimit(List<Integer> lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	protected void setUpperLimit(List<Integer> upperLimit) {
		this.upperLimit = upperLimit;
	}

	protected void setRequirements(List<Requirement> reqs) {
		this.requirements = reqs;
	}
	
	protected void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}
	
	protected void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	@Override
	public PrioritizedProjectSolution createSolution() {	
		return new PrioritizedProjectSolution(this, requirements, jobs, employees);
	}
}
