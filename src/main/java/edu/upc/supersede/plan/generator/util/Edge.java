package edu.upc.supersede.plan.generator.util;

public class Edge {

    private int name;    // edge ID, started from 0 to n-1
    private int start;   // start vertex of this edge
    private int end;     // end vertex of this edge
    private int direct;  // forwards (+1) or backwards (-1) on augmenting path
                  // if 0 then not part of augmenting path
    private double capacity; // capacity
    private int flow;    // current flow
 
    public Edge(int id)
    {
        name = id;
        start = -1;
        end = -1;
        direct = 0; // default is neither
        capacity = 0;
        flow = 0;
    }
 
    public int getName() {
		return name;
	}

	public void setName(int name) {
		this.name = name;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getDirect() {
		return direct;
	}

	public void setDirect(int direct) {
		this.direct = direct;
	}

	public double getCapacity() {
		return capacity;
	}

	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}
 
	public int getFlow() {
		return flow;
	}

	public void setFlow(int flow) {
		this.flow = flow;
	}

	public String toString()
    {
        return name + ": s=" + start + " e=" + end + " d=" + direct;
    }
}
