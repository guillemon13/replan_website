package edu.upc.supersede.plan.generator.util;

import java.util.Comparator;
import edu.upc.supersede.app.model.Job;

public class JobComparator implements Comparator<Job>{

    public int compare(Job myObj1, Job myObj2) {
        
        int criteriaResult = Integer.valueOf(myObj1.getRequirement().getCriteria())
        		.compareTo(Integer.valueOf(myObj2.getRequirement().getCriteria()));
        
        if (criteriaResult == 0) {
            // Priorities are Equal. Sort whether they have predecessors or not.
            
        	return Boolean.valueOf(myObj1.getPredecessors().isEmpty()).compareTo(myObj2.getPredecessors().isEmpty());
        }
        else {
            return criteriaResult;
        }
    }
}