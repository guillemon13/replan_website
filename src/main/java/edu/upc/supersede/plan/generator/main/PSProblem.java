package edu.upc.supersede.plan.generator.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.uma.jmetal.problem.ConstrainedProblem;
import org.uma.jmetal.problem.impl.AbstractDoubleProblem;
import org.uma.jmetal.problem.impl.AbstractDoubleProblem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.JMetalException;
import org.uma.jmetal.util.solutionattribute.impl.NumberOfViolatedConstraints;
import org.uma.jmetal.util.solutionattribute.impl.OverallConstraintViolation;

import edu.upc.supersede.app.model.*;
import edu.upc.supersede.app.model.Requirement.State;
import edu.upc.supersede.plan.generator.customsolution.AbstractProjectProblem;
import edu.upc.supersede.plan.generator.customsolution.ProjectSolution;
import edu.upc.supersede.plan.generator.util.JobComparator;
import edu.upc.supersede.plan.generator.util.RequirementPriorityComparator;


public class PSProblem extends AbstractProjectProblem implements
		ConstrainedProblem<ProjectSolution> {

	private static final long serialVersionUID = 1L;

	public OverallConstraintViolation<ProjectSolution> overallConstraintViolationDegree;
	public NumberOfViolatedConstraints<ProjectSolution> numberOfViolatedConstraints;

	private Date deadline;
	private int initTime;
	private Date initDate;
	
	public PSProblem(KnapsackLoader loader) {
		setName("THEME_EVOLVE");

		this.setRequirements(loader.getRequirements());
		this.setJobs(loader.getJobs());
		this.setEmployees(loader.getResources());

		this.deadline = loader.getDeadline();
		this.initTime = loader.getInitTime();
		this.initDate = loader.getStartDate();
		
		overallConstraintViolationDegree = new OverallConstraintViolation<ProjectSolution>();
		numberOfViolatedConstraints = new NumberOfViolatedConstraints<ProjectSolution>();

		setNumberOfVariables(loader.getJobs().size());
		setNumberOfObjectives(3);
		setNumberOfConstraints(1);

		List<Double> upperLimit = new ArrayList<Double>();
		List<Double> lowerLimit = new ArrayList<Double>();

		for (int i = 0; i < getNumberOfVariables(); i++) {
			lowerLimit.add(0.0);
			upperLimit.add(1.0);
		}

		this.setUpperLimit(upperLimit);
		this.setLowerLimit(lowerLimit);
	}

	public void evaluate(ProjectSolution solution)  {
		
		List<Requirement> allTasks = solution.getRequirements();
		
		List<Requirement> inProgress = new ArrayList<Requirement>(); 
		List<Requirement> tasks = new ArrayList<Requirement>();
		
		for (Requirement req : allTasks) {
			if (req.getState().equals(State.TO_DO)) {
				tasks.add(req);
			} else{
				inProgress.add(req);
			}
		}
		
		List<Job> jobs = solution.getJobs();
		List<Employee> resources = solution.getEmployees();

		double[] fx = new double[solution.getNumberOfObjectives()];
		double[][] x = new double[tasks.size()][resources.size()];

		double task_ahr[] = new double[tasks.size()];
		double task_dur[] = new double[tasks.size()];
		double originalEfforts[] = new double[tasks.size()];
		
		int count = 0;

		double[] employeeWorkload = new double[resources.size()];
		
		//tjAhr = \sum{i=1}{E}x_{ij}
		for (int i = 0; i < tasks.size(); i++) {
			for (int j = 0; j < resources.size(); j++) {
				if (tasks.get(i).fitsEmployee(resources.get(j))) {
					x[i][j] = solution.getVariableValue(count);
					task_ahr[i] += x[i][j];
					count++;
					
					//Assign dedication to job.
					tasks.get(i).getJob(resources.get(j)).setDedication(x[i][j]);
					
					employeeWorkload[j] += x[i][j];
				} else {
					x[i][j] = 0.0;
				}
			}
		}
			
		Requirement req;
		for (int j = 0; j < tasks.size(); j++) {
			req = tasks.get(j);
			
			originalEfforts[j] = req.getEffort();
			task_dur[j] = Math.ceil(req.getEffort() / task_ahr[j]);
			
			if (req.getEffort() != task_dur[j]) {		
				req.setEffort(task_dur[j]);
				
				for (Job job : req.getJobs()) {
					job.setDevTime(task_dur[j]);
					job.updateTime(initDate);
				}
				
				tasks.get(j).setStartDate(req.getJobs().get(0).getFromTime());
			}
		}

		checkRequirementsFeasibility(tasks);
		
		Collections.sort(jobs, new JobComparator());

		for (Job job : jobs) {
			if (job.getPredecessors().size() == 0) {
				job.setEarlyStart(0);
			}
		}

		//Express evaluation functions.
		//Minimize duration
		double projectDuration  = 0.0;
		for (int j = 0; j < tasks.size(); j++) {
			projectDuration = Math.max(projectDuration, tasks.get(j).getEndTime());

			fx[0] = projectDuration;
		}
		
		//Avoid overload.
		for (int t = initTime; t < fx[0]; t++) {
			
			int jEmployee = 0;
			for (Employee empl : resources) {
				double workload = 0.0;

				//compute workload
				List<Requirement> assignedReqs = new ArrayList<Requirement>();
				
				for (int iReq =0; iReq < tasks.size(); iReq++) {
					if (tasks.get(iReq).isRunning(t) && tasks.get(iReq).fitsEmployee(empl)) {
						workload += x[iReq][jEmployee];
						assignedReqs.add(tasks.get(iReq));
					}
				}

				//case there is workload > 1.
				if (workload > (empl.getEffort()/100)) {
					Collections.sort(assignedReqs, new RequirementPriorityComparator());

					assignedReqs.get(assignedReqs.size()-1);
					
					Requirement movedReq;
					
					int i = 0;
					do {
						movedReq = assignedReqs.get(i);
						i++;
					} while (!movedReq.getState().equals(State.TO_DO));

					//Remove the workload of the less priority task in TO_DO state.
					task_ahr[movedReq.getInternalId()] -= movedReq.getJob(empl).getDedication();
					x[movedReq.getInternalId()][jEmployee] -= movedReq.getJob(empl).getDedication();
					
					//Assign that solution variable to 0 ??(inform the scheduler?)
					//solution.setVariableValue(index, 0.0);
					
					//Update the new duration of the task.
					task_dur[movedReq.getInternalId()] = Math.ceil(originalEfforts[movedReq.getInternalId()] / task_ahr[movedReq.getInternalId()]);
					movedReq.setEffort(task_dur[movedReq.getInternalId()]);
					
					for (Job job : movedReq.getJobs()) {
						job.setDevTime(task_dur[movedReq.getInternalId()]);
						job.updateTime(initDate);
					}
					
					//employeeWorkload[jEmployee] -= movedReq.getJob(empl).getDedication();
				}

				jEmployee++;
			}
		}
		
		//Maximize criteria
		for (int i = 0; i < tasks.size(); i++) {
			for (int j = 0; j < resources.size(); j++) {
				fx[1] += tasks.get(i).getCriteria() * x[i][j];
			}
		}

		//Minimize highest load -> Homogeneity of resources.
		for (int i = 0; i < resources.size(); i++) {
			fx[2] = Math.max(fx[2], employeeWorkload[i]);
		}

		solution.setObjective(0, fx[0]);
		solution.setObjective(1, -1.0 * fx[1]);
		//solution.setObjective(2, fx[2]);
	}

	public void evaluateConstraints(ProjectSolution solution) {
		List<Requirement> tasks = solution.getRequirements();
		List<Employee> resources = solution.getEmployees();

		double[][] x = new double[tasks.size()][resources.size()];

		int overallConstraintViolation = 0;
		int violatedConstraints = 0;

		int count = 0;
		for (int i = 0; i < tasks.size(); i++) {
			for (int j = 0; j < resources.size(); j++) {
				if (tasks.get(i).fitsEmployee(resources.get(j))) {
					x[i][j] = solution.getVariableValue(count);
					count++;
				} else {
					x[i][j] = 0.0;
				}
			}
		}

		double constraint = 0.0;
		double task_ahr[] = new double[tasks.size()];

		//tjAhr = \sum{i=1}{E}x_{ij}
		for (int i = 0; i < tasks.size(); i++) {
			for (int j = 0; j < resources.size(); j++) {
				task_ahr[i] += x[i][j];
			}

			if (task_ahr[i] == 0.0)
				constraint -= resources.size();
		}

		if (constraint < 0) {
			violatedConstraints++;
			overallConstraintViolation += constraint;
		}

		constraint = 0;
		
		for (int i = 0; i < tasks.size(); i++) {
			double sum = 0;
			for (int j = 0; j < resources.size(); j++) {
				sum += x[i][j];
			}

			if (sum > 1.0) {
				constraint += (1.0 - sum);
			}
		}

		if (constraint < 0) {
			violatedConstraints++;
			overallConstraintViolation += constraint;
		}

		overallConstraintViolationDegree.setAttribute(solution, (double) overallConstraintViolation);
		numberOfViolatedConstraints.setAttribute(solution, violatedConstraints);
	}
	
	private void checkRequirementsFeasibility(List<Requirement> requirements) {
		//Remove non-affordable jobs.
		for (Requirement req : requirements) {
			if (req.getDeadline() != null && req.getDeadline().after(deadline)) {
				//Update predecessors not to link this non-affordable node.
				for (Requirement pred : req.getPredecessors()) {
					pred.removeSuccessor(req);
				}
	
				for (Job job : req.getJobs()) {
					for (Job pred : job.getPredecessors()) {
						pred.removeSuccessor(job);
					}
				}
				
				purgeSuccessors(req, requirements);
	
				requirements.remove(req);
			}
		}
	}

	private void purgeSuccessors(Requirement req, List<Requirement> requirements) {
		List<Requirement> predecessors;
		
		for (Requirement succ : req.getSuccessors()) {
			predecessors = succ.getPredecessors();
			
			for (int i = 0; i < predecessors.size(); i++) {
				if (!predecessors.get(i).equals(req)) {
					predecessors.get(i).removeSuccessor(succ);	
				}
			}
			
			purgeSuccessors(succ, requirements);
			
			requirements.remove(succ);
		}
	}
	
	/*private double workingFunction (double[][] x, int iEmpl, int instant) {
		double sum = 0;

		for (int i = 0; i < tasks.size(); i++) {
			Requirement req = tasks.get(i);
			if (req.fitsEmployee(resources.get(iEmpl)) && instant >= req.getStartTime() && instant <= req.getEndTime()) {
				sum += x[i][iEmpl];
			}
		}

		return sum;
	}*/
}
