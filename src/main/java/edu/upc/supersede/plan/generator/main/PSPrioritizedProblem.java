package edu.upc.supersede.plan.generator.main;

import java.util.*;

import org.uma.jmetal.problem.ConstrainedProblem;
import org.uma.jmetal.util.solutionattribute.impl.NumberOfViolatedConstraints;
import org.uma.jmetal.util.solutionattribute.impl.OverallConstraintViolation;

import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.Job;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.Requirement.State;
import edu.upc.supersede.plan.generator.priorprojectsolution.AbstractProjectProblem;
import edu.upc.supersede.plan.generator.priorprojectsolution.PrioritizedProjectSolution;
import edu.upc.supersede.plan.generator.util.RequirementPriorityComparator;


public class PSPrioritizedProblem extends AbstractProjectProblem implements
		ConstrainedProblem<PrioritizedProjectSolution> {

	private static final long serialVersionUID = 1L;

	public OverallConstraintViolation<PrioritizedProjectSolution> overallConstraintViolationDegree;
	public NumberOfViolatedConstraints<PrioritizedProjectSolution> numberOfViolatedConstraints;

	private Date deadline;
	private int initTime;
	private Date initDate;
	
	public PSPrioritizedProblem(KnapsackLoader loader) {
		setName("THEME_EVOLVE");

		this.setRequirements(loader.getRequirements());
		this.setJobs(loader.getJobs());
		this.setEmployees(loader.getResources());

		this.deadline = loader.getDeadline();
		this.initTime = loader.getInitTime();
		this.initDate = loader.getStartDate();
		
		overallConstraintViolationDegree = new OverallConstraintViolation<PrioritizedProjectSolution>();
		numberOfViolatedConstraints = new NumberOfViolatedConstraints<PrioritizedProjectSolution>();

		setNumberOfVariables(loader.getJobs().size());
		setNumberOfObjectives(3);
		setNumberOfConstraints(1);

		List<Integer> upperLimit = new ArrayList<Integer>();
		List<Integer> lowerLimit = new ArrayList<Integer>();

		for (int i = 0; i < getNumberOfVariables(); i++) {
			lowerLimit.add(0);
			upperLimit.add(1);
		}

		this.setUpperLimit(upperLimit);
		this.setLowerLimit(lowerLimit);
	}

	public void evaluate(PrioritizedProjectSolution solution)  {
		
		List<Requirement> allTasks = solution.getRequirements();
		
		List<Requirement> inProgress = new ArrayList<Requirement>(); 
		List<Requirement> tasks = new ArrayList<Requirement>();
		
		for (Requirement req : allTasks) {
			if (req.getState().equals(State.TO_DO)) {
				tasks.add(req);
			} else {
				for (Job job : req.getJobs()) {
					Employee jobEmployee = job.getEmployee();
					jobEmployee.addJob(job);
				}
				
				inProgress.add(req);
			}
		}
		
		List<Employee> resources = solution.getEmployees();

		int[] fx = new int[solution.getNumberOfObjectives()];
		int[][] x = new int[tasks.size()][resources.size()];

		int task_ahr[] = new int[tasks.size()];
		int task_dur[] = new int[tasks.size()];
		
		int count = 0;

		int[] employeeWorkload = new int[resources.size()];
		
		List<Employee> reqCandidates = new ArrayList<Employee>();
		
		Map<Long, Integer> dedications = new HashMap<Long, Integer>();
		
		//tjAhr = \sum{i=1}{E}x_{ij}
		for (int i = 0; i < tasks.size(); i++) {
			reqCandidates.clear();
			dedications.clear();
			
			int nEmployees = 0;
			
			for (int j = 0; j < resources.size(); j++) {
				if (tasks.get(i).fitsEmployee(resources.get(j))) {
					x[i][j] = solution.getVariableValue(count);
		
					dedications.put(resources.get(j).getId(), count);
					reqCandidates.add(resources.get(j));
					
					if (task_ahr[i] < tasks.get(i).getMaxEmployees()) {
						Job job = tasks.get(i).getJob(resources.get(j)); 
						job.setDedication(x[i][j]);
						
						resources.get(j).addJob(job);
						
						employeeWorkload[j] += x[i][j];
						task_ahr[i] += x[i][j];
					}
					
					count++;
					
				} else {
					x[i][j] = 0;
				}
			}
			
			//TODO: prioritize to low-charged priority employees.				
			//sort candidates by charge of priority.
//			Collections.sort(reqCandidates, new EmployeePriorityComparator());
//			
//			int k;
//			for (k = reqCandidates.size()-1; k >= 0; k--) {
//				
//				if (nEmployees <= tasks.get(i).getMaxEmployees()) {
//					tasks.get(i).getJob(reqCandidates.get(k)).setDedication(1.0);
//					nEmployees++;
//					
//					solution.setVariableValue(dedications.get(reqCandidates.get(k).getId()), 1);
//				} else {
//					tasks.get(i).getJob(reqCandidates.get(k)).setDedication(0.0);
//					solution.setVariableValue(dedications.get(reqCandidates.get(k).getId()), 0);
//				}
//			}
//			
//			task_ahr[i] = Math.min(task_ahr[i], tasks.get(i).getMaxEmployees());
		}
		
		Requirement req;
		for (int j = 0; j < tasks.size(); j++) {
			req = tasks.get(j);
			
			//Delete jobs as there are employees not allowed to perform a task.
			//req.purgeNonAssignedJobs();
			
			//originalEfforts[j] = (int)req.getEffort();
			
			//In case more than one user is performing the feature.
			if (task_ahr[j] > 0) {
				task_dur[j] = (int) Math.ceil(req.getEffort() / task_ahr[j]);
				
				if (req.getEffort() != task_dur[j]) {		
					req.setEffort(task_dur[j]);
					
					for (Job job : req.getJobs()) {
						job.setDevTime(task_dur[j]);
						job.updateTime(initDate);
					}
					
					tasks.get(j).setStartDate(req.getJobs().get(0).getFromTime());
				}
			} else task_dur[j] = 0;
		}

		//Delete tasks after deadline.
		//checkRequirementsFeasibility(tasks);


		//Express evaluation functions.
		//Minimize duration
		int projectDuration  = 0;
		for (int j = 0; j < tasks.size(); j++) {
			projectDuration = (int) Math.max(projectDuration, tasks.get(j).getEndTime());

			fx[0] = projectDuration;
		}
		
		//Avoid overload.
		for (int t = initTime; t < projectDuration; t++) {
			
			for (Employee empl : resources) {
				int workload = 0;

				//compute workload
				List<Requirement> assignedReqs = new ArrayList<Requirement>();
				
				for (Job job : empl.getJobs()) {
					if (job.isRunning(t)) {
						workload += job.getDedication();
						
						if (job.getDedication() > 0) {
							assignedReqs.add(job.getRequirement());
						}
					}
				}

				//case there is workload > 1.
				if (workload > (empl.getEffort()/100)) {
					Collections.sort(assignedReqs, new RequirementPriorityComparator());

					//Select the TO_DO task to postpose with lower priority.
					Requirement movedReq = null;
					
					do {
						int priority = Integer.MIN_VALUE;
						
						for (Requirement asReq : assignedReqs) {
							if (asReq.getState().equals(State.TO_DO)) {
								if (asReq.getCriteria() >= priority) {
									priority = asReq.getCriteria();
									movedReq = asReq;
								}
							}
						} 
	
						if (movedReq != null) {
							movedReq.postpose(assignedReqs.get(0).getEffort());
							projectDuration = (int) Math.max(projectDuration, movedReq.getEndTime());
							assignedReqs.remove(0);
						}
					
					} while (assignedReqs.size() > 1 && movedReq != null);
					
				}
			}
		}
		
		//Maximize criteria
		for (int i = 0; i < tasks.size(); i++) {
			for (int j = 0; j < resources.size(); j++) {
				fx[1] += tasks.get(i).getCriteria() * x[i][j];
			}
		}

//		//Minimize highest load -> Homogeneity of resources.
//		for (int i = 0; i < resources.size(); i++) {
//			fx[2] = Math.max(fx[2], employeeWorkload[i]);
//		}

		solution.setObjective(0, projectDuration);
		solution.setObjective(1, -1.0 * fx[1]);
		//solution.setObjective(2, fx[2]);
		
		for (int i = 0; i < resources.size(); i++) {
			resources.get(i).resetJobs();
		}
	}

	public void evaluateConstraints(PrioritizedProjectSolution solution) {
		List<Requirement> allTasks = solution.getRequirements();
		
		List<Requirement> tasks = new ArrayList<Requirement>();
		
		for (Requirement req : allTasks) {
			if (req.getState().equals(State.TO_DO)) {
				tasks.add(req);
			} 
		}
		
		List<Employee> resources = solution.getEmployees();

		int[][] x = new int[tasks.size()][resources.size()];

		int overallConstraintViolation = 0;
		int violatedConstraints = 0;

		int count = 0;
		for (int i = 0; i < tasks.size(); i++) {
			for (int j = 0; j < resources.size(); j++) {
				if (tasks.get(i).fitsEmployee(resources.get(j))) {
					x[i][j] = solution.getVariableValue(count);
					count++;
				} else {
					x[i][j] = 0;
				}
			}
		}

		int constraint = 0;
		int task_ahr[] = new int[tasks.size()];

		//tjAhr = \sum{i=1}{E}x_{ij}
		for (int i = 0; i < tasks.size(); i++) {
			for (int j = 0; j < resources.size(); j++) {
				task_ahr[i] += x[i][j];
			}

			if (task_ahr[i] == 0.0)
				constraint -= resources.size();
		}

		if (constraint < 0) {
			violatedConstraints++;
			overallConstraintViolation += constraint;
		}

		constraint = 0;
		
		for (int i = 0; i < tasks.size(); i++) {
			int sum = 0;
			for (int j = 0; j < resources.size(); j++) {
				sum += x[i][j];
			}

			if (sum > 1.0) {
				constraint += (1.0 - sum);
			}
		}

		if (constraint < 0) {
			violatedConstraints++;
			overallConstraintViolation += constraint;
		}

		overallConstraintViolationDegree.setAttribute(solution, (double) overallConstraintViolation);
		numberOfViolatedConstraints.setAttribute(solution, violatedConstraints);
	}
	
	private void checkRequirementsFeasibility(List<Requirement> requirements) {
		//Remove non-affordable jobs.
		for (Requirement req : requirements) {
			if (req.getDeadline() != null && req.getDeadline().after(deadline)) {
				//Update predecessors not to link this non-affordable node.
				for (Requirement pred : req.getPredecessors()) {
					pred.removeSuccessor(req);
				}
	
				for (Job job : req.getJobs()) {
					for (Job pred : job.getPredecessors()) {
						pred.removeSuccessor(job);
					}
				}
				
				purgeSuccessors(req, requirements);
	
				requirements.remove(req);
			}
		}
	}

	private void purgeSuccessors(Requirement req, List<Requirement> requirements) {
		List<Requirement> predecessors;
		
		for (Requirement succ : req.getSuccessors()) {
			predecessors = succ.getPredecessors();
			
			for (int i = 0; i < predecessors.size(); i++) {
				if (!predecessors.get(i).equals(req)) {
					predecessors.get(i).removeSuccessor(succ);	
				}
			}
			
			purgeSuccessors(succ, requirements);
			
			requirements.remove(succ);
		}
	}
	
	/*private Integer workingFunction (Integer[][] x, int iEmpl, int instant) {
		Integer sum = 0;

		for (int i = 0; i < tasks.size(); i++) {
			Requirement req = tasks.get(i);
			if (req.fitsEmployee(resources.get(iEmpl)) && instant >= req.getStartTime() && instant <= req.getEndTime()) {
				sum += x[i][iEmpl];
			}
		}

		return sum;
	}*/
}
