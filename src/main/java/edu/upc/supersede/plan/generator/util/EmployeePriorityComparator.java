package edu.upc.supersede.plan.generator.util;

import java.util.*;

import edu.upc.supersede.app.model.Employee;

public class EmployeePriorityComparator implements Comparator<Employee>{

	@Override
	public int compare(Employee o1, Employee o2) {
        int criteriaResult = Integer.valueOf(o1.getPriorityCharge()).compareTo(Integer.valueOf(o2.getPriorityCharge()));
        
        return criteriaResult;
	}
}
