package edu.upc.supersede.plan.generator.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;

import edu.upc.supersede.app.dto.PlanDTO;
import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.Plan;
import edu.upc.supersede.app.model.Release;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.plan.generator.algorithm.NSGAIICustomPopulation;
import edu.upc.supersede.plan.generator.customsolution.PolynomialMutation;
import edu.upc.supersede.plan.generator.customsolution.ProjectSolution;
import edu.upc.supersede.plan.generator.customsolution.SBXCrossover;
import edu.upc.supersede.plan.generator.priorprojectsolution.IntegerPolynomialMutation;
import edu.upc.supersede.plan.generator.priorprojectsolution.IntegerSBXCrossover;
import edu.upc.supersede.plan.generator.priorprojectsolution.PrioritizedProjectSolution;

public class PlanGenerator {
	
	private long projectId;
	private String namePlan;
	private Release release;
	
	private List<Requirement> requirements;
	private List<Employee> resources;
	private Solver solver;
	
	private List<Requirement> solutionRequirements;
	
	public PlanGenerator(long projectId, Release release) {
		this.projectId = projectId;
		this.namePlan = release.getName();
		this.release = release;
	}
	
	public void loadRequirements(List<Requirement> requirements) {
		this.requirements = requirements;
	}

	public void loadResources(List<Employee> resources) {
		this.resources = resources;
	}	
	
	public void loadSolver (Solver solver) {
		this.solver = solver;
	}
	
	public List<PrioritizedProjectSolution> generate(Problem<PrioritizedProjectSolution> problem) {
		CrossoverOperator<PrioritizedProjectSolution> crossover;
		MutationOperator<PrioritizedProjectSolution> mutation;
		SelectionOperator<List<PrioritizedProjectSolution>, PrioritizedProjectSolution> selection;
		
		double crossoverProbability = 0.9;
		double crossoverDistributionIndex = 20.0;
		crossover = new IntegerSBXCrossover(crossoverProbability,
				crossoverDistributionIndex);

		double mutationProbability = 1.0 / problem.getNumberOfVariables();
		double mutationDistributionIndex = 20.0;
		mutation = new IntegerPolynomialMutation(mutationProbability,
				mutationDistributionIndex);

		selection = new BinaryTournamentSelection<PrioritizedProjectSolution>(
				new RankingAndCrowdingDistanceComparator<PrioritizedProjectSolution>());

		NSGAIICustomPopulation<PrioritizedProjectSolution> nsgaII = new NSGAIICustomPopulation<PrioritizedProjectSolution>(
				problem, 150, 150, crossover, mutation, selection,
				new SequentialSolutionListEvaluator<PrioritizedProjectSolution>());

		nsgaII.run();

		// It retrieves all the population
		List<PrioritizedProjectSolution> population = nsgaII.getPopulation();

		// Use this one to obtain only the most ranked-one solution.
		List<PrioritizedProjectSolution> solutions = nsgaII.getResult();

		return solutions;
		//return solver.translateSolutions(solutions, this.projectId, this.namePlan, this.releaseDate);
		//return new ArrayList<Plan>(Arrays.asList(new Plan[]{solver.translatePlanSolutions(solutions, this.projectId, this.namePlan, this.release)}));
	}

	
	// GETTERS
	public List<Requirement> getRequirements() {
		return this.requirements;
	}
	
	public List<Employee> getResources() {
		return this.resources;
	}

	public List<Requirement> getSolutionRequirements() {
		
		if (this.solutionRequirements != null) {			
			return this.solutionRequirements;
		} 
		else
			return requirements;
	}
	
	public void setSolutionRequirements(List<Requirement> requirements) {
		this.solutionRequirements = requirements;
	}
}
