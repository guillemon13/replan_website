package edu.upc.supersede.plan.generator.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import edu.upc.supersede.app.dto.PlanDTO;
import edu.upc.supersede.app.dto.plan.ChildDTO;
import edu.upc.supersede.app.dto.plan.DependencyDTO;
import edu.upc.supersede.app.dto.plan.DeveloperDTO;
import edu.upc.supersede.app.dto.plan.TaskDTO;
import edu.upc.supersede.app.model.*;
import edu.upc.supersede.app.model.Requirement.State;
import edu.upc.supersede.plan.generator.customsolution.ProjectSolution;
import edu.upc.supersede.plan.generator.priorprojectsolution.PrioritizedProjectSolution;

public class Solver {

	private List<Requirement> updatedRequirements;
	
	
	public List<PlanDTO> translateSolutions(List<ProjectSolution> solutions, 
			long projectId, String namePlan, Date releaseDate) {
		
		List<PlanDTO> plans = new ArrayList<PlanDTO>();

		for (ProjectSolution solution : solutions) {
			PlanDTO plan = new PlanDTO(projectId, namePlan, releaseDate);
			
			List<Requirement> reqs = solution.getRequirements();
			List<Employee> resources = solution.getEmployees();

			Map<String, List<Requirement>> assignments = new TreeMap<String, List<Requirement>>();
			Map<Integer, List<Employee>> reqEmployees = new HashMap<Integer, List<Employee>>();
			
			for (Requirement req : reqs) {
				for (Employee employee : resources) {
					if (req.fitsEmployee(employee)) {
						if (!assignments.containsKey(employee.getName())) {
							assignments.put(employee.getName(), new ArrayList<Requirement>());
						}

						if (!reqEmployees.containsKey(req.getInternalId())) {
							reqEmployees.put(req.getInternalId(), new ArrayList<Employee>());
						}
						
						assignments.get(employee.getName()).add(req);
						reqEmployees.get(req.getInternalId()).add(employee);
					}
				}
			}

			TaskDTO task; 
			
			for (String employee : assignments.keySet()) {
				DeveloperDTO developer = new DeveloperDTO(employee);
								
				for (Requirement req : assignments.get(employee)) {
					task = new TaskDTO(employee + "-" + req.getName(), req.getName(), req.getStartDate(), req.getEndDate(), req.getCriteria());
					
					for (Requirement succ : req.getSuccessors()) {
						for (Employee e : reqEmployees.get(succ.getId())) {
							DependencyDTO dep = new DependencyDTO(e.getName() + "-" + succ.getName());
							task.addDependency(dep);
						}
						
						task.addDepTask(new TaskDTO(employee + "-" + succ.getName(), succ.getName(), succ.getStartDate(), succ.getEndDate(), succ.getCriteria()));
					}
					
					developer.addChild(employee + "-" + req.getName());
					developer.addTask(task);
					
					ChildDTO child = new ChildDTO(employee + "-" + req.getName());
					child.addTask(task);
					
					plan.addObject(child);
				}
				
				plan.addObject(developer);
			}
			
			plans.add(plan);
			break;
		}

		return plans;
	}

	public List<Plan> translatePlanSolutions(List<PrioritizedProjectSolution> solutions, 
			long projectId, String namePlan, Release release) {
		
		List<Plan> plans = new ArrayList<Plan>();

		boolean updated = false;
		
		for (PrioritizedProjectSolution solution : solutions) {
			Plan plan = new Plan(projectId, namePlan, release.getDeadline());
			
			List<Requirement> reqs = solution.getRequirements();
			List<Employee> resources = solution.getEmployees();
			
			Map<String, List<Requirement>> assignments = new TreeMap<String, List<Requirement>>();
			Map<Integer, List<Employee>> reqEmployees = new HashMap<Integer, List<Employee>>();
			
			System.out.println("REQS SIZE: " + reqs.size());
			
			for (Requirement req: reqs) {
				System.out.println(req.getName());
				System.out.println("StartTime: " + req.getStartTime());
				System.out.println("EndTime: " + req.getEndTime());
			}
			
			for (Requirement req : reqs) {
				for (Employee employee : resources) {
					if (req.fitsEmployee(employee) && req.containsJob(employee) && 
							(req.getJob(employee).getDedication() > 0 || req.getState().equals(State.IN_PROGRESS))) {
						
						System.out.println("ASSIGNMENT! "+ req);
						
						if (!assignments.containsKey(employee.getName())) {
							assignments.put(employee.getName(), new ArrayList<Requirement>());
						}

						if (!reqEmployees.containsKey(req.getInternalId())) {
							reqEmployees.put(req.getInternalId(), new ArrayList<Employee>());
						}
						
						assignments.get(employee.getName()).add(req);
						reqEmployees.get(req.getInternalId()).add(employee);
						
						if (req.getState().equals(State.IN_PROGRESS)) {
							break;
						}
					}
				}
				if (!req.getJobs().isEmpty() && !req.getState().equals(State.IN_PROGRESS)) {
					req.updateStartDate(release.getInitialDate());
				}
			}

			if (!updated) {
				this.updatedRequirements = reqs;
				updated = true;
			}
			
			Task task; 
			
			for (String employee : assignments.keySet()) {
				Developer developer = new Developer(employee, projectId);
								
				for (Requirement req : assignments.get(employee)) {
					task = new Task(employee + "-" + req.getName(), req.getName(), req.getStartDate(), req.getEndDate(), req.getCriteria(), projectId);
					
					for (Requirement succ : req.getSuccessors()) {						
						for (Employee e : reqEmployees.get(succ.getInternalId())) {
							task.addDependency(e.getName() + "-" + succ.getName());
						}
					}
					
					developer.addChild(employee + "-" + req.getName());
					developer.addTask(task);
				}
				
				plan.addDeveloper(developer);
			}
			plan.setReleaseId(release.getId());
			plans.add(plan);
			
			break;
		}

		return plans;
	}

	public List<Requirement> getUpdatedRequirements() {
		return updatedRequirements;
	}

	public void setUpdatedRequirements(List<Requirement> updatedRequirements) {
		this.updatedRequirements = updatedRequirements;
	}
	
	
	
}
