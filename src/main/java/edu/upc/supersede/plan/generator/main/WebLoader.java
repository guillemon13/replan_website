package edu.upc.supersede.plan.generator.main;

import java.util.List;

import org.apache.log4j.Logger;

import edu.upc.supersede.app.model.*;


public class WebLoader extends KnapsackLoader{
	
	private long projectId;
	
	public WebLoader(int nObjectives) {
		super(nObjectives);
	}

	public void createFeatures(List<Requirement> reqs) {
		this.setRequirements(reqs);
	}

	public void createEmployees(List<Employee> resources) {
		this.setResources(resources);
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	
}
