package edu.upc.supersede.plan.generator.util;

import java.util.List;
import java.util.Vector;

import edu.upc.supersede.app.model.Job;
 
 
public class LongestPathJobs
{
	
	private int    nNodes;                      // number of nodes
    private int    target;                 // destination node
    private int    minLength;              // the minimal length of each path
    private Node[] vecNodes;                      // used to store Nodes
    private Edge[] vecEdges;                      // used to store Edges
    
    private int[]  path;                   // used to store temporary path
    private int    length       = 0;       // length of the path
    private int    distance     = 0;       // distance of the path
    
    private int[]  bestPath;               // used to store temporary path
    private int    bestLength   = 0;       // length of the longest path
    private int    bestDistance = -1000000; // distance of the longest path
    
    private int[]  visited;                // used to mark a node as visited if set as 1
 
    public LongestPathJobs(List<Job> jobs, int nEdges)
    {
    	nNodes = jobs.size();

        vecNodes = new Node[nNodes];
        vecEdges = new Edge[nEdges];

        for (int i = 0; i < nNodes; i++)
            vecNodes[i] = new Node(i);

        int i = 0, sVal = 0, eVal = 0;
        
    	for (Job job : jobs) {
    		Edge edge; 
    		
    		sVal = 0;
    		
    		for (Job succ : job.getSuccessors()) {
    			edge = new Edge(i);
	            sVal = job.getInternalId();
	            edge.setStart(sVal);
	            
	            eVal = succ.getInternalId();
	            edge.setEnd(eVal);
	            
	            edge.setCapacity(job.getDevTime());
	            
	            vecEdges[i] = edge;
	            vecNodes[sVal].addFors(i);
	            vecNodes[eVal].addBacks(i);
	            i++;
    		}
            
        }
        	
        visited = new int[vecNodes.length];
        path = new int[vecNodes.length];
        bestPath = new int[vecNodes.length];
    }
 
    /*
     * this function looks for a longest path starting from being to end,
     * using the backtrack depth-first search.
     */
    public boolean findLongestPath(int begin, int end, int minLen)
    {
        /*
         * compute a longest path from begin to end
         */
        target = end;
        bestDistance = -100000000;
        minLength = minLen;
        dfsLongestPath(begin);
        if (bestDistance == -100000000)
            return false;
        else 
            return true;
    }
 
    private void dfsLongestPath(int current)
    {
        visited[current] = 1;
        path[length++] = current;
        if (current == target && length >= minLength)
        {
            if (distance > bestDistance)
            {
                for (int i = 0; i < length; i++)
                    bestPath[i] = path[i];
                bestLength = length;
                bestDistance = distance;
            }
        }
        else
        {
            Vector<Integer> fors = vecNodes[current].getFors();
            for (int i = 0; i < fors.size(); i++)
            {
                Integer edge_obj = (Integer) fors.elementAt(i);
                int edge = edge_obj.intValue();
                if (visited[vecEdges[edge].getEnd()] == 0)
                {
                    distance += vecEdges[edge].getCapacity();
                    dfsLongestPath(vecEdges[edge].getEnd());
                    distance -= vecEdges[edge].getCapacity();
                }
            }
        }
        visited[current] = 0;
        length--;
    }
 
    public int computeBestDistance(int begin, int end) {
    	if (this.findLongestPath(begin, end, 1)) {
    		return this.bestDistance;	
    	} else {
    		return -1;
    	}
    }
    
    public String toString()
    {
        String output = "v" + bestPath[0];
        for (int i = 1; i < bestLength; i++)
            output = output + " -> v" + bestPath[i];
        return output;
    }
}
