package edu.upc.supersede.plan.generator.main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import edu.upc.supersede.app.model.*;
import edu.upc.supersede.app.model.Requirement.State;
import edu.upc.supersede.plan.generator.util.LongestPathJobs;


/**
 *
 * @author grufian - SUPERSEDE project
 *
 * This class is the base to which all loaders must inheritate.
 * The Release Planning problem refines (must add abstract methods) from here.
 *
 */
public abstract class KnapsackLoader {

	private List<Requirement> requirements;
	private int nObjectives;
	private List<Requirement> mustRequirements;

	private List<Requirement> notAffordables;
	
	private List<Employee> resources;

	private List<Job> jobs;

	private int initialTime = -1;
	
	private Date deadline;
	private Date startDate;
	
	public KnapsackLoader(int nObjectives) {
		this.requirements = new ArrayList<Requirement>();
		this.resources = new ArrayList<Employee>();
		this.jobs = new ArrayList<Job>();
		this.nObjectives = nObjectives;
		this.mustRequirements = new ArrayList<Requirement>();
	}

	public List<Requirement> getRequirements() {
		return requirements;
	}

	public void setRequirements(List<Requirement> requirements) {
		this.requirements = requirements;
	}

	public int getNumberOfObjectives() {
		return nObjectives;
	}

	public int getNumberOfRequirements() {
		return requirements.size();
	}

	public List<Employee> getResources() {
		return resources;
	}

	public void setResources(List<Employee> resources) {
		this.resources = resources;
	}

	public void setMustRequirements(List<Requirement> requirements) {
		this.mustRequirements = requirements;
	}

	public void createJobs() {
		//Setting up reference start and end jobs.
		int k = 0;

		Job start = new Job(0, "START");
		start.setDevTime(0);

		jobs.add(start);

		Job end = new Job(k, "END");
		end.setDevTime(0);

		jobs.add(end);

		k++;

		//List<Requirement> notAffordables = new ArrayList<Requirement>();

		this.notAffordables = new ArrayList<Requirement>();
		
		boolean replan = initialTime != -1;
		if (!replan) initialTime = 0;
		
		List<Requirement> omittedReqs = new ArrayList<Requirement>();
		
		for (Requirement req : requirements) {
			boolean isAffordable = false;

			if (replan && req.isDone()) {
				omittedReqs.add(req);
				req.setState(Requirement.State.DONE);
			} else if (replan && req.isRunningNow()) {
				omittedReqs.add(req);
				req.setState(Requirement.State.IN_PROGRESS);
			} else {
				if (replan) {
					req.clearJobs();
				}
			
				//Normal process.
				for (Employee empl : resources) {
					if (req.fitsEmployee(empl)) {
						Job job = new Job(k, "Job " + req.getName());
						job.setRequirement(req);
						job.setDevTime(req.getEffort());
						job.setEmployee(empl);
	
						k++;
	
						jobs.add(job);
						req.addJob(job);
						
						isAffordable = true;
						
						if (replan) {
							System.out.println(req.getName() + " FITS! ");
						}
					}
				}
	
				//If there are no employees able to do this work... the successors cannot be done!
				if (!isAffordable) {
					notAffordables.add(req);
				} else {
					req.setState(State.TO_DO);
				}
			}
		}
		
		//Add omitted reqs jobs.
		List<Job> omittedJobs = new ArrayList<Job>();
		if (replan) {
			for (Requirement req : omittedReqs) {
				for (Job job: req.getJobs()) {
					job.setInternalId(k);
					omittedJobs.add(job);
					k++;
					jobs.add(job);
				}
			}
		}
		
		end.setInternalId(k);
		
		if ((notAffordables.size() + omittedReqs.size()) == requirements.size()) {
			notAffordables.addAll(omittedReqs);
		}
		
		//Remove non-affordable jobs.
		for (Requirement req : notAffordables) {

			//Update predecessors not to link this non-affordable node.
			for (Requirement pred : req.getPredecessors()) {
				pred.removeSuccessor(req);
			}

			for (Job job : req.getJobs()) {
				for (Job pred : job.getPredecessors()) {
					pred.removeSuccessor(job);
				}
			}
			
			purgeSuccessors(req);

			requirements.remove(req);
			jobs.removeAll(req.getJobs());
		}

		for (Requirement req : requirements) {
			for (Requirement succ : req.getSuccessors()) {
				for (Job job: req.getJobs()) {
					job.addSuccessors(succ.getJobs());
				}
			}

			for (Requirement pred : req.getPredecessors()) {
				for (Job job: req.getJobs()) {
					job.addPredecessors(pred.getJobs());
				}
			}
		}

		for (Job job : jobs) {
			if (job.getInternalId() != 0 && job.getInternalId() != jobs.size()-1) {
				if (job.getSuccessors().isEmpty()) {
					job.addSuccessor(end);
					end.addPredecessor(job);
				}

				if (job.getPredecessors().isEmpty()) {
					job.addPredecessor(start);
					start.addSuccessor(job);
				}
			}
		}

		int i = 1;
		for (Job job : jobs) {
			if (job.getInternalId() != 0 && job.getInternalId() != jobs.size()-1) {
				job.setInternalId(i);
				i++;
			}
		}

		computePaths(replan);
		
		if (replan) {
			jobs.removeAll(omittedJobs);
		}
		
		i = 0;
		for (Requirement req: requirements) {
			req.setInternalId(i);
			i++;
		}
	}

	public void computePaths(boolean replan) {
		int nEdges = 0;

		for (Job job : jobs) {
			nEdges += job.getSuccessors().size();
			nEdges += job.getPredecessors().size();
		}

		for (Job job : jobs) {
			if (job.getDevTime() != 0.0) {
				LongestPathJobs lpj = new LongestPathJobs(jobs, nEdges);
				
				//Cannot replan something executing.
				if (job.getState().equals(State.TO_DO)) {
					int startTime = lpj.computeBestDistance(0, job.getInternalId());
					
					if (startTime == -1) {
						startTime = 0;
					}
					
					job.purge();
					
					if (!replan) {
						job.setEarlyStart(startTime);
					} else if (replan && !job.getPredecessors().isEmpty()){
						if (job.getPredecessors().get(0).getDevTime() > 0) {
							job.setEarlyStart(startTime);	
						} else {
							job.setEarlyStart(startTime + initialTime);
						}
						
					} else {
						job.setEarlyStart(startTime + initialTime);
					}
				}
				
				//job.setLatestStart(limitTime - (int)job.getDevTime());
				//job.setLatestStart(limitTime - lpj.computeBestDistance(job.getInternalId(), end));
			}
		}
		
		int i = 0;
		while (i < jobs.size()) {
			if (jobs.get(i).getDevTime() == 0.0) {
				jobs.remove(i);
			} else {
				jobs.get(i).purge();
				i++;
			}
		}
	}

	private void purgeSuccessors(Requirement req) {
		List<Requirement> predecessors;
		
		int i = 0;
		while (i < req.getSuccessors().size()) {
			Requirement succ = req.getSuccessors().get(i);
			
			predecessors = succ.getPredecessors();
			
			for (int j = 0; j < predecessors.size(); j++) {
				if (!predecessors.get(j).equals(req)) {
					predecessors.get(j).removeSuccessor(succ);	
				}
			}
			
			for (Job job: succ.getJobs()) {
				System.out.println("JOBS REQ" + succ.getName() + ":" + job.getInternalId());
				System.out.println();
			}
			
			purgeSuccessors(succ);
			
			requirements.remove(succ);
			jobs.removeAll(succ.getJobs());
			
			i++;
		}
	}
	

	
	public void setStartDate(Date startDate) {
		Calendar c = new GregorianCalendar();
		c.setTime(startDate);
		c.set(Calendar.HOUR_OF_DAY, 9);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		
		for (Requirement req : requirements) {
			if (req.getStartDate() == null) {
				req.setStartDate(c.getTime());
				
				for (Requirement succ : req.getSuccessors()) {
					succ.setStartDate(c.getTime());
				}
			}
		}
		
		this.startDate = c.getTime();
	}
	
	public int getTmax() {
		int tMax = 0;

		for (Requirement req : requirements) {
			tMax += req.getEffort();
		}

		return tMax;
	}
	
	public Date getDeadline() {
		return this.deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	
	public List<Job> getJobs() {
		return this.jobs;
	}

	public List<Requirement> getMustRequirements() {
		return this.mustRequirements;
	}

	public int getInitTime() {
		return this.initialTime;
	}

	public void computeInitTime(Date startDate, Date replanDate) {
		Calendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(startDate);
		
		Calendar replanCalendar = new GregorianCalendar();
		replanCalendar.setTime(replanDate);
		
		//Case 1 - We are in the same day.
		if (startCalendar.get(Calendar.DAY_OF_YEAR) != replanCalendar.get(Calendar.DAY_OF_YEAR)) {
			initialTime = 8 * (replanCalendar.get(Calendar.DAY_OF_YEAR)-startCalendar.get(Calendar.DAY_OF_YEAR)) ;
		} else {
			initialTime = 0;
		}
		
		//Case 1.1.- same day inside the working days.
		if (replanCalendar.get(Calendar.HOUR_OF_DAY) >= 9 && replanCalendar.get(Calendar.HOUR_OF_DAY) < 17) {
			initialTime += replanCalendar.get(Calendar.HOUR_OF_DAY) - startCalendar.get(Calendar.HOUR_OF_DAY);
		}
		//Case 1.2.- out of working days.
		else if (replanCalendar.get(Calendar.HOUR_OF_DAY) < 9)
		{
			initialTime += 0;
		}
		else {
			initialTime += 8;
		}
		
		
	}
	
	public Date getStartDate() {
		return this.startDate;
	}
	
	public List<Requirement> getNotAffordables() {
		return this.notAffordables;
	}
}
