package edu.upc.supersede.plan.generator.tester.main;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.upc.supersede.app.model.*;
import edu.upc.supersede.plan.generator.main.WebLoader;


public class SiemensUseCase {
	public static WebLoader loadReqs() throws Exception{
		Skill PL = new Skill("Planning", "PL", "Planning", 1000L);
		Skill PR = new Skill("Presentation", "PR", "Presentation", 1000L);
		Skill CO = new Skill("Concept", "CO", "Concept", 1000L);
		Skill AR = new Skill("Architecture", "AR", "Architecture", 1000L);
		Skill DE = new Skill("Design", "DE", "Design", 1000L);
		Skill DV = new Skill("Development", "DV", "Development", 1000L);
		Skill DVF = new Skill("Development Frontend", "DVF", "Development Frontend", 1000L);
		Skill DVB = new Skill("Development Backend", "DVB", "Development Backend", 1000L);
		Skill DVT = new Skill("Development Testing", "DVT", "Development Testing", 1000L);
		Skill VI = new Skill("Visualization", "DA", "Visualization", 1000L);
		Skill SE = new Skill("Semantics", "SE", "Semantics", 1000L);
		Skill WT = new Skill("Web Toolkits", "DA", "Web Toolkits", 1000L);
		Skill DA = new Skill("Data Analytics", "DA", "Data Analytics", 1000L);
		Skill DB = new Skill("Databases", "DB", "Databases", 1000L);
		Skill SC = new Skill("Security", "SC", "Security", 1000L);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Date M3 = sdf.parse("23/03/2016");
		Date M4 = sdf.parse("23/03/2016");
		Date M5 = sdf.parse("23/03/2016");
		Date M6 = sdf.parse("23/03/2016");
		Date M7 = sdf.parse("23/03/2016");
		Date M9 = sdf.parse("23/03/2016");
		
		int bin = 3;
		
		Requirement u11 = new Requirement("Setup of a server infrastructure for operational environment",
				3 * bin, 1, null, 1, 1000L);
		u11.addSkill(DVB);
		u11.setDeadline(M4);
		
		Requirement u12 = new Requirement("Connectivity of operational infrastructure to DWH",
				2 * bin, 1, null, 1, 1000L);
		u12.addSkill(DB);
		u12.addPredecessor(u11);
		
		Requirement u13 = new Requirement("Access to Internet APIs e.g. a weather service",
				1 * bin, 2, null, 1, 1000L);
		u13.addPredecessor(u11);
		u13.setDeadline(M5);
		
		Requirement u15 = new Requirement("Access to operational infrastructure from internet",
				1 * bin, 2, null, 1, 1000L);
		u15.addPredecessor(u11);
		u15.setDeadline(M4);
		
		Requirement u16 = new Requirement("Monitoring Facility for operational system",
				2 * bin, 2, null, 1, 1000L);
		u16.addPredecessor(u11);
		u16.setDeadline(M4);
		
		Requirement u17 = new Requirement("Installation and operation of a Basic API Management platform ",
				2 * bin, 1, null, 1, 1000L);
		u17.addPredecessor(u11);
		u17.setDeadline(M5);
		
		Requirement u18 = new Requirement("Look and Feel for User Facing components",
				1 * bin, 2, null, 1, 1000L);
		u18.setDeadline(M6);
		u18.addSkill(DVF);
		u18.addPredecessor(u17);
		
		Requirement u21 = new Requirement("Setup of a testing server infrastructure for development and test",
				2 * bin, 1, null, 1, 1000L);
		u21.setDeadline(M4);
		
		Requirement u22 = new Requirement("Setup of development environment",
				2 * bin, 1, null, 1, 1000L);
		u22.setDeadline(M7);
		
		Requirement u31 = new Requirement("IF Source API, Desc, Patterns, Groups, Edit",
				3 * bin, 1, null, 1, 1000L);
		u31.addSkill(SE);
		u31.setDeadline(M6);
		
		Requirement u32 = new Requirement("IF Policies, Pricing, Access",
				3 * bin, 1, null, 1, 1000L);
		u32.addPredecessor(u31);
		u32.setDeadline(M9);
		
		Requirement u33 = new Requirement("Support for REST style data APIs",
				1 * bin, 1, null, 1, 1000L);
		u33.addSkill(WT);
		u33.setDeadline(M6);
		
		Requirement u34 = new Requirement("Provide mockup source API for testing",
				2 * bin, 2, null, 1, 1000L);
		u34.setDeadline(M6);
		
		Requirement u35 = new Requirement("Registration as API Provider",
				1 * bin, 1, null, 1, 1000L);
		u35.setDeadline(M4);	
		
		Requirement u36 = new Requirement("Specify quota Policy for API usage",
				1 * bin, 3, null, 1, 1000L);
		u36.setDeadline(M7);
		
		Requirement u37 = new Requirement("Specify geographic Policy for API usage",
				1 * bin, 2, null, 1, 1000L);
		u37.setDeadline(M7);
		
		Requirement u38 = new Requirement("Promotion of APIs",
				1 * bin, 2, null, 1, 1000L);
		u38.setDeadline(M9);
		
		Requirement u41 = new Requirement("Monitoring Dashboard (Accesses by Apps, users or location)",
				2 * bin, 2, null, 1, 1000L);
		u41.addSkill(DVF);
		u41.addSkill(VI);
		u41.addSkill(DA);
		u41.setDeadline(M9);
		
		Requirement u42 = new Requirement("Notifications for failed accesses",
				1 * bin, 3, null, 1, 1000L);
		u42.addSkill(DVF);
		u42.addPredecessor(u41);
		u42.setDeadline(M9);
		
		Requirement u43 = new Requirement("Report / Output for billing system",
				1 * bin, 3, null, 1, 1000L);
		u43.addSkill(DVF);
		u43.addPredecessor(u41);
		u43.setDeadline(M9);
		
		Requirement u51 = new Requirement("Explicit clearance of applications / services ",
				1 * bin, 1, null, 1, 1000L);
		u51.setDeadline(M4);
		
		Requirement u52 = new Requirement("Revocation of service / application / user credentials",
				1 * bin, 2, null, 1, 1000L);
		u52.setDeadline(M5);

		Requirement u53 = new Requirement("Real time view of API accesses",
				1 * bin, 3, null, 1, 1000L);
		u53.setDeadline(M6);
		u53.addPredecessor(u41);
		
		Requirement u54 = new Requirement("Log file of all accesses to own APIs",
				1 * bin, 2, null, 1, 1000L);
		u54.setDeadline(M6);
		
		Requirement u55 = new Requirement("Ensure and enforce quota policies specified ",
				1 * bin, 2, null, 1, 1000L);
		u55.setDeadline(M4);
		
		Requirement u56 = new Requirement("Ensure and enforce location based access policies (IP ranges, geographic)",
				2 * bin, 2, null, 1, 1000L);
		u56.setDeadline(M6);
		
		Requirement u57 = new Requirement("Block/ban developers/publishers/…",
				1 * bin, 2, null, 1, 1000L);
		u57.setDeadline(M5);
		
		Requirement u58 = new Requirement("Realization of a DMZ scenario",
				2 * bin, 1, null, 1, 1000L);
		u58.setDeadline(M6);

		Requirement u61 = new Requirement("Registration at Ecosystem", 2 * bin, 1, null, 1, 1000L);
		u61.setDeadline(M3);
		
		Requirement u62 = new Requirement("Creation of application and application keys", 2 * bin, 1, null, 1, 1000L);
		u62.setDeadline(M3);
		u62.addSkill(SC);
		
		Requirement u63 = new Requirement("CRUD Subscription to APIs", 2 * bin, 1, null, 1, 1000L);
		u63.addSkill(SC);
		u63.setDeadline(M3);
		u63.addPredecessor(u62);
		
		Requirement u64 = new Requirement("Search for suitable APIs (free/Paid/location/format/contents)",
				1 * bin, 2, null, 1, 1000L);
		u64.setDeadline(M3);
		
		Requirement u65 = new Requirement("Give suggestions for API improvements", 1 * bin, 3, null, 1, 1000L);
		u65.setDeadline(M6);
		u65.addPredecessor(u64);
		
		Requirement u66 = new Requirement("Create general and end user data based applications", 1 * bin, 2, null, 1, 1000L);
		u66.setDeadline(M6);
		
		Requirement u67 = new Requirement("Discuss and Rate APIs", 2 * bin, 3, null, 1, 1000L);
		u67.setDeadline(M6);
		
		Requirement u68 = new Requirement("Use Mockup Service for testing", 2 * bin, 2, null, 1, 1000L);
		u68.setDeadline(M6);
		
		Employee anna = new Employee("Anna", 16, 1000L);
		anna.addSkill(PL);
		anna.addSkill(PR);
		anna.addSkill(CO);
		anna.addSkill(AR);
		
		Employee bea = new Employee("Bea", 10, 1000L);
		bea.addSkill(PL);
		bea.addSkill(PR);
		bea.addSkill(CO);
		
		Employee cesar = new Employee("Cesar", 32, 1000L);
		cesar.addSkill(DV);
		cesar.addSkill(DVF);
		cesar.addSkill(WT);
		cesar.addSkill(DB);

		Employee dan = new Employee("Dan", 32, 1000L);
		dan.addSkill(DVF);
		dan.addSkill(DV);
		dan.addSkill(DVT);
		dan.addSkill(SC);
		
		Employee eva = new Employee("Eva", 32, 1000L);
		eva.addSkill(DV);
		eva.addSkill(VI);
		eva.addSkill(SE);
		eva.addSkill(DB);
		
		Employee fred = new Employee("Fred", 10, 1000L);
		fred.addSkill(DV);
		fred.addSkill(VI);
		fred.addSkill(DA);
		fred.addSkill(DVB);
		
		Employee gillian = new Employee("Gillian", 6, 1000L);
		gillian.addSkill(AR);
		gillian.addSkill(DA);
		gillian.addSkill(SE);
		gillian.addSkill(DB);
		gillian.addSkill(SC);
		
		Employee helena = new Employee("Helena", 10, 1000L);
		helena.addSkill(AR);
		helena.addSkill(WT);
		helena.addSkill(CO);
		helena.addSkill(PL);
		
		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { u11, u12, u13, u15, u16, u17,
						u18, u21, u22, u31, u32, u33, u34, u35, u36, u37, u38, u41, u42, u43, u51, u52, u53,
						u54, u55, u56, u57, u58, u61, u62, u63, u64, u65, u66, u67, u68}));
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { anna, bea, cesar, dan, eva, fred, gillian, helena }));

		WebLoader loader = new WebLoader(1);
		loader.createEmployees(employees);
		loader.createFeatures(reqs);
		loader.createJobs();

		int i = 0;
		for (Requirement req : reqs) {
			req.setInternalId(i);
			i++;
		}

		return loader;
	}
}
