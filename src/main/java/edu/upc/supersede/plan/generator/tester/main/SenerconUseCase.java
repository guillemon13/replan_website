package edu.upc.supersede.plan.generator.tester.main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.upc.supersede.app.model.*;
import edu.upc.supersede.plan.generator.main.WebLoader;


public class SenerconUseCase {
	
	public static WebLoader loadReqs() throws Exception {
		Skill T = new Skill("Test", "T", "testing requirement", 1000L);
		Skill BD = new Skill("Backend Development", "BD", "backend development", 1000L);
		Skill AT = new Skill("Acceptance Test", "AT", "Acceptance Test", 1000L);
		Skill D = new Skill("Development", "D","Development", 1000L);
		Skill P = new Skill("planning", "P","", 1000L);
		Skill C = new Skill("Concept", "C","", 1000L);
		Skill AG = new Skill("Aggregation", "AG","", 1000L);
		Skill FH = new Skill("Form handling", "FH","", 1000L);
		Skill CH = new Skill("Charts", "CH","", 1000L);
		Skill FD = new Skill("Frontend development", "FD", "FD", 1000L);
		Skill LO = new Skill("Layout", "LO","Layout", 1000L);
		Skill CA = new Skill("Calculation", "CA","calculation", 1000L);
		Skill EAV = new Skill("EAV", "EAV", "EAV", 1000L);

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Requirement f1 = new Requirement("Update Layout framework", 15, 3, null, 1, 1000L);
		f1.addSkill(FD);

		Requirement f1t = new Requirement("Testing Layout Update", 4, 3, null, 1, 1000L);
		f1t.addSkill(T);
		f1t.addPredecessor(f1);

		Requirement f2 = new Requirement("HTML Error Codes for Exceptions",
				8, 2, null, 1, 1000L);
		f2.addSkill(BD);

		Requirement f3 = new Requirement("Documentation of the restful webservice", 6, 2, null, 1, 1000L);
		f3.addSkill(BD);
		f3.addPredecessor(f2);

		Requirement f3at = new Requirement("Acceptance of the Webservice docu", 2, 2, null, 1, 1000L);
		f3at.addSkill(AT);
		f3at.addPredecessor(f3);

		Requirement f4 = new Requirement("Portal binding", 2, 1, null, 1, 1000L);
		f4.addSkill(BD);

		Requirement f4t = new Requirement("Testing the Portal binding", 1, 1, null, 1, 1000L);
		f4t.addSkill(T);
		f4t.addPredecessor(f4);
		f4t.setDeadline(sdf.parse("29/02/2016"));

		Requirement f5 = new Requirement("Autologin via cookie", 4, 2, null, 1, 1000L);
		f5.addSkill(BD);

		Requirement f5t = new Requirement("Testing the new autologin", 2, 2, null, 1, 1000L);
		f5t.addSkill(T);
		f5t.addPredecessor(f5);

		Requirement f6 = new Requirement("Automatic setting of the height in iframes", 6, 1, null, 1, 1000L);
		f6.addSkill(D);

		Requirement f6at = new Requirement("Acceptance Test of the autoheight", 1, 1, null, 1, 1000L);
		f6at.addPredecessor(f6);
		f6at.addSkill(AT);
		f6at.setDeadline(sdf.parse("31/03/2016"));

		Requirement f7 = new Requirement("Layout adaptation for customer ProKlima", 20, 1, null, 1, 1000L);
		f7.addSkill(FD);

		Requirement f7t = new Requirement("Internal test of the proKlima Layout", 8, 1, null, 1, 1000L);
		f7t.addSkill(T);
		f7t.addPredecessor(f7);

		Requirement f7at = new Requirement("Customertest of the proKlima Layout", 4, 1, null, 1, 1000L);
		f7at.addSkill(AT);
		f7at.addPredecessor(f7t);
		f7at.setDeadline(sdf.parse("31/03/2016"));

		Requirement f8 = new Requirement("Image rotation on landingpage",
				6, 1, null, 1, 1000L);
		f8.addSkill(D);

		Requirement f8t = new Requirement("Testing of the immage rotation",
				15, 3, null, 1,  1000L);
		f8t.addSkill(T);
		f8t.addPredecessor(f8);

		Requirement f8at = new Requirement("Acceptance test of the image rotation", 1, 1, null, 1, 1000L);
		f8at.addSkill(AT);
		f8at.addPredecessor(f8t);
		f8at.setDeadline(sdf.parse("31/03/2016"));

		Requirement f9 = new Requirement("Webservice for pushnotifictaions", 30, 2, null, 1, 1000L);
		f9.addSkill(BD);

		Requirement f9t = new Requirement("Testing of push notifications",
				2, 2, null, 1, 1000L);
		f9t.addSkill(T);
		f9t.addPredecessor(f9);

		Requirement f10 = new Requirement("Table with single data values of the EAV", 4, 1, null, 1, 1000L);
		f10.addSkill(EAV);

		Requirement f11 = new Requirement("Adaptation for business customer", 20, 1, null, 1, 1000L);
		f11.addSkill(BD);

		Requirement f11t = new Requirement("Testing the adaption for business customer", 2, 1, null, 1, 1000L);
		f11t.addSkill(T);

		Requirement f11at = new Requirement("Acceptance of the adaption for business customer", 1, 1, null, 1, 1000L);
		f11at.addSkill(AT);
		f11at.setDeadline(sdf.parse("15/02/2016"));

		Requirement f12 = new Requirement("Consumption Grouping", 80, 1, null, 1, 1000L);
		f12.addSkill(BD);

		Requirement f12t = new Requirement("Testing the consumption grouping", 12, 1, null, 1, 1000L);
		f12t.addSkill(CA);
		f12t.addSkill(T);
		f12t.addPredecessor(f12);

		Requirement f13c = new Requirement("Feedback Tool for Supersede - concept", 12, 1, null, 1, 1000L);
		f13c.addSkill(C);

		Requirement f13 = new Requirement("Feedback Tool for Supersede",
				40, 1, null, 1, 1000L);
		f13.addSkill(BD);
		f13.addSkill(FD);
		f13.addPredecessor(f13c);

		Requirement f13t = new Requirement("Testing of the feedback tool",
				4, 1, null, 1, 1000L);
		f13t.addSkill(T);
		f13t.addPredecessor(f13);
		f13t.setDeadline(sdf.parse("16/03/2016"));

		Employee customer = new Employee("customer", 8, 1000L);
		customer.addSkill(AT);

		Employee markus = new Employee("Markus", 30, 1000L);
		markus.addSkill(T);
		markus.addSkill(P);
		markus.addSkill(C);

		Employee stefan = new Employee("Stefan", 23, 1000L);
		stefan.addSkill(D);
		stefan.addSkill(BD);
		stefan.addSkill(AG);
		stefan.addSkill(FH);

		Employee oleg = new Employee("Oleg", 14, 1000L);
		oleg.addSkill(D);
		oleg.addSkill(BD);
		oleg.addSkill(FH);
		oleg.addSkill(CH);
		oleg.addSkill(EAV);

		Employee oliver = new Employee("Oliver", 15, 1000L);
		oliver.addSkill(D);
		oliver.addSkill(FD);
		oliver.addSkill(LO);
		oliver.addSkill(T);

		Employee andreas = new Employee("Andreas", 20, 1000L);
		andreas.addSkill(C);
		andreas.addSkill(T);
		andreas.addSkill(CA);

		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { f1, f1t, f2, f3, f3at, f4,
						f4t, f5, f5t, f6, f6at, f7, f7t, f7at, f8, f8t, f8at,
						f9, f9t, f11, f11t, f11at, f12, f12t, f13, f13c, f13t }));
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { customer, markus, stefan,
						oliver, oleg, andreas }));

		WebLoader loader = new WebLoader(1);
		loader.createEmployees(employees);
		loader.createFeatures(reqs);
		loader.createJobs();

		int i = 0;
		for (Requirement req : reqs) {
			req.setInternalId(i);
			i++;
		}

		return loader;
	}

}
