package edu.upc.supersede.app.init;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.upc.supersede.app.model.*;
import edu.upc.supersede.plan.generator.main.WebLoader;
import edu.upc.supersede.plan.generator.tester.main.ProblemInstanceTranslator;

import javax.persistence.EntityManagerFactory;

import java.io.FileInputStream;
import java.sql.Time;
import java.util.*;

/**
 *
 * This is a initializing bean that inserts some test data in the database. It is only active in
 * the development profile, to see the data login with GRufian / Password2
 *
 * Added some requirements and resources to perform some tests.
**/
@Component
public class TestDataInitializer {

    @Autowired
    private EntityManagerFactory entityManagerFactory;


    public void init() throws Exception {

        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        User user = new User("GRufian", "$2a$10$x9vXeDsSC2109FZfIJz.pOZ4dJ056xBpbesuMJg3jZ.ThQkV119tS", "test@email.com", 1L);
        
        //Test Definition.
        //Test 1.- Priority test.
        user.addProject(createPriorityTest(session));
        
        //Test 2.- Capabilities test.
        user.addProject(createCapabilitiesTest(session));
        
        //Test 3.- Dependencies test.
        user.addProject(createDependenciesTest(session));
        
        //Test 4.- Real case.
        user.addProject(createRealCaseTest(session));
                
        //Test 5.- Unfeasible case.
        user.addProject(createUnfeasibleCaseTest(session));
        
        session.persist(user);
	
        transaction.commit();
    }
    
    private Project createRealCaseTest(Session session) {
    	Project project = new Project("Real Case Test", "Lorem Ipsum", new Date(System.currentTimeMillis()), 1);
    	session.save(project);
    	
    	long projectId = project.getId();
    	
    	try {
	    	String inputPath = System.getProperty("user.dir") + "/src/test/resources/instances/"; 
	        String nameFile = "inst10-10-10-5.conf";
	        
	        Properties properties = new Properties();
	        FileInputStream fileInputStream = new FileInputStream(inputPath + nameFile);
	        
	        properties.load(fileInputStream);
	        
	        ProblemInstanceTranslator pit = new ProblemInstanceTranslator(properties);
			WebLoader loader = new WebLoader(2);
			loader.setProjectId(projectId);
			
			loader.createEmployees(new ArrayList<Employee>(pit.getEmployees()));
			loader.createFeatures(new ArrayList<Requirement>(pit.getTasks()));
			loader.setStartDate(new Date(System.currentTimeMillis()));
	    	
			List<Skill> skills = new ArrayList<Skill>(pit.getSkills());
			
			for (Skill sk : skills) {
				sk.setProjectId(projectId);
				session.persist(sk);
			}
	    	
			for (Requirement req : loader.getRequirements()) {
				req.setProjectId(projectId);
				req.setMaxEmployees(1);
				session.persist(req);
			}
			
			for (Employee employee : loader.getResources()) {
				employee.setEffort(100.0f);
				employee.setProjectId(projectId);
				session.persist(employee);
			}

			Calendar cal = new GregorianCalendar();
			cal.setTime(new Date(System.currentTimeMillis()));
			cal.set(Calendar.HOUR_OF_DAY, 9);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			
			Date initialDate = cal.getTime();
			cal.add(Calendar.MONTH, 2);
			Date deadline = cal.getTime();
			
			Release release = new Release("Release Complete Case", "Release Test", initialDate, deadline, projectId); 
			release.setResources(loader.getResources());
			release.setRequirements(loader.getRequirements());
			
			//RePlan features.
			Requirement reqReplan = new Requirement("RePlan 1", 7, 4, deadline, 1, projectId);
	 		reqReplan.addSkill(skills.get(9));
	 		reqReplan.setProjectId(projectId);
	 		
			session.persist(reqReplan);
			
			Requirement reqReplan2 = new Requirement("RePlan 2", 7, 4, deadline, 1, projectId);
	 		reqReplan2.addSkill(skills.get(0));
	 		reqReplan2.addSkill(skills.get(5));
	 		reqReplan2.addSkill(skills.get(8));
	 		reqReplan2.setProjectId(projectId);
	 		session.persist(reqReplan2);
		
			session.persist(release);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return project;
	}
    
    private Project createUnfeasibleCaseTest(Session session) {
    	Project project = new Project("Unfeasible Case Test", "Lorem Ipsum", new Date(System.currentTimeMillis()), 1);
    	session.save(project);
    	
    	long projectId = project.getId();
    	
    	try {
	    	String inputPath = System.getProperty("user.dir") + "/src/test/resources/instances/"; 
	        String nameFile = "inst10-5-10-5.conf";
	        
	        Properties properties = new Properties();
	        FileInputStream fileInputStream = new FileInputStream(inputPath + nameFile);
	        
	        properties.load(fileInputStream);
	        
	        ProblemInstanceTranslator pit = new ProblemInstanceTranslator(properties);
	    
			WebLoader loader = new WebLoader(2);
			loader.setProjectId(projectId);
			
			loader.createEmployees(new ArrayList<Employee>(pit.getEmployees()));
			loader.createFeatures(new ArrayList<Requirement>(pit.getTasks()));
			loader.setStartDate(new Date(System.currentTimeMillis()));
			
			for (Skill sk : pit.getSkills()) {
				sk.setProjectId(projectId);
				session.persist(sk);
			}
	    	
			for (Requirement req : loader.getRequirements()) {
				req.setProjectId(projectId);
				req.setMaxEmployees(1);
				session.persist(req);
			}
			
			for (Employee employee : loader.getResources()) {
				employee.setEffort(100.0f);
				employee.setProjectId(projectId);
				session.persist(employee);
			}

			Calendar cal = new GregorianCalendar();
			cal.setTime(new Date(System.currentTimeMillis()));
			cal.set(Calendar.HOUR_OF_DAY, 9);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			
			Date initialDate = cal.getTime();
			cal.add(Calendar.MONTH, 2);
			Date deadline = cal.getTime();
			
			
			Release release = new Release("Release Unfeasible Case", "Release Test", initialDate, deadline, projectId); 
			release.setResources(loader.getResources());
			release.setRequirements(loader.getRequirements());
			
			session.merge(release);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return project;
	}

	private Project createDependenciesTest(Session session) {
		Project project = new Project("Dependencies Test", "Dependencies", new Date(System.currentTimeMillis()), 1);
		session.save(project);
    	
		long projectId = project.getId();
		
		WebLoader loader = new WebLoader(2);
		loader.setProjectId(projectId);
		
		Skill PL = new Skill("Planning", "PL", "Planning", projectId);
		
		Date initialDate = new Date(System.currentTimeMillis());
		Calendar cal = new GregorianCalendar();
		cal.setTime(initialDate);
		cal.add(Calendar.MONTH, 2);
		Date deadline = cal.getTime();
		
		Requirement req0 = new Requirement("Feature 1", 7, 3, deadline, 1, projectId);
		req0.addSkill(PL);
		Requirement req1 = new Requirement("Feature 2", 5, 3, deadline, 1, projectId);
		req1.addSkill(PL);
		Requirement req2 = new Requirement("Feature 3", 4, 3, deadline, 1, projectId);
		req2.addSkill(PL);
		Requirement req3 = new Requirement("Feature 4", 3, 3, deadline, 1, projectId);
		req3.addSkill(PL);
		Requirement req4 = new Requirement("Feature 5", 5, 3, deadline, 1, projectId);
		req4.addSkill(PL);
		
		req1.addPredecessor(req0);
		req2.addPredecessor(req0);
		req2.addPredecessor(req1);
		req4.addPredecessor(req1);
		
		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { req0, req1, req2, req3, req4 }));
		
		Employee alice = new Employee("Alice", 100, projectId);
		alice.addSkill(PL);
		
		Employee bob = new Employee("Bob", 100, projectId);
		bob.addSkill(PL);

		Employee charlie = new Employee("Charlie", 100, projectId);
		charlie.addSkill(PL);
		
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { alice, bob, charlie}));
		
		loader.createEmployees(employees);
		loader.createFeatures(reqs);
		loader.setStartDate(new Date(System.currentTimeMillis()));
		
		commitProjectElements(loader, session, "Dependencies");
		
		return project;
	}

	private Project createCapabilitiesTest(Session session) {
		Project project = new Project("Skills Test", "Capabilities", new Date(System.currentTimeMillis()), 1);
		session.save(project);
    	
		long projectId = project.getId();

		WebLoader loader = new WebLoader(2);
		loader.setProjectId(projectId);
		
		Skill PL = new Skill("Planning", "PL", "Planning", projectId);
		Skill AR = new Skill("Architecture", "AR", "Architecture", projectId);
		Skill DE = new Skill("Decision", "DE", "Decision", projectId);
		
		Date initialDate = new Date(System.currentTimeMillis());
		Calendar cal = new GregorianCalendar();
		cal.setTime(initialDate);
		cal.add(Calendar.MONTH, 2);
		Date deadline = cal.getTime();
		
		Requirement req0 = new Requirement("Feature 1", 7, 2, deadline, 1, projectId);
		req0.addSkill(PL);
		req0.addSkill(AR);
		req0.addSkill(DE);
		Requirement req1 = new Requirement("Feature 2", 5, 2, deadline, 1, projectId);
		req1.addSkill(PL);
		Requirement req2 = new Requirement("Feature 3", 4, 2, deadline, 1, projectId);
		req2.addSkill(AR);
		Requirement req3 = new Requirement("Feature 4", 5, 2, deadline, 1, projectId);
		req3.addSkill(AR);
		req3.addSkill(DE);
		Requirement req4 = new Requirement("Feature 5", 4, 2, deadline, 1, projectId);
		req4.addSkill(PL);
		
		
		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { req0, req1, req2, req3, req4 }));
		
		Employee alice = new Employee("Alice", 100, projectId);
		alice.addSkill(PL);
		alice.addSkill(AR);
		alice.addSkill(DE);
		
		Employee bob = new Employee("Bob", 100, projectId);
		bob.addSkill(PL);
		
		Employee charlie = new Employee("Charlie", 100, projectId);
		charlie.addSkill(DE);
		
		Employee david = new Employee("David", 100, projectId);
		david.addSkill(PL);
		david.addSkill(AR);
		
		Employee emma = new Employee("Emma", 100, projectId);
		emma.addSkill(PL);
		
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { alice, bob}));
		
		loader.createEmployees(employees);
		loader.createFeatures(reqs);
		loader.setStartDate(new Date(System.currentTimeMillis()));
		
		commitProjectElements(loader, session, "Skills");
		
		return project;
	}

	private Project createPriorityTest(Session session) {
    	Project project = new Project("Priority Test", "Priority", new Date(System.currentTimeMillis()), 1);
        
        session.save(project);
    	
		long projectId = project.getId();

    	WebLoader loader = new WebLoader(2);
		loader.setProjectId(projectId);
    	
		Skill PL = new Skill("Planning", "PL", "Planning", projectId);
		Skill AR = new Skill("Architecture", "AR", "Architecture", projectId);
		
		Date initialDate = new Date(System.currentTimeMillis());
		Calendar cal = new GregorianCalendar();
		cal.setTime(initialDate);
		cal.set(Calendar.HOUR_OF_DAY, 9);
		cal.add(Calendar.MONTH, 2);
		Date deadline = cal.getTime();
		
		Requirement req0 = new Requirement("Feature 0", 8, 1, deadline, 1, projectId);
		req0.addSkill(PL);
		Requirement req1 = new Requirement("Feature 1", 4, 4, deadline, 1, projectId);
		req1.addSkill(PL);
		Requirement req2 = new Requirement("Feature 2", 4, 3, deadline, 1, projectId);
		req2.addSkill(PL);
		
		//req0.addPredecessor(req2);
		
		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { req0, req1, req2 }));
		
		Employee alice = new Employee("Alice", 100, projectId);
		alice.addSkill(PL);
		
		Employee bob = new Employee("Bob", 100, projectId);
		bob.addSkill(PL);
		
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { alice, bob}));
		
		loader.createEmployees(employees);
		loader.createFeatures(reqs);
		loader.setStartDate(new Date(System.currentTimeMillis()));
		
		commitProjectElements(loader, session, "Priority");
		

		
		return project;
	}

   
    private void commitProjectElements(WebLoader loader, Session session, String releaseName) {
    	
    	Set<Skill> skills = new HashSet<Skill>();
    	
    	for (Employee employee : loader.getResources()) {
    		skills.addAll(employee.getSkills());
    	}
    	
		for (Skill sk : skills) {
			session.persist(sk);
		}
    	
		for (Requirement req : loader.getRequirements()) {
			session.persist(req);
		}
		
		for (Employee employee : loader.getResources()) {
			employee.setEffort(100.0f);
			session.persist(employee);
		}

		Calendar cal = new GregorianCalendar();
		cal.setTime(new Date(System.currentTimeMillis()));
		cal.set(Calendar.HOUR_OF_DAY, 9);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		
		Date initialDate = cal.getTime();
		cal.add(Calendar.MONTH, 2);
		Date deadline = cal.getTime();
		
		
		Release release = new Release("Release " + releaseName, "Release Test", initialDate, deadline, loader.getProjectId()); 
		release.setResources(loader.getResources());
		release.setRequirements(loader.getRequirements());
		
//		//RePlan features.
//		if (releaseName.equals("Priority")) {
//			Requirement reqReplan = new Requirement("RePlan 1", 5, 1, deadline, 1, 1L);
//			
//			List<Skill> skillsReplan = new ArrayList<Skill>(skills);
//	 		reqReplan.addSkill(skillsReplan.get(0));
//	 		//reqReplan.setProjectId(0);
//	 		
//			session.persist(reqReplan);
//			
//			Requirement reqReplan2 = new Requirement("RePlan 2", 4, 2, deadline, 1, 1L);
//	 		reqReplan2.addSkill(skillsReplan.get(0));
//	 		//reqReplan2.setProjectId(projectId);
//	 		session.persist(reqReplan2);
//		}
 		
		session.persist(release);
    }
}
