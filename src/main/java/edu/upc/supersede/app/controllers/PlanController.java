package edu.upc.supersede.app.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.upc.supersede.app.dto.PlanDTO;
import edu.upc.supersede.app.model.Plan;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.services.PlanService;

@Controller
@RequestMapping("plans")
public class PlanController {
	Logger LOGGER = Logger.getLogger(PlanController.class);

	@Autowired
	private PlanService planService;

	/**
     * search Plans for the current project.
    *
    *
    * @param principal  - the current logged in user
    * @return - @see PlanDTO with the project's plans
    */
   @ResponseBody
   @ResponseStatus(HttpStatus.OK)
   @RequestMapping(method = RequestMethod.GET)
   public List<PlanDTO> searchPlansByReleaseId(Principal principal) {
       return planService.findPlansByReleaseId(principal.getName());
   }
	
	/**
	 *
	 * deletes a plan
	 *
	 * @param deletedPlanId
	 *            - the id of the plan to be deleted
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.DELETE)
	public void deletePlan(@RequestBody List<Long> deletedPlanIds) {
		planService.deletePlans(deletedPlanIds);
	}

	/**
	 *
	 * generates a plan plan
	 *
	 * 
	 */
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/generatePlan/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> generatePlan(Principal principal,
			@RequestParam(value = "releaseId", required = false) Long releaseId) {
		
		try {
			List<Plan> plans = planService.generatePlan(principal.getName(), releaseId); 
			List<PlanDTO> dtoPlans = new ArrayList<PlanDTO>();
			
			if (plans.size() > 0) {
				planService.deletePlanByRelease(releaseId);
			}
			
			for (Plan plan : plans) {
				planService.savePlan(principal.getName(), plan);	
		    	dtoPlans.add(Plan.mapPlanDTO(plan));  	
			}
			
			return new ResponseEntity<>(dtoPlans, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("[\""+e.getMessage()+"\"]", HttpStatus.BAD_REQUEST);
		}
	}
	
	
	
	/**
	 *
	 * error handler for backend errors - a 400 status code will be sent back,
	 * and the body of the message contains the exception text.
	 *
	 * @param exc
	 *            - the exception caught
	 */

	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> errorHandler(Exception exc) {
		LOGGER.error(exc.getMessage(), exc);
		return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
	}
}
