package edu.upc.supersede.app.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import edu.upc.supersede.app.dto.EmployeeDTO;
import edu.upc.supersede.app.dto.PlanDTO;
import edu.upc.supersede.app.dto.ReleaseDTO;
import edu.upc.supersede.app.dto.RequirementDTO;
import edu.upc.supersede.app.dto.SkillDTO;
import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.Release;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.model.Skill;
import edu.upc.supersede.app.services.ReleaseService;

/**
 *
 * REST service for releases - allows to update, create and search for releases
 * for the currently logged in user.
 *
 */

@Controller
@RequestMapping("releases")
public class ReleaseController {

	Logger LOGGER = Logger.getLogger(ReleaseController.class);

	@Autowired
	private ReleaseService releaseService;

	/**
     * search Releases for the current project.
    *
    *
    * @param principal  - the current logged in user
    * @return - @see ReleaseDTO with the project's releases
    */
   @ResponseBody
   @ResponseStatus(HttpStatus.OK)
   @RequestMapping(method = RequestMethod.GET)
   public List<ReleaseDTO> searchReleasesByProjectId(Principal principal, Long projectId) {

       SearchResult<Release> result = releaseService.findReleasesByProjectId(principal.getName(), projectId);
       
       return ReleaseDTO.mapFromReleasesEntities(result.getResult());
   }
   
	/**
    * search concrete Release for the current project.
   *
   *
   *
   *
   * @param principal  - the current logged in user
   * @return - @see ReleaseDTO with the release
   */
   @ResponseBody
   @ResponseStatus(HttpStatus.OK)
   @RequestMapping(value="/search/", method = RequestMethod.GET)
   public ReleaseDTO searchReleaseById(Principal principal, 
   		@RequestParam(value = "releaseId", required = false) Long releaseId) {

       Release result = releaseService.findReleaseById(principal.getName(), releaseId);
       
       //List<Release> releases = releaseService.findAllReleases(principal.getName()); 
       
       //LOGGER.warn("Numb resources: " + releases.get(0).getResources().size());
       
  	   return ReleaseDTO.mapFromReleaseEntity(result);
   }
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST)
	public ReleaseDTO saveRelease(Principal principal,
			@RequestBody ReleaseDTO release) {

		Release savedRelease = releaseService.saveRelease(principal.getName(),
				release.getId(), release.getName(), release.getDescription(), 
				release.getInitialDate(), release.getDeadline(),
				release.getResources(), release.getRequirements());
		return ReleaseDTO.mapFromReleaseEntity(savedRelease);
	}
	
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/saveReqs", method = RequestMethod.POST)
	public ReleaseDTO saveReleaseRequirements(Principal principal,
			@RequestBody ReleaseDTO release) {

		Release savedRelease = releaseService.saveReleaseRequirements(principal.getName(),
				release.getId(), release.getRequirements());
		
		return ReleaseDTO.mapFromReleaseEntity(savedRelease);
	}
	
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/updateRequirements/{idRelease}", method = RequestMethod.PUT)
	public ReleaseDTO setReleaseRequirements(Principal principal,
			@PathVariable(value="idRelease") long idRelease,
			@RequestBody Long[] requirements) {

		List<Long> reqs = new ArrayList<Long>(Arrays.asList(requirements));
		
		Release updatedRelease = releaseService.addRequirements(principal.getName(), idRelease, reqs);
		
		return ReleaseDTO.mapFromReleaseEntity(updatedRelease);
	}

	/**
	 *
	 * deletes a release
	 *
	 * @param deletedReleaseId
	 *            - the id of the release to be deleted
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.DELETE)
	public void deleteRelease(@RequestBody Long deletedReleaseId) {
		releaseService.deleteRelease(deletedReleaseId);
	}

	/**
	 *
	 * error handler for backend errors - a 400 status code will be sent back,
	 * and the body of the message contains the exception text.
	 *
	 * @param exc
	 *            - the exception caught
	 */

	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> errorHandler(Exception exc) {
		LOGGER.error(exc.getMessage(), exc);
		return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
	}
}
