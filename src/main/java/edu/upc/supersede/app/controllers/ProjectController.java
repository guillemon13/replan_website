package edu.upc.supersede.app.controllers;

import java.security.Principal;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import edu.upc.supersede.app.dto.ProjectDTO;
import edu.upc.supersede.app.model.Project;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.services.ProjectService;

/**
 *
 * REST service for projects - allows to update, create and search for projects
 * for the currently logged in user.
 *
 */

@Controller
@RequestMapping("projects")
public class ProjectController {

	Logger LOGGER = Logger.getLogger(ProjectController.class);

	@Autowired
	private ProjectService projectService;

	/**
	 * search Project for the current project.
	 *
	 *
	 * @param principal
	 *            - the current logged in user
	 * @return - @see ProjectDTO with the user's projects.
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/projects/id/{projectId}", method = RequestMethod.GET)
	public List<ProjectDTO> searchProjectByID(Principal principal, @PathVariable("projectId") Long projectId) {

		SearchResult<Project> result = projectService.findProjectById(principal.getName(), projectId);
		
		return ProjectDTO.mapFromProjectsEntities(result.getResult());
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST)
	public ProjectDTO saveProject(Principal principal,
			@RequestBody ProjectDTO project) {

		Project savedProject = projectService.saveProject(principal.getName(),
				project.getId(), project.getName(), project.getDescription(),
				project.getDefaultEmployees(), project.getReleases());
		
		return ProjectDTO.mapFromProjectEntity(savedProject);
	}

	/**
	 *
	 * deletes a project
	 *
	 * @param deletedProjectId
	 *            - the id of the project to be deleted
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.DELETE)
	public void deleteProject(@RequestBody Long deletedProjectId) {
		projectService.deleteProject(deletedProjectId);
	}

	/**
	 *
	 * error handler for backend errors - a 400 status code will be sent back,
	 * and the body of the message contains the exception text.
	 *
	 * @param exc
	 *            - the exception caught
	 */

	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> errorHandler(Exception exc) {
		LOGGER.error(exc.getMessage(), exc);
		return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
	}
}
