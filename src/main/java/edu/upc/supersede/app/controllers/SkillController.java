package edu.upc.supersede.app.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import edu.upc.supersede.app.dto.SkillDTO;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.model.Skill;
import edu.upc.supersede.app.services.SkillService;

import java.security.Principal;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * REST Service for Skills
 */

@Controller
@RequestMapping("/skills")
public class SkillController {

	private static final Logger LOGGER = Logger
			.getLogger(SkillController.class);

	@Autowired
	private SkillService skillService;

    /**
     * search Skills for the current project.
     *
     *
     * @param principal  - the current logged in user
     * @return - @see SkillsDTO with the project's skills
     */
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/search/", method = RequestMethod.GET)
    public List<SkillDTO> searchSkillsByProjectId(Principal principal,
    		@RequestParam(value = "projectId", required = false) Long projectId) {

        SearchResult<Skill> result = skillService.getProjectSkills(principal.getName(), projectId);
        
        if (!result.getResult().isEmpty())
        	return SkillDTO.mapFromSkillsEntities(result.getResult());
        else 
        	return new ArrayList<SkillDTO>();
    }
	
	/**
	 *
	 * saves a list of meals - they be either new or existing
	 *
	 * @param principal
	 *            - the current logged in user
	 * @param meals
	 *            - the list of meals to save
	 * @return - an updated version of the saved meals
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST)
	public SkillDTO saveSkill(Principal principal, @RequestBody SkillDTO skill) {

		Skill savedskill = skillService.saveSkill(principal.getName(),
				skill.getId(), skill.getName(), skill.getAbbr(), skill.getDescription());
		return SkillDTO.mapFromSkillEntity(savedskill);
	}

	/**
	 *
	 * deletes a skill
	 *
	 * @param deletedSkillId
	 *            - the ids of the skills to be deleted
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.DELETE)
	public void deleteSkills(@RequestBody Long deletedSkillId) {
		skillService.deleteSkill(deletedSkillId);
	}

	/**
	 *
	 * error handler for backend errors - a 400 status code will be sent back,
	 * and the body of the message contains the exception text.
	 *
	 * @param exc
	 *            - the exception caught
	 */

	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> errorHandler(Exception exc) {
		LOGGER.error(exc.getMessage(), exc);
		return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
	}
}
