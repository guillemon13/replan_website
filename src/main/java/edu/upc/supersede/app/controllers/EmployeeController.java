package edu.upc.supersede.app.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import edu.upc.supersede.app.dto.EmployeeDTO;
import edu.upc.supersede.app.dto.SkillDTO;
import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.model.Skill;
import edu.upc.supersede.app.services.EmployeeService;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * REST service for employees - allows to update, create and search for
 * employees for the currently logged in user.
 *
 */
@Controller
@RequestMapping("/employees")
public class EmployeeController {

	Logger LOGGER = Logger.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeService employeeService;

    /**
     * search Employees for the current project.
     *
     *
     * @param principal  - the current logged in user
     * @return - @see EmployeeDTO with the project's employees
     */
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="/search/", method = RequestMethod.GET)
    public List<EmployeeDTO> searchEmployeesByProjectId(Principal principal, 
    		@RequestParam(value = "projectId", required = false) Long projectId) {

        SearchResult<Employee> result = employeeService.findEmployeesByProjectId(principal.getName(), projectId);

        return EmployeeDTO.mapFromEmployeesEntities(result.getResult());
    }
	
	private EmployeeDTO saveEmployee(Principal principal,
			@RequestBody EmployeeDTO employee) {

		Employee savedEmployee = employeeService.saveEmployee(
				principal.getName(), employee.getId(), employee.getName(),
				employee.getEffort(), employee.getSkills());
		return EmployeeDTO.mapFromEmployeeEntity(savedEmployee);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST)
	public List<EmployeeDTO> saveEmployees(Principal principal,
			@RequestBody List<EmployeeDTO> employees) {

        List<Employee> savedEmployees = employeeService.saveEmployees(principal.getName(), employees);

        return savedEmployees.stream()
                .map(EmployeeDTO::mapFromEmployeeEntity)
                .collect(Collectors.toList());
	}
	
	/**
	 *
	 * deletes a employee
	 *
	 * @param deletedEmployeeId
	 *            - the id of the employee to be deleted
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.DELETE)
	public void deleteEmployee(@RequestBody List<Long> deletedEmployeeIds) {
		employeeService.deleteEmployees(deletedEmployeeIds);
	}

	/**
	 *
	 * error handler for backend errors - a 400 status code will be sent back,
	 * and the body of the message contains the exception text.
	 *
	 * @param exc
	 *            - the exception caught
	 */

	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> errorHandler(Exception exc) {
		LOGGER.error(exc.getMessage(), exc);
		return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
	}
}
