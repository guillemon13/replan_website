package edu.upc.supersede.app.controllers;

import java.security.Principal;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import edu.upc.supersede.app.dto.RequirementDTO;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.services.RequirementService;

/**
 *
 * REST service for requirements - allows to update, create and search for
 * requirements for the currently logged in user.
 *
 */

@Controller
@RequestMapping("requirements")
public class RequirementController {

	Logger LOGGER = Logger.getLogger(RequirementController.class);

	@Autowired
	private RequirementService requirementService;

	
    /**
    *
    *
    * @param principal  - the current logged in user
    * @return - @see SkillsDTO with the project's skills
    */
   @ResponseBody
   @ResponseStatus(HttpStatus.OK)
   @RequestMapping(method = RequestMethod.GET)
   public RequirementDTO searchRequirementById(Principal principal, 
   		@RequestParam(value = "requirementId", required = true) Long requirementId) {

   	   Requirement result = requirementService.findRequirementsById(principal.getName(), requirementId);

       return RequirementDTO.mapFromRequirementEntity(result);
   }
   
   
    /**
     *
     *
     * @param principal  - the current logged in user
     * @return - @see project's all requirements.
     */
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="/search/", method = RequestMethod.GET)
    public List<RequirementDTO> searchRequirementsByProjectId(Principal principal, 
    		@RequestParam(value = "projectId", required = true) Long projectId) {

    	SearchResult<Requirement> result = requirementService.findRequirementsByProjectId(principal.getName(), projectId);

    	
    	
        return RequirementDTO.mapFromRequirementsEntities(result.getResult());
    }
    
    /**
    * @param principal  - the current logged in user
    * @return - @see project's all requirements.
    */
   @ResponseBody
   @ResponseStatus(HttpStatus.OK)
   @RequestMapping(value="/searchAvailable/", method = RequestMethod.GET)
   public List<RequirementDTO> searchAvailableRequirementsByProjectId(Principal principal, 
   		@RequestParam(value = "projectId", required = true) Long projectId) {

   	   SearchResult<Requirement> result = requirementService.findAvailableRequirementsByProjectId(principal.getName(), projectId);

       return RequirementDTO.mapFromRequirementsEntities(result.getResult());
   }
    
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST)
	public RequirementDTO saveRequirement(Principal principal,
			@RequestBody RequirementDTO requirement) {

		Requirement savedRequirement = requirementService.saveRequirement(
				principal.getName(), requirement.getId(),
				requirement.getName(), (double)requirement.getEffort(),
				requirement.getCriteria(), requirement.getDeadline(),
				requirement.getPredecessors(), requirement.getSkills(), 
				requirement.getMaxEmployees());

		return RequirementDTO.mapFromRequirementEntity(savedRequirement);
	}

	/**
	 *
	 * deletes a list of requirements
	 *
	 * @param deletedRequirementIds
	 *            - the ids of the meals to be deleted
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.DELETE)
	public void deleteRequirements(@RequestParam(value = "reqId") Long id) {
		requirementService.deleteRequirement(id);
	}

	/**
	 *
	 * error handler for backend errors - a 400 status code will be sent back,
	 * and the body of the message contains the exception text.
	 *
	 * @param exc
	 *            - the exception caught
	 */

	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> errorHandler(Exception exc) {
		LOGGER.error(exc.getMessage(), exc);
		return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
	}

}
