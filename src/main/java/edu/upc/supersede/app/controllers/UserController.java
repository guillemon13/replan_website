package edu.upc.supersede.app.controllers;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import edu.upc.supersede.app.dto.NewUserDTO;
import edu.upc.supersede.app.dto.ProjectDTO;
import edu.upc.supersede.app.dto.UserInfoDTO;
import edu.upc.supersede.app.model.Project;
import edu.upc.supersede.app.model.User;
import edu.upc.supersede.app.services.UserService;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *  REST service for users.
 *
 */

@Controller
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = Logger.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public UserInfoDTO getUserInfo(Principal principal) {

        User user = userService.findUserByUsername(principal.getName());

        return user != null ? new UserInfoDTO(user.getUsername(), user.getCurrentProject(), mapProjectsDTO(user.getProjects())) : null;
    }
    
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="/changeProject", method = RequestMethod.GET)
    public UserInfoDTO changeUserProject(Principal principal, 
    		@RequestParam(value = "projectId") Long projectId) {

        User user = userService.updateUserProject(principal.getName(), projectId);
        
        return user != null ? new UserInfoDTO(user.getUsername(), user.getCurrentProject(), mapProjectsDTO(user.getProjects())) : null;
    }

    
    
	@ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<?> createUser(@RequestBody NewUserDTO user) {
		
		try {
			userService.createUser(user.getUsername(), user.getEmail(), user.getPlainTextPassword());
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<String>("[\""+ex.getMessage()+"\"]", HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }


    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="/selectRelease", method = RequestMethod.GET)
    public void selectRelease(Principal principal, 
    		@RequestParam(value = "releaseId") Long releaseId) {
        
        userService.selectRelease(principal.getName(), releaseId);
    }
	
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        LOGGER.error(exc.getMessage(), exc);
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }

    
    private List<ProjectDTO> mapProjectsDTO(List<Project> projects) {
    	List<ProjectDTO> dtos = new ArrayList<ProjectDTO>();
    	
    	for (Project proj : projects) {
    		dtos.add(new ProjectDTO(proj.getId(), proj.getName(), proj.getDescription(), proj.getDateCreation(), proj.getDefaultEmployees()));
    	}
    	
    	return dtos;
	}
}
