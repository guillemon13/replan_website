package edu.upc.supersede.app.services;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.upc.supersede.app.dao.EmployeeRepository;
import edu.upc.supersede.app.dao.SkillRepository;
import edu.upc.supersede.app.dao.UserRepository;
import edu.upc.supersede.app.dto.EmployeeDTO;
import edu.upc.supersede.app.dto.SkillDTO;
import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.model.Skill;
import edu.upc.supersede.app.model.User;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static edu.upc.supersede.app.services.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

/**
 *
 * Business service for Meal-related operations.
 *
 */
@Service
public class EmployeeService {

    private static final Logger LOGGER = Logger.getLogger(EmployeeService.class);

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    SkillRepository skillRepository;
    
    @Autowired
    UserRepository userRepository;

    
    /**
     *
     * searches employees from a project Id.
     *
     * @param projectId - project Id of the user.
     * @return - the found results
     */
    @Transactional(readOnly = true)
    public SearchResult<Employee> findEmployeesByProjectId(String userName, Long projectId) {
		User user = userRepository.findUserByUsername(userName);
		
		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}

        List<Employee> employees = employeeRepository.findEmployeesByProjectId(projectId);

        return new SearchResult<>(employees.size(), employees);
    }

    /**
     *
     * deletes a employee, given her id.
     *
     * @param deletedEmployeeId - The ID of the employee.
     */
    
    @Transactional
    public void deleteEmployee(Long deletedEmployeeId) {
        notNull(deletedEmployeeId, "deletedId is mandatory");
        employeeRepository.delete(deletedEmployeeId);
    }

    /**
     *
     * saves an employee (new or not) into the database.
     *
     * @param username - - the currently logged in user
     * @param id - the database id of the employee
     * @param name - the name of the employee
     * @param effort - the effort of the employee
     * @param skills - list of skills
     * @return - the new version of the employee
     */

    @Transactional
    public Employee saveEmployee(String username, Long id, String name, float effort, List<SkillDTO> skillsDTO) {

        assertNotBlank(username, "username cannot be blank");
        notNull(name, "date is mandatory");
        notNull(effort, "time is mandatory");
        notNull(skillsDTO, "skills are mandatory");

        Employee employee = null;
        User user = userRepository.findUserByUsername(username);
        
        List<Long> ids = skillsDTO.stream().map((skill) -> skill.getId()).collect(Collectors.toList());
        List<Skill> skills = skillRepository.findSkillsById(ids);
        
        if (id != null) {
        	employee = employeeRepository.findEmployeeById(id);

        	employee.setName(name);
        	employee.setEffort(effort);
        	employee.setSkills(skills);
        } else {
        	
        	if (user != null) {
            	employee = employeeRepository.save(new Employee(name, effort, skills, user.getCurrentProject()));
            }
        }

        return employee;
    }
    
    /**
    *
    * saves a list of employees (new or not) into the database
    *
    * @param username - the currently logged in user
    * @param employees - the list of employees to be saved
    * @return - the new versions of the saved employees
    */
   @Transactional
   public List<Employee> saveEmployees(String username, List<EmployeeDTO> employees) {
       return employees.stream()
               .map((employee) -> saveEmployee(
                       username,
                       employee.getId(),
                       employee.getName(),
                       employee.getEffort(), 
                       employee.getSkills()))
               .collect(Collectors.toList());
   }
    
   @Transactional
	public void deleteEmployees(List<Long> deletedEmployeeIds) {
		for (Long id : deletedEmployeeIds) {
			deleteEmployee(id);
		}
	}
   
//    private List<Skill> mapSkillsDTO (List<SkillDTO> skills, Long projectId) {
//    	List<Skill> sks = new ArrayList<Skill>();
//    	
//    	for (SkillDTO sk: skills) {
//    		sks.add(new Skill(sk.getId(), sk.getName(), sk.getAbbr(), sk.getDescription(), projectId));
//    	}
//    	
//    	return sks;
//    }

    
}
