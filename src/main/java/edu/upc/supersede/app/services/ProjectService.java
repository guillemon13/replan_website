package edu.upc.supersede.app.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.upc.supersede.app.dao.ProjectRepository;
import edu.upc.supersede.app.dao.ReleaseRepository;
import edu.upc.supersede.app.dao.UserRepository;
import edu.upc.supersede.app.dto.EmployeeDTO;
import edu.upc.supersede.app.dto.ReleaseDTO;
import edu.upc.supersede.app.dto.RequirementDTO;
import edu.upc.supersede.app.dto.SkillDTO;
import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.Project;
import edu.upc.supersede.app.model.Release;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.model.Skill;
import edu.upc.supersede.app.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static edu.upc.supersede.app.services.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

/**
 *
 * Business service for Meal-related operations.
 *
 */
@Service
public class ProjectService {

	private static final Logger LOGGER = Logger.getLogger(ProjectService.class);

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	ReleaseRepository releaseRepository;

	@Autowired
	UserRepository userRepository;

	/**
	 *
	 * searches projects by username
	 *
	 */

	@Transactional(readOnly = true)
	public SearchResult<Project> findUserProjects(String username) {

		User user = userRepository.findUserByUsername(username);

		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}

		List<Project> projects = userRepository.findUserByUsername(user.getUsername()).getProjects();

		return new SearchResult<>(projects.size(), projects);
	}

	/**
	 *
	 * searches projects by ID
	 *
	 */

	@Transactional(readOnly = true)
	public SearchResult<Project> findProjectById(String username, Long id) {

		User user = userRepository.findUserByUsername(username);

		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}

		List<Project> projects = projectRepository.findProjectById(id);

		return new SearchResult<>(projects.size(), projects);
	}

	/**
	 *
	 * deletes a project, given her id.
	 *
	 * @param deletedProjectId
	 *            - The ID of the project.
	 */

	@Transactional
	public void deleteProject(Long deletedProjectId) {
		notNull(deletedProjectId, "deletedId is mandatory");
		projectRepository.delete(deletedProjectId);
	}

	/**
	 *
	 * saves a project (new or not) into the database.
	 */
	
	@Transactional
	public Project saveProject(String username, Long id, String name,
			String description, int defaultEmployees, List<ReleaseDTO> releasesDTO) {

		Project project = null;

		User user = userRepository.findUserByUsername(username);

        List<Long> ids = releasesDTO.stream().map((release) -> release.getId()).collect(Collectors.toList());
        List<Release> releases = releaseRepository.findReleasesById(ids);
		
		if (id != null) {
			project = projectRepository.findProjectById(id).get(0);

			project.setName(name);
			project.setDescription(description);
			project.setDateCreation(new Date());
			project.setReleases(releases);

		} else {

			if (user != null) {
				project = projectRepository.save(new Project(name, description, new Date(System.currentTimeMillis()), defaultEmployees));
				
				if (user.getProjects().isEmpty()) {
					user.setCurrentProject(project.getId());
				}
				
				user.addProject(project);
				LOGGER.warn("A project was attempted to be saved for a non-existing user: " + username);
			}
		}

		return project;
	}
}
