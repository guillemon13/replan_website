package edu.upc.supersede.app.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.upc.supersede.app.dao.UserRepository;
import edu.upc.supersede.app.model.User;

import java.util.regex.Pattern;

import static edu.upc.supersede.app.services.ValidationUtils.*;

/**
 *
 * Business service for User entity related operations
 *
 */

@Service
public class UserService {

    private static final Logger LOGGER = Logger.getLogger(UserService.class);

    private static final Pattern PASSWORD_REGEX = Pattern.compile("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}");

    private static final Pattern EMAIL_REGEX = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    @Autowired
    private UserRepository userRepository;

    /**
     *
     * creates a new user in the database
     *
     * @param username - the username of the new user
     * @param email - the user email
     * @param password - the user plain text password
     */
    @Transactional
    public void createUser(String username, String email, String password) {

        if (!userRepository.isUsernameAvailable(username)) {
            throw new IllegalArgumentException("The username is not available.");
        }
        
        User user = new User(username, new BCryptPasswordEncoder().encode(password), email, 0L);

        userRepository.save(user);
    }

    @Transactional(readOnly = true)
    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }
    
    @Transactional
	public User updateUserProject(String username, Long projectId) {
    	User user = userRepository.findUserByUsername(username);
    	user.setCurrentProject(projectId);
		return user;
	}

    @Transactional
	public void selectRelease(String username, Long releaseId) {
    	User user = userRepository.findUserByUsername(username);
    	user.setSelectedRelease(releaseId);
	}
}
