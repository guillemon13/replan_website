package edu.upc.supersede.app.services;

import static edu.upc.supersede.app.services.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.uma.jmetal.problem.Problem;

import edu.upc.supersede.app.dao.JobRepository;
import edu.upc.supersede.app.dao.PlanRepository;
import edu.upc.supersede.app.dao.ReleaseRepository;
import edu.upc.supersede.app.dao.RequirementRepository;
import edu.upc.supersede.app.dao.UserRepository;
import edu.upc.supersede.app.dto.PlanDTO;
import edu.upc.supersede.app.dto.plan.ChildDTO;
import edu.upc.supersede.app.dto.plan.DependencyDTO;
import edu.upc.supersede.app.dto.plan.DeveloperDTO;
import edu.upc.supersede.app.dto.plan.TaskDTO;
import edu.upc.supersede.app.model.Developer;
import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.Job;
import edu.upc.supersede.app.model.Plan;
import edu.upc.supersede.app.model.Release;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.model.Task;
import edu.upc.supersede.app.model.User;
import edu.upc.supersede.app.model.Requirement.State;
import edu.upc.supersede.plan.generator.customsolution.ProjectSolution;
import edu.upc.supersede.plan.generator.main.KnapsackGenerator;
import edu.upc.supersede.plan.generator.main.KnapsackLoader;
import edu.upc.supersede.plan.generator.main.PSPrioritizedProblem;
import edu.upc.supersede.plan.generator.main.PSProblem;
import edu.upc.supersede.plan.generator.main.PlanGenerator;
import edu.upc.supersede.plan.generator.main.Solver;
import edu.upc.supersede.plan.generator.main.WebLoader;
import edu.upc.supersede.plan.generator.priorprojectsolution.PrioritizedProjectSolution;

@Service
public class PlanService {
	private static final Logger LOGGER = Logger.getLogger(PlanService.class);

	@Autowired
	PlanRepository planRepository;

	@Autowired
	ReleaseRepository releaseRepository;

	@Autowired
	RequirementRepository requirementRepository;

	@Autowired
	JobRepository jobRepository;
	
	@Autowired
	UserRepository userRepository;

	/**
	 *
	 * searches plans from a project Id.
	 *
	 * @param projectId
	 *            - project Id of the user.
	 * @return - the found results
	 */
	@Transactional(readOnly = true)
	public SearchResult<Plan> findPlansByProjectId(String userName,
			Long projectId) {
		User user = userRepository.findUserByUsername(userName);

		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}

		List<Plan> plans = planRepository.findPlansByProjectId(projectId);

		return new SearchResult<>(plans.size(), plans);
	}

	public List<PlanDTO> findPlansByReleaseId(String userName) {
		User user = userRepository.findUserByUsername(userName);

		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}

		List<Plan> plans = planRepository.findPlansByReleaseId(user.getSelectedRelease());

		List<PlanDTO> dtos = new ArrayList<PlanDTO>();
		for (Plan plan : plans) {
			LOGGER.warn("PLAN ID: "+ plan.getReleaseId());
			LOGGER.warn("DEVELOPERS SIZE: " + plan.getDevelopers().size());
			dtos.add(Plan.mapPlanDTO(plan));
		}
		
		return dtos;
	}
	
	/**
	 *
	 * deletes a plan, given her id.
	 *
	 * @param deletedEmployeeId
	 *            - The ID of the employee.
	 */

	@Transactional
	public void deletePlan(Long deletedPlanId) {
		notNull(deletedPlanId, "deletedId is mandatory");
		Plan plan = planRepository.findPlanById(deletedPlanId);
		
		Release release = releaseRepository.findReleaseById(plan.getReleaseId());
		
		planRepository.delete(deletedPlanId);
		
		release.setHasPlans(false);
	}

	@Transactional
	public void deletePlans(List<Long> deletedPlanIds) {
		for (Long id : deletedPlanIds) {
			planRepository.delete(id);	
		}
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public List<Plan> generatePlan(String username, long releaseId) {
        assertNotBlank(username, "username cannot be blank");
        
        Release release = null;
        
        User user = userRepository.findUserByUsername(username);
        release = releaseRepository.findReleaseById(releaseId);
        
        List<Requirement> requirements = release.getRequirements();
        List<Employee> resources = release.getResources();
        
        PlanGenerator nextReleaseGenerator = new PlanGenerator(user.getCurrentProject(), release);
      
        LOGGER.info("Starting generation plan...Creating jobs");
        
		WebLoader loader = new WebLoader(2);
		
		if (release.isHasPlans()) {
			loader.computeInitTime(release.getInitialDate(), new Date(System.currentTimeMillis()));
			System.out.println("INITIAL TIME: " + loader.getInitTime());
		}
		
		loader.createEmployees(new ArrayList<Employee>(resources));
		loader.createFeatures(new ArrayList<Requirement>(requirements));
		loader.createJobs();
		loader.setStartDate(release.getInitialDate());
		loader.setDeadline(release.getDeadline());
		
		if (loader.getRequirements().isEmpty()) {
			String message = "<p>Plan not feasible to be performed.<br>";
			message += "This dependencies are not feasible to be planned:<br>";
			
			Set<Requirement> deps = new HashSet<Requirement>();
			
			for (Requirement req : loader.getNotAffordables()) {
				deps.addAll(req.getSuccessors());
				message += "- " + req.getName() + "<br>";
			}
			
			message += "And dependant features are also unfeasible:<br>";
			for (Requirement req : deps) {
				message += "- " + req.getName() + "<br>";
			}
			
			throw new IllegalArgumentException(message);
		}
		
        nextReleaseGenerator.loadSolver(new Solver());
        
        Problem<PrioritizedProjectSolution> problem = new PSPrioritizedProblem(loader);
        
        List<PrioritizedProjectSolution> solutions = nextReleaseGenerator.generate(problem);

        Solver solver = new Solver();
        
        List<Plan> plans = solver.translatePlanSolutions(solutions, 
        		release.getProjectId(), release.getName(), release);
        
       
        if (plans.size() > 0) {
        	release.setHasPlans(true);
        	
        	Requirement updatedReq;
        	
        	for (Requirement req : solver.getUpdatedRequirements()) {        		
        		updatedReq = release.getRequirementByName(req.getName());
        		
        		if (!updatedReq.isPlanned() || updatedReq.getState().equals(State.TO_DO)) {
        			updatedReq.setStartDate(req.getStartDate());
            		updatedReq.setPlanned(true);
            		updatedReq.setEndDate(req.getEndDate());
            		
            		List<Job> assignedJobs = new ArrayList<Job>();
            		
            		for (Job job : req.getJobs()) {
            			if (job.getDedication() > 0) {
            				job.clearDependencies();
            				job.setRequirement(release.getRequirementByName(job.getRequirement().getName()));
            				job.setEmployee(release.getEmployeeByName(job.getEmployee().getName()));
            			
            				jobRepository.save(job);
            				assignedJobs.add(job);
            			}
            		}
            		
            		updatedReq.setJobs(assignedJobs);
            		
            		if (!req.getJobs().isEmpty()) {
    	        		LOGGER.warn("REQ " + updatedReq.getName());
    	        		LOGGER.warn("REQ Job " + updatedReq.getJobs().size());
    	        		LOGGER.warn("REQ " + updatedReq.getStartTime());
    	        		LOGGER.warn("REQ " + updatedReq.getEndTime());
    	        		LOGGER.warn("REQ " + updatedReq.getStartDate());
    	        		LOGGER.warn("REQ " + updatedReq.getEndDate());
            		}
        		}
        		
        	}
        }
        	
        LOGGER.info("DONE"); 
        
        for (Requirement req : release.getRequirements()) {
        	if (!req.isPlanned()) {
        		req.setStartDate(null);
    			req.setEndDate(null);
    			req.clearJobs();
        	}
        }
        
        
        for (Plan plan : plans) {
        	for (Developer dev : plan.getDevelopers()) {
        		for (Task task : dev.getTasks()) {
        			LOGGER.warn(task.getName() + ": " + task.getFromDate() + " - " + task.getToDate());
        		}
        	}
        }
          
        //createRePlan(username, releaseId);
        
        return plans;
	}
	
	
	public Plan rePlan (long releaseId) {
		Release release = null;
        
        release = releaseRepository.findReleaseById(releaseId);
		
        Plan releasePlan = planRepository.findPlansByReleaseId(releaseId).get(0);

        List<Developer> developers = releasePlan.getDevelopers();
        
        boolean replan0 = false;
        boolean replan1 = false;
        
        Task replanTask = null;
        Task replanTask1 = null;
        for (Developer dev : developers) {
        	for (Task task : dev.getTasks()) {
        		if (task.getName().equals("Feature 0")) {
        			Calendar cal = new GregorianCalendar();
        			cal.setTime(task.getToDate());
        			cal.add(Calendar.HOUR_OF_DAY, 5);
        			replanTask = new Task(dev.getName() + "-" + "RePlan 0", "RePlan 0", task.getToDate(), cal.getTime(), 4, release.getProjectId());
        			
        			replan0 = true;
        			break;
        		} else if (task.getName().equals("Feature 1")) {
        			Calendar cal = new GregorianCalendar();
        			cal.setTime(task.getToDate());
        			cal.add(Calendar.HOUR_OF_DAY, 4);
        			replanTask1 = new Task(dev.getName() + "-" + "RePlan 1", "RePlan 1", task.getToDate(), cal.getTime(), 4, release.getProjectId());
        			
        			replan1 = true;
        			break;
        		}
        	}
        	
        	if (replan0) {
        		dev.addTask(replanTask);
        		replan0 = false;
        	} else if (replan1) {
        		dev.addTask(replanTask1);
        		replan0 = false;
        	}
        }
        
        return releasePlan;
	}
	

	
	private List<Requirement> initializeData(List<Requirement> requirements) {
		List<Requirement> copiedReqs = new ArrayList<Requirement>();
		
		Map<Integer, Requirement> mapReqs = new HashMap<Integer,Requirement>();

		for (Requirement req : requirements) {
			Requirement copiedReq = new Requirement(req);
			if (req.isPlanned()) {
				copiedReq.setJobs(req.getJobs());
			}
			
			copiedReqs.add(copiedReq);
			mapReqs.put(req.getInternalId(), copiedReq);
		}
		
		for (Requirement req : requirements) {
			for (Requirement succ : req.getSuccessors()) {
				mapReqs.get(req.getInternalId()).addSuccessor(mapReqs.get(succ.getInternalId()));
			}
			
			for (Requirement pred : req.getPredecessors()) {
				mapReqs.get(req.getInternalId()).addPredecessorRelation(mapReqs.get(pred.getInternalId()));
			}
		}
		
		return copiedReqs;
	}
	
	/**
	 *
	 * saves a plan (new or not) into the database.
	 *
	 * @return - the new version of the plan
	 */

	@Transactional
	public Plan savePlan(String username, Plan plan) {

		User user = userRepository.findUserByUsername(username);
		Long id = plan.getId();
		
		if (id != null) {
			plan = planRepository.findPlanById(id);

			plan.setName(plan.getName());
			plan.setReleaseDate(plan.getReleaseDate());
			plan.setDevelopers(plan.getDevelopers());
		} else {
			if (user != null) {
				plan = planRepository.save(plan);
				LOGGER.info("Plan " + plan.getId() + " saved OK!");
			}
		}

		return plan;
	}

	@Transactional
	public void deletePlanByRelease(Long releaseId) {
		planRepository.deleteByRelease(releaseId);
	}
}
