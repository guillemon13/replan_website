package edu.upc.supersede.app.services;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.upc.supersede.app.dao.SkillRepository;
import edu.upc.supersede.app.dao.UserRepository;
import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.model.Skill;
import edu.upc.supersede.app.model.User;
import static edu.upc.supersede.app.services.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

/**
 *
 * Business service for Skill-related operations.
 *
 */
@Service
public class SkillService {

	private static final Logger LOGGER = Logger.getLogger(SkillService.class);

	@Autowired
	SkillRepository skillRepository;

	@Autowired
	UserRepository userRepository;

	
	/**
	 *
	 * retrieves all existing skills.
	 *
	 * @return - the found results
	 */
	@Transactional(readOnly = true)
	public SearchResult<Skill> findAllSkills(String username) {
		User user = userRepository.findUserByUsername(username);
		
		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}

		List<Skill> skills = skillRepository.getAllSkills();

		return new SearchResult<>(skills.size(), skills);
	}
	
	/**
	 *
	 * retrieves all project's skills.
	 *
	 * @return - the found results
	 */
	@Transactional(readOnly = true)
	public SearchResult<Skill> getProjectSkills(String username, Long projectId) {
		User user = userRepository.findUserByUsername(username);
		
		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}

		List<Skill> skills = skillRepository.findSkillsByProjectId(projectId);

		return new SearchResult<>(skills.size(), skills);
	}
    
	/**
	 *
	 * deletes a skill, given her id.
	 *
	 * @param deletedEmployeeId
	 *            - The ID of the skill.
	 */

	@Transactional
	public void deleteSkill(Long deletedSkillId) {
		notNull(deletedSkillId, "deletedId is mandatory");
		skillRepository.delete(deletedSkillId);
	}

	/**
	 *
	 * saves an skill (new or not) into the database.
	 *
	 * @param username
	 *            - - the currently logged in user
	 * @param id
	 *            - the database id of the skill
	 * @param name
	 *            - the name of the skill
	 * @param description
	 *            - the description of the skill
	 * @return - the new version of the skill
	 */

	@Transactional
	public Skill saveSkill(String username, Long id, String name, String abbr, String description) {

		assertNotBlank(username, "username cannot be blank");
		notNull(name, "name is mandatory");
		notNull(description, "description is mandatory");

		Skill skill = null;

		if (id != -1) {
			skill = skillRepository.findSkillById(id);

			skill.setName(name);
			skill.setDescription(description);
		} else {
			User user = userRepository.findUserByUsername(username);

			if (user != null) {
				skill = skillRepository.save(new Skill(name, abbr, description, user.getCurrentProject()));
				LOGGER.warn("A skill was attempted to be saved for a non-existing user: "
						+ username);
			}
		}

		return skill;
	}


}
