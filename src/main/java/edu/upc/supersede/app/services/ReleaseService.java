package edu.upc.supersede.app.services;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.upc.supersede.app.dao.EmployeeRepository;
import edu.upc.supersede.app.dao.ReleaseRepository;
import edu.upc.supersede.app.dao.RequirementRepository;
import edu.upc.supersede.app.dao.UserRepository;
import edu.upc.supersede.app.dto.EmployeeDTO;
import edu.upc.supersede.app.dto.PlanDTO;
import edu.upc.supersede.app.dto.RequirementDTO;
import edu.upc.supersede.app.dto.SkillDTO;
import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.Release;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.Requirement.State;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.model.Skill;
import edu.upc.supersede.app.model.User;
import edu.upc.supersede.plan.generator.main.PlanGenerator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import static edu.upc.supersede.app.services.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

/**
 *
 * Business service for Meal-related operations.
 *
 */
@Service
public class ReleaseService {

    private static final Logger LOGGER = Logger.getLogger(ReleaseService.class);

    @Autowired
    ReleaseRepository releaseRepository;

    @Autowired
    RequirementRepository requirementRepository;

    @Autowired
    EmployeeRepository employeeRepository;
    
    @Autowired
    UserRepository userRepository;

    /**
     *
     * searches releases by projectID
     *
	*/
    @Transactional(readOnly = true)
    public SearchResult<Release> findReleasesByProjectId(String username, Long projectId) {

    	User user = userRepository.findUserByUsername(username);
    	
		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}
    	
		for (Release rel : releaseRepository.getAll()) {
			System.out.println("PROJECT REL " + rel.getProjectId());
		}
		
    	List<Release> releases = releaseRepository.findReleasesByProjectId(projectId);
    	
        return new SearchResult<Release>(releases.size(), releases);
    }
    
	public Release findReleaseById(String username, Long releaseId) {
		
		User user = userRepository.findUserByUsername(username);
    	
		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}

    	Release rel = releaseRepository.findReleaseById(releaseId);

        return rel;
	}
	
	
	public List<Release> findAllReleases(String username) {
		
		User user = userRepository.findUserByUsername(username);
    	
		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}
    	
    	return releaseRepository.getAll();
	}
    
    /**
     *
     * deletes a release, given her id.
     *
     * @param deletedReleaseId - The ID of the release.
     */
    
    @Transactional
    public void deleteRelease(Long deletedReleaseId) {
        notNull(deletedReleaseId, "deletedId is mandatory");
        releaseRepository.delete(deletedReleaseId);
    }

    /**
     *
     * saves a release (new or not) into the database.
     *
     * @param username - - the currently logged in user
     * @param id - the database id of the release
     * @param name - the name of the release
     * @param effort - the effort of the release
     * @param skills - list of skills
     * @return - the new version of the release
     */

    @Transactional
    public Release saveRelease(String username, Long id, String name, String description, Date initialDate,
    		Date deadline, List<EmployeeDTO> resourcesDTO, List<RequirementDTO> requirementsDTO) {

        assertNotBlank(username, "username cannot be blank");
        notNull(name, "name is mandatory");
        notNull(description, "description is mandatory");
        notNull(initialDate, "initial date is mandatory");
        notNull(deadline, "deadline is mandatory");
        
        //notNull(resources, "resources are mandatory");
        
        Release release = releaseRepository.findReleaseById(id);

        User user = userRepository.findUserByUsername(username);
        
        List<Requirement> requirements = requirementRepository.
        		findRequirementsByIds(requirementsDTO.stream().map((resource) -> resource.getId()).collect(Collectors.toList()));
        List<Employee> resources = employeeRepository.
        		findEmployeesByIds(resourcesDTO.stream().map((requirement) -> requirement.getId()).collect(Collectors.toList()));
        
        Calendar cal = new GregorianCalendar();
        cal.setTime(initialDate);
        cal.set(Calendar.HOUR_OF_DAY, 9);
        
        initialDate = cal.getTime();
        
        cal.setTime(deadline);
        cal.set(Calendar.HOUR_OF_DAY, 9);
        
        deadline = cal.getTime();
        
        if (id != null) {
        	release = releaseRepository.findReleaseById(id);

        	release.setName(name);
        	release.setDescription(description);
        	release.setInitialDate(initialDate);
        	release.setDeadline(deadline);
        	release.setResources(resources);
        	release.setRequirements(requirements);
        } else {

            if (user != null) {
            	release = releaseRepository.save(new Release(name, description, initialDate, deadline, 
            			resources, 
            			requirements,
            			user.getCurrentProject()));
                LOGGER.warn("A release was attempted to be saved for a non-existing user: " + username);
            }
        }

        return release;
    }
    
    @Transactional
    public Release saveReleaseRequirements(String username, Long id, List<RequirementDTO> requirementsDTO) {

        assertNotBlank(username, "username cannot be blank");
        notNull(id, "name is mandatory");
        
        List<Requirement> requirements = requirementRepository.
        		findRequirementsByIds(requirementsDTO.stream().map((resource) -> resource.getId()).collect(Collectors.toList()));
        
    	Release release = releaseRepository.findReleaseById(id);
    	release.setRequirements(requirements);
    	
    	LOGGER.warn("RELEASE " + id + " saved properly. Resources = " + release.getResources().size());
    	
        return release;
    }
    
    @Transactional
    public Release addRequirements(String username, Long idRelease, List<Long> requirements) {

        assertNotBlank(username, "username cannot be blank");

        Release release = null;

        userRepository.findUserByUsername(username);
        
        if (idRelease != null) {
        	release = releaseRepository.findReleaseById(idRelease);

        	List<Requirement> ids = requirementRepository.findRequirementsByIds(requirements);
        	
        	release.setRequirements(ids);
        	
        	releaseRepository.save(release);
        }

        return release;
    }
    
    @Transactional
    public Release setEmployees(String username, Long idRelease, List<Long> employees) {

        assertNotBlank(username, "username cannot be blank");

        Release release = null;

        userRepository.findUserByUsername(username);
        
        if (idRelease != null) {
        	release = releaseRepository.findReleaseById(idRelease);
        	
        	List<Employee> resources = employeeRepository.findEmployeesByIds(employees);
        	
        	release.setResources(resources);
        	releaseRepository.save(release);
        }

        return release;
    }

}
