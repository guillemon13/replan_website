package edu.upc.supersede.app.services;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.upc.supersede.app.dao.ReleaseRepository;
import edu.upc.supersede.app.dao.RequirementRepository;
import edu.upc.supersede.app.dao.SkillRepository;
import edu.upc.supersede.app.dao.UserRepository;
import edu.upc.supersede.app.dto.RequirementDTO;
import edu.upc.supersede.app.dto.SkillDTO;
import edu.upc.supersede.app.model.Release;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.model.Skill;
import edu.upc.supersede.app.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static edu.upc.supersede.app.services.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

/**
 *
 * Business service for Requirement-related operations.
 *
 */
@Service
public class RequirementService {

    private static final Logger LOGGER = Logger.getLogger(RequirementService.class);

    @Autowired
    RequirementRepository requirementRepository;

    @Autowired
    ReleaseRepository releaseRepository;
    
    @Autowired
    SkillRepository skillRepository;
    
    @Autowired
    UserRepository userRepository;

    @Transactional(readOnly = true)
    public SearchResult<Requirement> findRequirementsByProjectId(String username, Long projectId) {

    	User user = userRepository.findUserByUsername(username);
    	
		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}
    	
    	List<Requirement> reqs = requirementRepository.findRequirementByProjectId(projectId);
    	
        return new SearchResult<>(reqs.size(), reqs);
    }
    
    @Transactional(readOnly = true)
    public SearchResult<Requirement> findAvailableRequirementsByProjectId(
			String username, Long projectId) {
		
    	User user = userRepository.findUserByUsername(username);
    	
		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}
		
		List<Requirement> projectReqs = requirementRepository.findRequirementByProjectId(projectId);
    	
		List<Release> projectReleases = releaseRepository.findReleasesByProjectId(projectId);
		
		List<Requirement> releaseRequirements = new ArrayList<Requirement>();
		
		for (Release release : projectReleases) {
			releaseRequirements.addAll(release.getRequirements());
		}
		
		for (Requirement req : releaseRequirements) {
			LOGGER.warn("NOT AVAILABLE: " + req.getId());
		}
		
		projectReqs.removeAll(releaseRequirements);
		
        return new SearchResult<>(projectReqs.size(), projectReqs);
	}
    
	public Requirement findRequirementsById(String username, Long requirementId) {
    	User user = userRepository.findUserByUsername(username);
    	
		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}
    	
    	Requirement req = requirementRepository.findRequirementById(requirementId);
    	
        return req;
	}
    
    /**
     *
     * deletes a requirement, given her id.
     *
     * @param deletedRequirementId - The ID of the requirement.
     */
    
    @Transactional
    public void deleteRequirement(Long deletedRequirementId) {
        notNull(deletedRequirementId, "deletedId is mandatory");
        requirementRepository.delete(deletedRequirementId);
    }

    /**
     *
     * saves an requirement (new or not) into the database.
     *
     * @param username - - the currently logged in user
     * @param id - the database id of the requirement
     * @param name - the name of the requirement
     * @param effort - the effort of the requirement
     * @param skills - list of skills
     * @return - the new version of the requirement
     */

    @Transactional
    public Requirement saveRequirement(String username, Long id, String name, double effort, int criteria, Date deadline,
    		List<RequirementDTO> predecessors, List<SkillDTO> skillsDTO, int maxEmployees) {

        assertNotBlank(username, "username cannot be blank");
        notNull(name, "name is mandatory");
        notNull(effort, "effort is mandatory");
        notNull(criteria, "criteria is mandatory");
        
        Requirement requirement = null;
        
        List<Long> ids = skillsDTO.stream().map((skill) -> skill.getId()).collect(Collectors.toList());
        List<Skill> skills = skillRepository.findSkillsById(ids);
        
        User user = userRepository.findUserByUsername(username);
        
        if (id != null) {
        	
        	requirement = requirementRepository.findRequirementById(id);

        	requirement.setName(name);
        	requirement.setEffort(effort);
        	requirement.setCriteria(criteria);
        	requirement.setDeadline(deadline);
 
        	for (Requirement predecessor : requirementRepository.findRequirementsByIds
        			(predecessors.stream().map((pred) -> pred.getId()).collect(Collectors.toList()))) {
        		
        		requirement.addPredecessor(predecessor);
        	}
        	
        	requirement.setSkills(skills);
        	
        } else {
            if (user != null) {
            	Requirement req = new Requirement(name, effort, criteria, deadline, maxEmployees, user.getCurrentProject());
            	req.setSkills(skills);
            	
            	if (predecessors != null) {
	            	LOGGER.warn("PRED " + predecessors.size());
	            	
	            	for (Requirement predecessor : requirementRepository.findRequirementsByIds
	            			(predecessors.stream().map((pred) -> pred.getId()).collect(Collectors.toList()))) {
	            		
	            		req.addPredecessor(predecessor);
	            	}
	            	
            	}
            	
            	requirement = requirementRepository.save(req);
                LOGGER.warn("A requirement was attempted to be saved for a non-existing user: " + username);
            }
        }

        return requirement;
    }

    
//    private List<Requirement> mapRequirementsDTO (List<RequirementDTO> requirements, Long projectId) {
//    	List<Requirement> reqs = new ArrayList<Requirement>();
//    	
//    	for (RequirementDTO req: requirements) {
//    		Requirement rq = new Requirement(req.getName(), req.getEffort(), req.getCriteria(), req.getDeadline(), req.getMaxEmployees(), projectId); 
//    		rq.setSkills(mapSkillsDTO(req.getSkills(), projectId));
//    		
//    		if (!req.getSuccessors().isEmpty()) {
//    			rq.setSuccessors(mapRequirementsDTO(req.getSuccessors(), projectId));
//    		}
//    		
//    		reqs.add(rq);
//    	}
//    	
//    	return reqs;
//    }
    
//    private List<Skill> mapSkillsDTO (List<SkillDTO> skills, Long projectId) {
//    	List<Skill> sks = new ArrayList<Skill>();
//    	
//    	for (SkillDTO sk: skills) {
//    		sks.add(new Skill(sk.getId(), sk.getName(), sk.getAbbr(), sk.getDescription(), projectId));
//    	}
//    	
//    	return sks;
//    }


}
