package edu.upc.supersede.app.services;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.upc.supersede.app.dao.JobRepository;
import edu.upc.supersede.app.dao.UserRepository;
import edu.upc.supersede.app.model.Job;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.SearchResult;
import edu.upc.supersede.app.model.User;

import java.util.List;

import static edu.upc.supersede.app.services.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

/**
 *
 * Business service for Meal-related operations.
 *
 */
@Service
public class JobService {

    private static final Logger LOGGER = Logger.getLogger(JobService.class);

    @Autowired
    JobRepository jobRepository;

    @Autowired
    UserRepository userRepository;

    
    /**
     *
     * searches meals by date/time
     *
     * @param username - the currently logged in user
     * @param fromDate - search from this date, including
     * @param toDate - search until this date, including
     * @param fromTime - search from this time, including
     * @param toTime - search to this time, including
     * @param pageNumber - the page number (each page has 10 entries)
     * @return - the found results
     */
    /*@Transactional(readOnly = true)
    public SearchResult<Meal> findMeals(String username, Date fromDate, Date toDate, Time fromTime, Time toTime, int pageNumber) {

        if (fromDate == null || toDate == null) {
            throw new IllegalArgumentException("Both the from and to date are needed.");
        }

        if (fromDate.after(toDate)) {
            throw new IllegalArgumentException("From date cannot be after to date.");
        }

        if (fromDate.equals(toDate) && fromTime != null && toTime != null && fromTime.after(toTime)) {
            throw new IllegalArgumentException("On searches on the same day, from time cannot be after to time.");
        }

        Long resultsCount = mealRepository.countMealsByDateTime(username, fromDate, toDate, fromTime, toTime);

        List<Meal> meals = mealRepository.findMealsByDateTime(username, fromDate, toDate, fromTime, toTime, pageNumber);

        return new SearchResult<>(resultsCount, meals);
    }*/

    @Transactional(readOnly = true)
    public SearchResult<Job> findJobsByProjectId(String username, Long projectId) {

    	User user = userRepository.findUserByUsername(username);
    	
		if (user == null) {
			throw new IllegalArgumentException("The user must be logged.");
		}
    	
    	List<Job> jobs = jobRepository.findJobsByProjectId(projectId);
    	
        return new SearchResult<>(jobs.size(), jobs);
    }
    
    /**
     *
     * deletes a job, given her id.
     *
     * @param deletedJobId - The ID of the job.
     */
    
    @Transactional
    public void deleteJob(Long deletedJobId) {
        notNull(deletedJobId, "deletedId is mandatory");
        jobRepository.delete(deletedJobId);
    }

    /**
     *
     * saves an employee (new or not) into the database.
     *
     * @param username - - the currently logged in user
     * @param id - the database id of the employee
     * @param name - the name of the employee
     * @param effort - the effort of the employee
     * @param skills - list of skills
     * @return - the new version of the employee
     */

    @Transactional
    public Job saveJob(String username, Long id, String name, int devTime, 
    		Requirement requirement, List<Job> successors) {

        assertNotBlank(username, "username cannot be blank");
        notNull(name, "name is mandatory");
        notNull(devTime, "development time is mandatory");
        notNull(requirement, "requirement are mandatory");
        notNull(successors, "requirement are mandatory");
        
        Job job = null;

        if (id != null) {
        	job = jobRepository.findJobById(id);

        	job.setName(name);
        	job.setDevTime(devTime);
        	job.setRequirement(requirement);
        	job.setSuccessors(successors);
        	
        } else {
            User user = userRepository.findUserByUsername(username);

            if (user != null) {
            	job = jobRepository.save(new Job(name, devTime, requirement, successors, user.getCurrentProject()));
                LOGGER.warn("A meal was attempted to be saved for a non-existing user: " + username);
            }
        }

        return job;
    }
}
