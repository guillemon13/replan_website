package edu.upc.supersede.app.dto;

import java.util.List;
import java.util.stream.Collectors;

import edu.upc.supersede.app.model.Skill;


public class SkillDTO {

	private Long id;
	private String name;
	private String abbr;
	private String description;
	
	public SkillDTO () { }

	public SkillDTO (Long id, String name, String abbr, String description) {
		this.id = id;
		this.name = name;
		this.setAbbr(abbr);
		this.description = description;
	}

    public static SkillDTO mapFromSkillEntity(Skill skill) {
        return new SkillDTO(skill.getId(), skill.getName(), skill.getAbbr(), skill.getDescription());
    }

    public static List<SkillDTO> mapFromSkillsEntities(List<Skill> skills) {
        return skills.stream().map((skill) -> mapFromSkillEntity(skill)).collect(Collectors.toList());
    }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getAbbr() {
		return abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}



}
