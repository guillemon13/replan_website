package edu.upc.supersede.app.dto.plan;

import java.util.*;

public class ChildDTO  extends GanttObjectDTO {

	private String name;
	
	private List<TaskDTO> tasks;
	
	public ChildDTO(String name) {
		super(name);
		this.name = name;
		this.tasks = new ArrayList<TaskDTO>();
	}

	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void addTask(TaskDTO task) {
		this.tasks.add(task);
	}
	
	public List<TaskDTO> getTasks() {
		return this.tasks;
	}
}
