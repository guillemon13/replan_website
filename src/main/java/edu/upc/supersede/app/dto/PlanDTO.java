package edu.upc.supersede.app.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import edu.upc.supersede.app.dto.plan.DeveloperDTO;
import edu.upc.supersede.app.dto.plan.GanttObjectDTO;
import edu.upc.supersede.app.model.Developer;
import edu.upc.supersede.app.model.Plan;


public class PlanDTO {
	private Long internalId;
	private Long projectId;
	private String name;
	private Date releaseDate;
	
	private List<GanttObjectDTO> objects;
	
	public PlanDTO() {}
	
	public PlanDTO (Long projectId, String name, Date releaseDate) {
		this.projectId = projectId;
		this.name = name;
		this.releaseDate = releaseDate;
		
		this.objects = new ArrayList<GanttObjectDTO>();	
	}
	
	public PlanDTO (Long projectId, String name, Date releaseDate, List<GanttObjectDTO> objects) {
		this.projectId = projectId;
		this.name = name;
		this.releaseDate = releaseDate;
		
		this.objects = objects;
	}

	//TODO
//    public static PlanDTO mapFromPlanEntity(Plan plan) {
//    	PlanDTO dto = new PlanDTO(plan.getProjectId(), plan.getName(), plan.getReleaseDate()); 
//    	
//    	for (Developer dep : plan.getDevelopers()) {
//    		
//    	}
//    	
//    	
//    	
//    	return dto;
//    }
//
//    public static List<PlanDTO> mapFromPlansEntities(List<Plan> plans) {
//        return plans.stream().map((plan) -> mapFromPlanEntity(plan)).collect(Collectors.toList());
//    }
	
	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public List<GanttObjectDTO> getObjects() {
		return objects;
	}

	public void setObjects(List<GanttObjectDTO> objects) {
		this.objects = objects;
	}

	public void addObject (GanttObjectDTO developer) {
		this.objects.add(developer);
	}
		
	public Long getInternalId() {
		return internalId;
	}

	public void setInternalId(Long internalId) {
		this.internalId = internalId;
	}

	public List<DeveloperDTO> getDevelopers() {
		return null;
	}

}
