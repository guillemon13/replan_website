package edu.upc.supersede.app.dto;

import edu.upc.supersede.app.model.Job;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JobDTO {

	private int internalId;

	private String name;
	
	private double devTime;
	
	private int earlyStart;
	private int latestStart;

	private RequirementDTO requirement;

	private List<JobDTO> successors;
	private List<JobDTO> predecessors;
	
	public JobDTO() {}

	public JobDTO(String name) {
		this.name = name;
		
		this.successors = new ArrayList<JobDTO>();
		this.setPredecessors(new ArrayList<JobDTO>());
	}

	public JobDTO(String name, double devTime, RequirementDTO requirement) {
		this.name = name;
		this.devTime = devTime;
		this.requirement = requirement;
		this.successors = new ArrayList<JobDTO>();
		this.setPredecessors(new ArrayList<JobDTO>());
	}
	
	public JobDTO(String name, double devTime, RequirementDTO requirement, List<JobDTO> successors) {
		this.name = name;
		this.devTime = devTime;
		this.requirement = requirement;
		this.successors = successors;
		this.predecessors = new ArrayList<JobDTO>();
	}
	
    public static JobDTO mapFromJobEntity(Job job) {
    	List<JobDTO> successors = new ArrayList<JobDTO>();
    	List<JobDTO> predecessors = new ArrayList<JobDTO>();
    	
    	JobDTO jobDTO = new JobDTO();
    	
    	if (job.getSuccessors().isEmpty()) {
    		jobDTO = new JobDTO(job.getName(), job.getDevTime(), RequirementDTO.mapFromRequirementEntity(job.getRequirement()));
    	} else {
    		for (Job successor : job.getSuccessors()) {
        		successors.add(mapFromJobEntity(successor));
        	}
        	
        	jobDTO = new JobDTO(job.getName(), job.getDevTime(), RequirementDTO.mapFromRequirementEntity(job.getRequirement()), successors);	
    	}
    	
    	if (job.getPredecessors().isEmpty()) {
    		if (job.getSuccessors().isEmpty())
    			jobDTO = new JobDTO(job.getName(), job.getDevTime(), RequirementDTO.mapFromRequirementEntity(job.getRequirement()));
    		else 
    			return jobDTO;
    	} else {
    		for (Job predecessor : job.getPredecessors()) {
    			predecessors.add(mapFromJobEntity(predecessor));
        	}
        	
    		if (!job.getSuccessors().isEmpty())
    			jobDTO.setPredecessors(predecessors);
    		else {
    			jobDTO = new JobDTO(job.getName(), job.getDevTime(), RequirementDTO.mapFromRequirementEntity(job.getRequirement()));
    			jobDTO.setPredecessors(predecessors);
    		}
    			
    	}
    	
    	return jobDTO;
    }

    public static List<JobDTO> mapFromJobsEntities(List<Job> jobs) {
        return jobs.stream().map((job) -> mapFromJobEntity(job)).collect(Collectors.toList());
    }
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public double getDevTime() {
		return devTime;
	}

	public void setDevTime(double devTime) {
		this.devTime = devTime;
	}
	
	public int getEarlyStart() {
		return earlyStart;
	}

	public void setEarlyStart(int earlyStart) {
		this.earlyStart = earlyStart;
	}

	public int getLatestStart() {
		return latestStart;
	}

	public void setLatestStart(int latestStart) {
		this.latestStart = latestStart;
	}

	public RequirementDTO getRequirement() {
		return requirement;
	}

	public void setRequirement(RequirementDTO requirement) {
		this.requirement = requirement;
	}
	
	public int getInternalId() {
		return this.internalId;
	}

	public List<JobDTO> getSuccessors() {
		return successors;
	}

	public List<JobDTO> getPredecessors() {
		return predecessors;
	}

	public void setPredecessors(List<JobDTO> predecessors) {
		this.predecessors = predecessors;
	}	
	
	
}
