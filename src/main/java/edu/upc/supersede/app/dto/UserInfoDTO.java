package edu.upc.supersede.app.dto;

import java.util.*;

/**
 *
 * JSON-serializable DTO containing user data
 *
 */
public class UserInfoDTO {

    private String userName;
    private Long projectId;
    private List<ProjectDTO> projects;
    	
	public UserInfoDTO(String userName, Long projectId, List<ProjectDTO> projects) {
        this.userName = userName;
        this.projectId = projectId;
        this.setProjects(projects);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public List<ProjectDTO> getProjects() {
		return projects;
	}

	public void setProjects(List<ProjectDTO> projects) {
		this.projects = projects;
	}
	
	
}
