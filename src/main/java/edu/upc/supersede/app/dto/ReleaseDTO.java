package edu.upc.supersede.app.dto;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import edu.upc.supersede.app.model.Release;
import edu.upc.supersede.app.model.Requirement;

public class ReleaseDTO {

	private Long id;
	private String name;
	private String description;
	
	private Date initialDate;
	private Date deadline;

	private boolean hasPlans;

	private List<EmployeeDTO> resources;
	private List<RequirementDTO> requirements;
	
	public ReleaseDTO() {}

	public ReleaseDTO(Long id, String name, String description, Date initialDate, Date deadline, 
			List<EmployeeDTO> resources, List<RequirementDTO> requirements, boolean hasPlans) {
		
		this.id = id;
		this.name = name;
		this.description = description;
		this.setInitialDate(initialDate);
		this.deadline = deadline;

		this.resources = resources;
		this.requirements = requirements;
		
		this.setHasPlans(hasPlans);
	}

    public static ReleaseDTO mapFromReleaseEntity(Release release) {

    	for (Requirement req: release.getRequirements()) {
        	if (req.isPlanned()) {
        		System.out.println("REQ FINISH DATE: " + req.getEndDate());
    			if (req.isDone()) {
    				req.setState(Requirement.State.DONE);
    			} else if (req.isRunningNow()) {
    				req.setState(Requirement.State.IN_PROGRESS);
    			} else {
    				req.setState(Requirement.State.TO_DO);
    			}
        	}
    	}
    	
    	return new ReleaseDTO(release.getId(), release.getName(), release.getDescription(),
        		release.getInitialDate(), release.getDeadline(),  
        		EmployeeDTO.mapFromEmployeesEntities(release.getResources()), 
        		RequirementDTO.mapFromRequirementsEntities(release.getRequirements()), 
        		release.isHasPlans());
    }

    public static List<ReleaseDTO> mapFromReleasesEntities(List<Release> employees) {
        return employees.stream().map((employee) -> mapFromReleaseEntity(employee)).collect(Collectors.toList());
    }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	
	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public List<EmployeeDTO> getResources() {
		return resources;
	}

	public void setResources (List<EmployeeDTO> resources) {
		this.resources = resources;
	}

	public List<RequirementDTO> getRequirements() {
		return requirements;
	}

	public void setRequirements(List<RequirementDTO> requirements) {
		this.requirements = requirements;
	}
	
	public void extendWeeks(int numWeeks) {
		Date newDate = new Date(deadline.getTime());

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(newDate);
		calendar.add(Calendar.DATE, 7 * numWeeks);
		newDate.setTime(calendar.getTime().getTime());

		this.deadline = newDate;
	}

	public boolean getHasPlans() {
		return hasPlans;
	}

	public void setHasPlans(boolean hasPlans) {
		this.hasPlans = hasPlans;
	}
}
