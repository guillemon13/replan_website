package edu.upc.supersede.app.dto.plan;

import java.util.*;

public class TaskPlanDTO {

	private String name;
	private String color;
	private Date from;
	private Date to;
	private int priority;
	
	private List<DependencyDTO> dependencies;
	
	public TaskPlanDTO (String name, Date from, Date to, int priority) {
		this.name = name;
		this.from = from;
		this.to = to;
		this.priority = priority;
		
		this.dependencies = new ArrayList<DependencyDTO>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public void addDependency(DependencyDTO dep) {
		this.dependencies.add(dep);
	}
	
	public List<DependencyDTO> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<DependencyDTO> dependencies) {
		this.dependencies = dependencies;
	}
}
