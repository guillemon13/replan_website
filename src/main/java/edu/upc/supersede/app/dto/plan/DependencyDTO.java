package edu.upc.supersede.app.dto.plan;

public class DependencyDTO {
	private String to;
	 
	public DependencyDTO(String to) {
		this.to = to;
	}
	
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
}
