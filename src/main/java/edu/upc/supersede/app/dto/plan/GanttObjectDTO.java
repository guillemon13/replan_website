package edu.upc.supersede.app.dto.plan;

public abstract class GanttObjectDTO {
	
	private String name;
	
	public GanttObjectDTO(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
