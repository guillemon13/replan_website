package edu.upc.supersede.app.dto.plan;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import edu.upc.supersede.app.model.Task;

public class TaskDTO {
	
	private String id;
	private String name;
	private Date from;
	private Date to;
	private int priority;
	private String color;
	
	private List<DependencyDTO> dependencies;
	private List<TaskDTO> depTasks;
	
	public TaskDTO () {
		
	}
	
	public TaskDTO (String id, String name, Date from, Date to, int priority) {
		this.id = id;
		this.name = name;
		this.from = from;
		this.to = to;
		this.priority = priority;
		
		this.setColor(priority);
		
		this.dependencies = new ArrayList<DependencyDTO>();
		this.setDepTasks(new ArrayList<TaskDTO>());
	}
	
	public TaskDTO (String id, String name, Date from, Date to, int priority, 
			List<DependencyDTO> dependencies, List<TaskDTO> depTasks) {
		
		this.id = id;
		this.name = name;
		this.from = from;
		this.to = to;
		this.priority = priority;
		
		this.setColor(priority);
		
		this.dependencies = dependencies;
		this.depTasks = depTasks;
	}
		
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public void addDependency(DependencyDTO dep) {
		this.dependencies.add(dep);
	}
	
	public List<DependencyDTO> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<DependencyDTO> dependencies) {
		this.dependencies = dependencies;
	}
	
	public List<TaskDTO> getDepTasks() {
		return depTasks;
	}

	public void setDepTasks(List<TaskDTO> depTasks) {
		this.depTasks = depTasks;
	}
	
	public void addDepTask(TaskDTO dep) {
		this.depTasks.add(dep);
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public void setColor (int priority) {
		switch (priority) {
		case 1: {
			this.color = "red"; break;
		}
		case 2: {
			this.color = "yellow"; break;
		}
		case 3: {
			this.color = "green"; break;
		}
		default: {
			this.color = "gray"; break;
		}
		}
	}
	
//	//TODO
//    public static TaskDTO mapFromTaskEntity(Task task) {
//    	List<DependencyDTO> deps = new ArrayList<DependencyDTO>();
//    	List<TaskDTO> dependenTasks = new ArrayList<TaskDTO>();
//    	
//    	if (task.getDependencies().isEmpty()) {
//    		return new TaskDTO(task.getName(), task.getName(), task.getFromDate(), task.getToDate(), task.getPriority());
//    	} else {
//    		for (Task dependence : task.getDependencies()) {
//        		dependenTasks.add(mapFromTaskEntity(dependence));
//    			deps.add(new DependencyDTO(dependence.getName()));
//        	}
//        	
//        	return new TaskDTO(task.getName(), task.getName(), task.getFromDate(), task.getToDate(), 
//        			task.getPriority(), deps, dependenTasks);	
//    	}
//    	
//    }
//
//    public static List<TaskDTO> mapFromTasksEntities(List<Task> tasks) {
//        return tasks.stream().map((task) -> mapFromTaskEntity(task)).collect(Collectors.toList());
//    }
}
