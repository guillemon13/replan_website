package edu.upc.supersede.app.dto;

import edu.upc.supersede.app.model.Employee;
import java.util.*;
import java.util.stream.Collectors;

public class EmployeeDTO {

	private Long id;
	private String name;
	private float effort;

	private List<SkillDTO> skills;
	
	public EmployeeDTO() {}
	
	public EmployeeDTO (Long id, String name, float effort) {
		this.id = id;
		this.name = name;
		this.effort = effort;
		
		this.skills = new ArrayList<SkillDTO>();
	}

    public EmployeeDTO (Long id, String name, float effort, List<SkillDTO> skills) {
    	this.id = id;
        this.name = name;
        this.effort = effort;

        this.skills = new ArrayList<SkillDTO>(skills);
    }

    public static EmployeeDTO mapFromEmployeeEntity(Employee employee) {
        return new EmployeeDTO(employee.getId(), employee.getName(), employee.getEffort(), SkillDTO.mapFromSkillsEntities(employee.getSkills()));
    }

    public static List<EmployeeDTO> mapFromEmployeesEntities(List<Employee> employees) {
        return employees.stream().map((employee) -> mapFromEmployeeEntity(employee)).collect(Collectors.toList());
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getEffort() {
		return effort;
	}

	public void setEffort(float effort) {
		this.effort = effort;
	}

	public List<SkillDTO> getSkills() {
		return skills;
	}

	public void addSkill(SkillDTO skill) {
		this.skills.add(skill);
	}
	
	public void setSkills(List<SkillDTO> skills) {
		this.skills = skills;
	}
	
}
