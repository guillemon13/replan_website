package edu.upc.supersede.app.dto;

import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.Requirement.State;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class RequirementDTO {

	private Long id;
	private int internalId;
	private String name;

	private double effort;
	private int criteria;
	private int maxEmployees;
	
	private Date deadline;
	
	private List<RequirementDTO> successors;
	private List<RequirementDTO> predecessors;
	
	private List<JobDTO> jobs;

	private List<SkillDTO> skills;

	private State state;
	
	private boolean isPlanned;
	
	public RequirementDTO () {} //JPA only

	public RequirementDTO (Long id, int internalId, String name, double effort, 
			int criteria, int maxEmployees, State state, boolean isPlanned) {
		this.setName(name);
		this.id = id;
		this.internalId = internalId;
		
		this.successors = new ArrayList<RequirementDTO>();
		this.setPredecessors(new ArrayList<RequirementDTO>());
		
		this.jobs = new ArrayList<JobDTO>();
		this.skills = new ArrayList<SkillDTO>();
		
		this.setEffort(effort);
		this.setCriteria(criteria);
		
		this.state = state;
		this.isPlanned = isPlanned;
	}

    public static RequirementDTO mapFromRequirementEntity(Requirement requirement) {
    	RequirementDTO req = new RequirementDTO(requirement.getId(), requirement.getInternalId(), requirement.getName(), 
        		requirement.getEffort(), requirement.getCriteria(), 
        		requirement.getMaxEmployees(), requirement.getState(),
        		requirement.isPlanned()); 
    	
    	RequirementDTO succ;
    	RequirementDTO pred;
    	
    	for (int i = 0; i < requirement.getSuccessors().size(); i++) {
    		succ = mapFromRequirementEntity(requirement.getSuccessors().get(i));
    		req.addSuccessor(succ);
    	}
    	
    	for (int i = 0; i < requirement.getPredecessors().size(); i++) {
    		pred = mapPredecessors(requirement.getPredecessors().get(i));
    		req.addPredecessor(pred);
    	}
    	
    	req.setSkills(SkillDTO.mapFromSkillsEntities(requirement.getSkills())); 
    	
        return req;
    }

	private static RequirementDTO mapPredecessors (Requirement requirement) {
    	RequirementDTO req = new RequirementDTO(requirement.getId(), requirement.getInternalId(), requirement.getName(), 
        		requirement.getEffort(), requirement.getCriteria(), 
        		requirement.getMaxEmployees(), requirement.getState(),
        		requirement.isPlanned()); 
    	
    	for (int i = 0; i < requirement.getPredecessors().size(); i++) {
    		req.addPredecessor(mapPredecessors(requirement.getPredecessors().get(i)));
    	}
    	
    	req.setSkills(SkillDTO.mapFromSkillsEntities(requirement.getSkills())); 
    	
        return req;
    }
    
    
    public static List<RequirementDTO> mapFromRequirementsEntities(List<Requirement> requirements) {
        return requirements.stream().map((requirement) -> mapFromRequirementEntity(requirement)).collect(Collectors.toList());
    }
	
	public int getInternalId() {
		return internalId;
	}

	public void setInternalId(int internalId) {
		this.internalId = internalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<RequirementDTO> getSuccessors() {
		return successors;
	}
	
	public void setSuccessors(List<RequirementDTO> successors) {
		this.successors = successors;
	}
	
	public List<RequirementDTO> getPredecessors() {
		return predecessors;
	}

	public void setPredecessors(List<RequirementDTO> predecessors) {
		this.predecessors = predecessors;
	}

	public List<JobDTO> getJobs() {
		return jobs;
	}

	public void addJob(JobDTO job) {
		this.jobs.add(job);
	}

	public void setJobs(List<JobDTO> jobs) {
		this.jobs = jobs;
	}

	public void addSkill(SkillDTO skill) {
		this.skills.add(skill);
	}

	public List<SkillDTO> getSkills() {
		return this.skills;
	}

	public double getEffort() {
		return effort;
	}

	public void setEffort(double effort) {
		this.effort = effort;
	}

	public int getCriteria() {
		return criteria;
	}

	public void setCriteria(int criteria) {
		this.criteria = criteria;
	}

	public boolean compareSkills(List<SkillDTO> emplSkills) {
		boolean found = false;

		if (this.skills.size() == emplSkills.size()) return false;

		for (SkillDTO s : this.skills) {
			for (SkillDTO emplSkill : emplSkills) {
				found = emplSkill.equals(s);
				if (found) break;
			}

			if (!found) return false;
			found = false;
		}

		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public int getMaxEmployees() {
		return maxEmployees;
	}

	public void setMaxEmployees(int maxEmployees) {
		this.maxEmployees = maxEmployees;
	}
	
	public void addSuccessor (RequirementDTO successor) {
		this.successors.add(successor);
	}
	
	public void addPredecessor (RequirementDTO predecessor) {
		this.predecessors.add(predecessor);
	}
	
	public void setSkills (List<SkillDTO> skills) {
		this.skills = skills;
	}

	public boolean isPlanned() {
		return isPlanned;
	}

	public void setPlanned(boolean isPlanned) {
		this.isPlanned = isPlanned;
	}
}
