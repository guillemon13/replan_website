package edu.upc.supersede.app.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import edu.upc.supersede.app.model.Project;

public class ProjectDTO {

	private Long id;
	private String name;
	private String description;
	private Date dateCreation;

	private int defaultEmployees;
	
	private List<ReleaseDTO> releases;

	public ProjectDTO() {}

	public ProjectDTO(Long id, String name, String description,  Date dateCreation, int defaultEmployees)  {
		this.id = id;
		this.name = name;
		this.description = description;
		this.dateCreation = dateCreation;
		
		this.releases = new ArrayList<ReleaseDTO>();
	}
	
	public ProjectDTO(Long id, String name, String description, Date dateCreation, int defaultEmployees, List<ReleaseDTO> releases) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.dateCreation = dateCreation;

		this.releases = releases;
	}

    public static ProjectDTO mapFromProjectEntity(Project project) {
        return new ProjectDTO(project.getId(), project.getName(), project.getDescription(),  
        		project.getDateCreation(), project.getDefaultEmployees(), ReleaseDTO.mapFromReleasesEntities(project.getReleases()));
    }

    public static List<ProjectDTO> mapFromProjectsEntities(List<Project> projects) {
        return projects.stream().map((project) -> mapFromProjectEntity(project)).collect(Collectors.toList());
    }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDate(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public List<ReleaseDTO> getReleases() {
		return releases;
	}

	public void setReleases (List<ReleaseDTO> releases) {
		this.releases = releases;
	}
	
	public int getDefaultEmployees() {
		return this.defaultEmployees;
	}

	public void setDefaultEmployees(int defaultEmployees) {
		this.defaultEmployees = defaultEmployees;
	}
}
