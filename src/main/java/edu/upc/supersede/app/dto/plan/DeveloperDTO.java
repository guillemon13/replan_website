package edu.upc.supersede.app.dto.plan;

import java.util.*;
import java.util.stream.Collectors;

import edu.upc.supersede.app.model.Developer;

public class DeveloperDTO extends GanttObjectDTO{
	
	private String name;
	private List<String> children;
	private List<TaskDTO> tsks;
	private String content;
	
	public DeveloperDTO () {
		super("");
	}
	
	public DeveloperDTO (String name) {
		super(name);
		this.name = name;
		this.tsks = new ArrayList<TaskDTO>();
		this.setChildren(new ArrayList<String>());
		this.setContent("<i class=\"fa fa-file-code-o\" ng-click=\"scope.handleRowIconClick(row.model)\"></i> {{row.model.name}}");
	}

	public DeveloperDTO (String name, List<String> children) {
		super(name);
		this.name = name;
		this.tsks = new ArrayList<TaskDTO>();
		this.setChildren(children);
		this.setContent("<i class=\"fa fa-file-code-o\" ng-click=\"scope.handleRowIconClick(row.model)\"></i> {{row.model.name}}");
	}
	
	//TODO
    public static DeveloperDTO mapFromDeveloperEntity(Developer developer) {
        return null;
    	//return new DeveloperDTO(developer.getName(), TaskDTO.mapFromTasksEntities(developer.getTasks()));
    }

    public static List<DeveloperDTO> mapFromDevelopersEntities(List<Developer> developers) {
        return developers.stream().map((developer) -> mapFromDeveloperEntity(developer)).collect(Collectors.toList());
    }
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getChildren() {
		return children;
	}

	public void setChildren(List<String> children) {
		this.children = children;
	}
	
	public void addChild (String child) {
		this.children.add(child);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void addTask(TaskDTO task) {
		this.tsks.add(task);
	}
	
	public List<TaskDTO> getTsks() {
		return this.tsks;
	}

}
