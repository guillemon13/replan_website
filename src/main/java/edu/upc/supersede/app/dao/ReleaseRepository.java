package edu.upc.supersede.app.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import edu.upc.supersede.app.model.Release;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.Skill;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * 
 * Repository class for the Releases repository.
 * 
 * 
 */

@Repository
public class ReleaseRepository {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Delete a release, given its identifier
	 *
	 * @param deletedJobId
	 *            - the id of the release to be deleted
	 */
	public void delete(Long deletedReleaseId) {
		Release delete = em.find(Release.class, deletedReleaseId);
		em.remove(delete);
	}

	/**
	 *
	 * finds a release given its id
	 *
	 */
	public Release findReleaseById(Long id) {
		//return em.find(Release.class, id);
		CriteriaBuilder cb = em.getCriteriaBuilder();

		// the actual search query that returns one page of results
		CriteriaQuery<Release> searchQuery = cb.createQuery(Release.class);
		Root<Release> searchRoot = searchQuery.from(Release.class);
		searchQuery.select(searchRoot);

		List<Predicate> predicates = new ArrayList<>();

		predicates.add(cb.equal(searchRoot.get("projectId"), id));
		searchQuery.where(predicates.toArray(new Predicate[] {}));

		List<Release> releases = em.createQuery(searchQuery).getResultList();
		
		if (releases.isEmpty()) 
			return new Release();
		else return releases.get(0);
	}

	public List<Release> findReleasesById(List<Long> ids) {
        Query query = em.createQuery("SELECT r FROM Release r WHERE r.id in :ids", Release.class);
        query.setParameter("ids", ids);
        
        List<Release> releases = new ArrayList<Release>();
        releases.addAll(query.getResultList());
		
        return releases;
	}
	
	public List<Release> getAll() {
	    Query query = em.createQuery("SELECT e FROM Release e");
	    return query.getResultList();
	}
	
	/**
	 *
	 * finds a release given its projectId
	 *
	 */
	public List<Release> findReleasesByProjectId(Long projectId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		// the actual search query that returns one page of results
		CriteriaQuery<Release> searchQuery = cb.createQuery(Release.class);
		Root<Release> searchRoot = searchQuery.from(Release.class);
		searchQuery.select(searchRoot);

		List<Predicate> predicates = new ArrayList<>();

		predicates.add(cb.equal(searchRoot.get("projectId"), projectId));
		searchQuery.where(predicates.toArray(new Predicate[] {}));

		return em.createQuery(searchQuery).getResultList();
	}

	/**
	 *
	 * save changes made to a release, or create the release if its a new
	 * release.
	 *
	 */
	public Release save(Release release) {
		return em.merge(release);
	}

}
