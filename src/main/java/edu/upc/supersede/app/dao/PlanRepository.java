package edu.upc.supersede.app.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import edu.upc.supersede.app.model.Plan;
import edu.upc.supersede.app.model.Release;
import edu.upc.supersede.app.model.Requirement;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * 
 * Repository class for the Plans repository.
 * 
 * 
 */

@Repository
public class PlanRepository {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Delete a plan, given its identifier
	 *
	 * @param deletedPlanId - the id of the plan to be deleted
	 */
	public void delete(Long deletedPlanId) {
		Plan delete = em.find(Plan.class, deletedPlanId);
		em.remove(delete);
	}

	public void delete(Plan delete) {
		em.remove(delete);
	}
	
	public void deleteByRelease(Long releaseId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		// the actual search query that returns one page of results
		CriteriaQuery<Plan> searchQuery = cb.createQuery(Plan.class);
		Root<Plan> searchRoot = searchQuery.from(Plan.class);
		searchQuery.select(searchRoot);

		List<Predicate> predicates = new ArrayList<>();

		predicates.add(cb.equal(searchRoot.get("releaseId"), releaseId));
		searchQuery.where(predicates.toArray(new Predicate[] {}));

		List<Plan> plans = em.createQuery(searchQuery).getResultList(); 
		
		if (!plans.isEmpty()) {
			for (Plan plan : plans) {
				em.remove(plan);
			}
		}
	}
	
	/**
	 *
	 * finds a plan given its id
	 *
	 */
	public Plan findPlanById(Long id) {
		return em.find(Plan.class, id);
	}

	/**
	 *
	 * finds a plan given its projectId
	 *
	 */
	public List<Plan> findPlansByProjectId(Long projectId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		// the actual search query that returns one page of results
		CriteriaQuery<Plan> searchQuery = cb.createQuery(Plan.class);
		Root<Plan> searchRoot = searchQuery.from(Plan.class);
		searchQuery.select(searchRoot);

		List<Predicate> predicates = new ArrayList<>();

		predicates.add(cb.equal(searchRoot.get("projectId"), projectId));
		searchQuery.where(predicates.toArray(new Predicate[] {}));

		return em.createQuery(searchQuery).getResultList();
	}

	public List<Plan> findPlansByReleaseId(Long releaseId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		// the actual search query that returns one page of results
		CriteriaQuery<Plan> searchQuery = cb.createQuery(Plan.class);
		Root<Plan> searchRoot = searchQuery.from(Plan.class);
		searchQuery.select(searchRoot);

		List<Predicate> predicates = new ArrayList<>();

		predicates.add(cb.equal(searchRoot.get("releaseId"), releaseId));
		searchQuery.where(predicates.toArray(new Predicate[] {}));

		return em.createQuery(searchQuery).getResultList();
	}
	
	/**
	 *
	 * save changes made to a plan, or create the plan if its a new
	 * plan.
	 *
	 */
	public Plan save(Plan plan) {
		return em.merge(plan);
	}

	public List<Plan> getAll() {
	    Query query = em.createQuery("SELECT e FROM Plan e");
	    return query.getResultList();
	}



}
