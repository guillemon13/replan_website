package edu.upc.supersede.app.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import edu.upc.supersede.app.model.Employee;


/**
 * Repository for the Employee class.
 */

@Repository
public class EmployeeRepository {
    
	@PersistenceContext
    private EntityManager em;
	
	/**
     * Delete a employee, given its identifier
     *
     * @param deletedEmployeeId - the id of the employee to be deleted
     */
    public void delete(Long deletedEmployeeId) {
    	Employee delete = em.find(Employee.class, deletedEmployeeId);
        em.remove(delete);
    }

    /**
     *
     * finds a employee given its id
     *
     */
    public Employee findEmployeeById(Long id) {
        return em.find(Employee.class, id);
    }
    
    /**
    *
    * finds the project's employees.
    *
    */
	public List<Employee> findEmployeesByProjectId(Long projectId) {
		
        CriteriaBuilder cb = em.getCriteriaBuilder();

        // the actual search query that returns one page of results
        CriteriaQuery<Employee> searchQuery = cb.createQuery(Employee.class);
        Root<Employee> searchRoot = searchQuery.from(Employee.class);
        searchQuery.select(searchRoot);
        
        List<Predicate> predicates = new ArrayList<>();
        
        predicates.add(cb.equal(searchRoot.get("projectId"), projectId));
        searchQuery.where(predicates.toArray(new Predicate[]{}));

		return em.createQuery(searchQuery).getResultList();
	}

    /**
    *
    * finds the employees for Ids.
    *
    */
	public List<Employee> findEmployeesByIds(List<Long> ids) {
		
        Query query = em.createQuery("SELECT e FROM " + Employee.class + " e WHERE e.id in :ids", Employee.class);
        query.setParameter("ids", ids);
        
        List<Employee> employees = new ArrayList<Employee>();
        employees.addAll(query.getResultList());
        
		return employees;
	}
	

    /**
     *
     * save changes made to a employee, or create the employee if its a new employee.
     *
     */
    public Employee save(Employee employee) {
        return em.merge(employee);
    }


	/*Optional<Employee> findById(String employeeId);

    List<Employee> findAllSkills(List<Skill> skills);*/
}
