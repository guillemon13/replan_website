package edu.upc.supersede.app.dao;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.Requirement;


/**
 * Repository for the Requirement class
 */

@Repository
public class RequirementRepository {
    
    @PersistenceContext
    private EntityManager em;
    
	/**
     * Delete a requirement, given its identifier
     *
     * @param deletedJobId - the id of the requirement to be deleted
     */
    public void delete(Long deletedRequirementId) {
    	Requirement delete = em.find(Requirement.class, deletedRequirementId);
        em.remove(delete);
    }

    /**
     *
     * finds a requirement given its id
     *
     */
    public Requirement findRequirementById(Long id) {
        return em.find(Requirement.class, id);
    }

	public List<Requirement> findRequirementsByIds(List<Long> ids) {
        List<Requirement> requirements = new ArrayList<Requirement>();
        
        if (!ids.isEmpty()) {
	        Query query = em.createQuery("SELECT f FROM Requirement f WHERE f.id in :ids", Requirement.class);
	        query.setParameter("ids", ids);
	        requirements.addAll(query.getResultList());
        }
        
		return requirements;
	}
    
	public List<Requirement> findRequirementByProjectId(Long projectId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        // the actual search query that returns one page of results
        CriteriaQuery<Requirement> searchQuery = cb.createQuery(Requirement.class);
        Root<Requirement> searchRoot = searchQuery.from(Requirement.class);
        searchQuery.select(searchRoot);
        
        List<Predicate> predicates = new ArrayList<>();
        
        predicates.add(cb.equal(searchRoot.get("projectId"), projectId));
        searchQuery.where(predicates.toArray(new Predicate[]{}));

		return em.createQuery(searchQuery).getResultList();
	}
	
    /**
     *
     * save changes made to a requirement, or create the requirement if its a new requirement.
     *
     */
    public Requirement save(Requirement requirement) {
        return em.merge(requirement);
    }




}
