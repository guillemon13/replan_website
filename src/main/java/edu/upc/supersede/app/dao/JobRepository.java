package edu.upc.supersede.app.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import edu.upc.supersede.app.model.Job;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Repository for the Job class
 */

@Repository
public class JobRepository  {
    
	@PersistenceContext
    private EntityManager em;
    
	/**
     * Delete a job, given its identifier
     *
     * @param deletedJobId - the id of the job to be deleted
     */
    public void delete(Long deletedJobId) {
    	Job delete = em.find(Job.class, deletedJobId);
        em.remove(delete);
    }

    /**
     *
     * finds a job given its id
     *
     */
    public Job findJobById(Long id) {
        return em.find(Job.class, id);
    }
    
    /**
    *
    * finds the project's jobs.
    *
    */
	public List<Job> findJobsByProjectId(Long projectId) {
		
        CriteriaBuilder cb = em.getCriteriaBuilder();

        // the actual search query that returns one page of results
        CriteriaQuery<Job> searchQuery = cb.createQuery(Job.class);
        Root<Job> searchRoot = searchQuery.from(Job.class);
        searchQuery.select(searchRoot);
        
        List<Predicate> predicates = new ArrayList<>();
        
        predicates.add(cb.equal(searchRoot.get("projectId"), projectId));
        searchQuery.where(predicates.toArray(new Predicate[]{}));

		return em.createQuery(searchQuery).getResultList();
	}
    

    /**
     *
     * save changes made to a job, or create the job if its a new job.
     *
     */
    public Job save(Job job) {
        return em.merge(job);
    }

	
    /*Optional<Job> findById(String jobId);
    List<Job> findAllJobs(String requirementId);*/
}
