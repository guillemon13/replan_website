package edu.upc.supersede.app.dao;


import org.springframework.stereotype.Repository;

import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Repository class for the User entity
 *
 */
@Repository
public class UserRepository {

    @PersistenceContext
    private EntityManager em;

    /**
     * finds a user given its username
     *
     * @param username - the username of the searched user
     * @return  a matching user, or null if no user found.
     */
    public User findUserByUsername(String username) {

        List<User> users = em.createNamedQuery(User.FIND_BY_USERNAME, User.class)
                .setParameter("username", username)
                .getResultList();

        return users.size() == 1 ? users.get(0) : null;
    }


    /**
    *
    * finds the project's users.
    *
    */
	public List<User> findUsersByProjectId(Long projectId) {
		
        CriteriaBuilder cb = em.getCriteriaBuilder();

        // the actual search query that returns one page of results
        CriteriaQuery<User> searchQuery = cb.createQuery(User.class);
        Root<User> searchRoot = searchQuery.from(User.class);
        searchQuery.select(searchRoot);
        
        List<Predicate> predicates = new ArrayList<>();
        
        predicates.add(cb.equal(searchRoot.get("projectId"), projectId));
        searchQuery.where(predicates.toArray(new Predicate[]{}));

		return em.createQuery(searchQuery).getResultList();
	}

    /**
     *
     * save changes made to a user, or insert it if its new
     *
     * @param user
     */
    public void save(User user) {
        em.merge(user);
    }

    /**
     * checks if a username is still available in the database
     *
     * @param username - the username to be checked for availability
     * @return true if the username is still available
     */
    public boolean isUsernameAvailable(String username) {

        List<User> users = em.createNamedQuery(User.FIND_BY_USERNAME, User.class)
                .setParameter("username", username)
                .getResultList();

        return users.isEmpty();
    }
}
