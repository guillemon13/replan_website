package edu.upc.supersede.app.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import edu.upc.supersede.app.model.Project;
import edu.upc.supersede.app.model.Requirement;
import edu.upc.supersede.app.model.Skill;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * 
 * Repository class for the Projects repository.
 * 
 * 
 */

@Repository
public class ProjectRepository {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Delete a project, given its identifier
	 *
	 * @param deletedJobId
	 *            - the id of the project to be deleted
	 */
	public void delete(Long deletedProjectId) {
		Project delete = em.find(Project.class, deletedProjectId);
		em.remove(delete);
	}

	/**
	 *
	 * finds a project given its id
	 *
	 */
	public List<Project> findProjectById(Long projectId) {
       CriteriaBuilder cb = em.getCriteriaBuilder();

       // the actual search query that returns one page of results
       CriteriaQuery<Project> searchQuery = cb.createQuery(Project.class);
       Root<Project> searchRoot = searchQuery.from(Project.class);
       searchQuery.select(searchRoot);
       
       List<Predicate> predicates = new ArrayList<>();
       
       predicates.add(cb.equal(searchRoot.get("projectId"), projectId));
       searchQuery.where(predicates.toArray(new Predicate[]{}));

       List<Project> projects = em.createQuery(searchQuery).getResultList(); 
       
       return projects;
	}
	
	/**
	 *
	 * finds a project given its id
	 *
	 */
	public List<Project> findProjectByUserId(Long userId) {
      CriteriaBuilder cb = em.getCriteriaBuilder();
      
      // the actual search query that returns one page of results
      CriteriaQuery<Project> searchQuery = cb.createQuery(Project.class);
      Root<Project> searchRoot = searchQuery.from(Project.class);
      searchQuery.select(searchRoot);
      
      List<Predicate> predicates = new ArrayList<>();
      
      predicates.add(cb.equal(searchRoot.get("projectId"), userId));
      searchQuery.where(predicates.toArray(new Predicate[]{}));

      List<Project> projects = em.createQuery(searchQuery).getResultList(); 
      
      return projects;
	}
	
	

	/**
	 *
	 * save changes made to a project, or create the project if its a new
	 * project.
	 *
	 */
	public Project save(Project project) {
		return em.merge(project);
	}

}
