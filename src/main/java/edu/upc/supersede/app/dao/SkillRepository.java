package edu.upc.supersede.app.dao;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import edu.upc.supersede.app.model.Employee;
import edu.upc.supersede.app.model.Release;
import edu.upc.supersede.app.model.Skill;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;



@Repository
public class SkillRepository  {
    
	@PersistenceContext
    private EntityManager em;
	
	/**
     * Delete a skill, given its identifier
     *
     * @param deletedJobId - the id of the skill to be deleted
     */
    public void delete(Long deletedSkillId) {
    	Skill delete = em.find(Skill.class, deletedSkillId);
        em.remove(delete);
    }

    /**
     *
     * finds a skill given its id
     *
     */
    public Skill findSkillById(Long id) {
        return em.find(Skill.class, id);
    }

    /**
    *
    * finds skill given ids
    *
    */
	public List<Skill> findSkillsById(List<Long> ids) {
        Query query = em.createQuery("SELECT s FROM Skill s WHERE s.id in :ids", Skill.class);
        query.setParameter("ids", ids);
        
        List<Skill> skills = new ArrayList<Skill>();
        skills.addAll(query.getResultList());
		
        return skills;
	}
    
    /**
    *
    * finds skills given its projectId
    *
    */
   public List<Skill> findSkillsByProjectId(Long projectId) {
       CriteriaBuilder cb = em.getCriteriaBuilder();

       // the actual search query that returns one page of results
       CriteriaQuery<Skill> searchQuery = cb.createQuery(Skill.class);
       Root<Skill> searchRoot = searchQuery.from(Skill.class);
       searchQuery.select(searchRoot);
       
       List<Predicate> predicates = new ArrayList<>();
       
       predicates.add(cb.equal(searchRoot.get("projectId"), projectId));
       searchQuery.where(predicates.toArray(new Predicate[]{}));

	   return em.createQuery(searchQuery).getResultList();
   }
	
    /**
     *
     * save changes made to a skill, or create the skill if its a new skill.
     *
     */
    public Skill save(Skill skill) {
        return em.merge(skill);
    }

    
    /**
     * 
     * retrieves all existing skills.
     * 
     */
	public List<Skill> getAllSkills() {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        // the actual search query that returns one page of results
        CriteriaQuery<Skill> searchQuery = cb.createQuery(Skill.class);
        Root<Skill> searchRoot = searchQuery.from(Skill.class);
        searchQuery.select(searchRoot);
        
		return em.createQuery(searchQuery).getResultList();
	}
}
