package edu.upc.supersede.app.model;

public class Dependency extends AbstractEntity{
	
	private String to;
	
	Dependency() {}
	
	public Dependency(String to) {
		this.to = to;
	}
	
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
}
