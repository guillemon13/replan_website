package edu.upc.supersede.app.model;

import java.util.*;

import javax.persistence.*;

import edu.upc.supersede.app.dto.PlanDTO;
import edu.upc.supersede.app.dto.plan.ChildDTO;
import edu.upc.supersede.app.dto.plan.DependencyDTO;
import edu.upc.supersede.app.dto.plan.DeveloperDTO;
import edu.upc.supersede.app.dto.plan.TaskDTO;

@Entity
@Table (name = "PLANS")
public class Plan extends AbstractEntity {

	private String name;
	private Date releaseDate;
	
	private Long releaseId;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Developer> developers;
	
	Plan () {}
	
	public Plan (Long projectId, String name, Date releaseDate) {
		this.setProjectId(projectId);
		this.name = name;
		this.releaseDate = releaseDate;
		
		this.developers = new ArrayList<Developer>();	
	}
	
	public Plan (Long projectId, String name, Date releaseDate, List<Developer> developers) {
		this.setProjectId(projectId);
		this.name = name;
		this.releaseDate = releaseDate;
		
		this.developers = developers;
	}

	public static PlanDTO mapPlanDTO (Plan plan) {
		PlanDTO planDTO = new PlanDTO(plan.getProjectId(), plan.getName(), plan.getReleaseDate());
		
		Map<String, List<Task>> assignments = new TreeMap<String, List<Task>>();
		
		for (Developer developer : plan.getDevelopers()) {
			if (!assignments.containsKey(developer.getName())) {
				assignments.put(developer.getName(), new ArrayList<Task>());
			}

			for (Task task : developer.getTasks()) {
				assignments.get(developer.getName()).add(task);	
			}
		}
	
		TaskDTO task; 
		
		for (String employee : assignments.keySet()) {
			DeveloperDTO developer = new DeveloperDTO(employee);
							
			for (Task employeeTask : assignments.get(employee)) {
				task = new TaskDTO(employeeTask.getIdTask(), employeeTask.getName(), employeeTask.getFromDate(), employeeTask.getToDate(), employeeTask.getPriority());
				
				for (String successor : employeeTask.getDependencies()) {
					DependencyDTO dep = new DependencyDTO(successor);
					task.addDependency(dep);
				}
				
				developer.addChild(employeeTask.getIdTask());
				developer.addTask(task);
				
				ChildDTO child = new ChildDTO(employeeTask.getIdTask());
				child.addTask(task);
				
				planDTO.addObject(child);
			}
			
			planDTO.addObject(developer);
		}
		
		return planDTO;
		
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public List<Developer> getDevelopers() {
		return developers;
	}

	public void setDevelopers(List<Developer> developers) {
		this.developers = developers;
	}

	public void addDeveloper(Developer developer) {
		this.developers.add(developer);
	}

	public Long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(Long releaseId) {
		this.releaseId = releaseId;
	}
	
}
