package edu.upc.supersede.app.model;

import javax.persistence.*;

import java.util.*;

@Entity
@Table(name = "EMPLOYEES")
public class Employee extends AbstractEntity{

	private int internalId;
	private String name;
	private float effort;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Skill> skills;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Job> jobs;
	
	Employee () {} // JPA only.
	
	public Employee (int id) {
		this.setInternalId(id);
		this.skills = new ArrayList<Skill>();
		
		this.jobs = new ArrayList<Job>();
	}
	
	public Employee (String name, float effort, Long projectId) {
		this.name = name;
		this.effort = effort;
		
		this.skills = new ArrayList<Skill>();
		this.jobs = new ArrayList<Job>();
		
		this.setProjectId(projectId);
	}

    public Employee (String name, float effort, List<Skill> skills, Long projectId) {
        this.name = name;
        this.effort = effort;

        this.skills = new ArrayList<Skill>(skills);
        this.jobs = new ArrayList<Job>();
        
        this.setProjectId(projectId);
    }

	public int getInternalId() {
		return internalId;
	}

	public void setInternalId(int internalId) {
		this.internalId = internalId;
	}
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getEffort() {
		return effort;
	}

	public void setEffort(float effort) {
		this.effort = effort;
	}

	public List<Skill> getSkills() {
		return skills;
	}

	public void addSkill(Skill skill) {
		this.skills.add(skill);
	}
	
	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	
	public List<Job> getJobs() {
		return this.jobs;
	}
	
	
	public void addJob(Job job) {
		this.jobs.add(job);
	}
	
	public boolean equals (Object employee) {
		Employee e = (Employee)employee;
		
		return this.getId() == e.getId();
	}
	
	public String toString() {
		String s = "Employee " + this.getId() + ". " + name + "\n";
		s += "Availability: " + effort + "\n";
		s += "Skills: \n";
		
		for (Skill sk : skills) {
			s += "\t- " + sk.toString() + "\n";
		}
		
		return s;
	}

	public Job getJob(Requirement requirement) {
		for (Job job : jobs) {
			if (job.getRequirement().equals(requirement)) return job;
		}
		
		return null;
	}

	public void resetJobs() {
		this.jobs = new ArrayList<Job>();
	}

	public int getPriorityCharge() {
		int sumPriority = 0;
		
		for (Job job : jobs) {
			sumPriority += job.getRequirement().getCriteria();
		}
		
		return sumPriority;
	}

}
