package edu.upc.supersede.app.model;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Table (name = "REQUIREMENTS")
public class Requirement extends AbstractEntity {

	@Column (name = "internalId")
	private int internalId;
	private String name;

	private double effort;
	private int criteria;
	private int maxEmployees;
	
	private Date deadline;

	private Date startDate;
	private Date endDate;
	
	private State state;
	
	private boolean isPlanned;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name="ReqPredecessors", 
	                joinColumns={@JoinColumn(name="ParentId")}, 
	                inverseJoinColumns={@JoinColumn(name="ReqId")})
	private List<Requirement> predecessors;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name="ReqSuccessors", 
	                joinColumns={@JoinColumn(name="ReqId")}, 
	                inverseJoinColumns={@JoinColumn(name="ParentId")})
	private List<Requirement> successors;
		
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private List<Job> jobs;

	@ManyToMany(fetch = FetchType.EAGER, targetEntity=Skill.class)
	@Column
	private List<Skill> skills;

	Requirement () {} //JPA only

	public Requirement (int id) {
		this.internalId = id;

		this.successors = new ArrayList<Requirement>();
		this.predecessors = new ArrayList<Requirement>();

		this.jobs = new ArrayList<Job>();
		this.skills = new ArrayList<Skill>();
		this.state = State.TO_DO;
	}
	
	public Requirement (String name, double effort, int criteria, Date deadline, int maxEmployees, Long projectId) {
		this.setName(name);

		this.successors = new ArrayList<Requirement>();
		this.predecessors = new ArrayList<Requirement>();
		
		this.jobs = new ArrayList<Job>();
		this.skills = new ArrayList<Skill>();
		
		this.setEffort(effort);
		this.setCriteria(criteria);
		this.setDeadline(deadline);
		this.setProjectId(projectId);
		this.setMaxEmployees(maxEmployees);
		
		this.state = State.TO_DO;
	}
	
	public Requirement (Requirement req) {
		this.setId(req.getId());
		this.internalId = req.getInternalId();
		this.setName(req.getName());
		
		this.effort = req.getEffort();
		this.criteria = req.getCriteria();

		this.deadline = req.getDeadline();
		
		this.successors = new ArrayList<Requirement>();
		this.predecessors = new ArrayList<Requirement>();
		
		this.jobs = new ArrayList<Job>();
		
		this.skills = new ArrayList<Skill>(req.getSkills());
		
		this.startDate = req.getStartDate();
		this.endDate = req.getEndDate();
		
		this.state = req.getState();
		this.setProjectId(req.getProjectId());
		
		this.maxEmployees = req.getMaxEmployees();
	}
	
	
	public int getInternalId() {
		return internalId;
	}

	public void setInternalId(int internalId) {
		this.internalId = internalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Requirement> getSuccessors() {
		return successors;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public Job getJob(Employee employee) {
		for (Job job : jobs) {
			if (job.getEmployee().getName().equals(employee.getName())) return job;
		}
		
		return null;
	}
	
	public void addJob(Job job) {
		this.jobs.add(job);
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public void addSkill(Skill skill) {
		this.skills.add(skill);
	}

	public List<Skill> getSkills() {
		return this.skills;
	}

	public double getEffort() {
		return effort;
	}

	public void setEffort(double effort) {
		this.effort = effort;	
	}

	public int getCriteria() {
		return criteria;
	}

	public void setCriteria(int criteria) {
		this.criteria = criteria;
	}

	public boolean fitsEmployee(Employee employee) {
		boolean found = false;
		
		for (Skill s : this.skills) {
			for (Skill emplSkill : employee.getSkills()) {
				if (emplSkill.equals(s)) {
					found = true;
					break;
				}
			}

			if (!found) return false;
			found = false;
		}

		return true;
	}

	public List<Skill> fitsEmployeeSkills(Employee employee) {

		List<Skill> foundSkills = new ArrayList<Skill>();

		for (Skill s : this.skills) {
			for (Skill emplSkill : employee.getSkills()) {
				if (emplSkill.equals(s)) {
					foundSkills.add(emplSkill);
				}
			}
		}

		return foundSkills;
	}

	public void addPredecessor(Requirement req) {
		this.predecessors.add(req);
		req.addSuccessor(this);
	}
	
	public void addPredecessorRelation(Requirement requirement) {
		this.predecessors.add(requirement);
	}

	public void addSuccessor(Requirement requirement) {
		this.successors.add(requirement);
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public List<Requirement> getPredecessors() {
		return this.predecessors;
	}

	public int getStartTime() {
		return this.jobs.get(0).getEarlyStart();
	}

	public double getEndTime() {
		return this.jobs.get(0).getEarlyStart() + this.effort;
	}

	public Date getStartDate() {
		if (jobs.size() > 0) {
			return this.jobs.get(0).getFromTime();
		} else {
			return this.startDate;
		}
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
		
		if (this.startDate != null) {
			updateDate(8);
		}
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void updateDate(int workHours) {
		//Add days to date.
		int hours = (int)Math.rint(effort) % workHours;
		int days = (int)Math.rint(effort) / workHours;
		
		Calendar c = Calendar.getInstance();
		c.setTime(this.startDate);
		c.add(Calendar.DAY_OF_MONTH, days);
		c.add(Calendar.HOUR_OF_DAY, hours);
		
		this.setEndDate(c.getTime());
		
		for (Job job : jobs) {
			job.setFromTime(startDate);
		}
	}
	
	public void setEndDate(Date date) {
		this.endDate = date;
	}
	
	public boolean isRunning (int t) {
		return t >= this.getStartTime() && t < this.getEndTime();
	}

	public boolean isRunningNow() {
		if (this.getStartDate() == null || this.getEndDate() == null) return false;
		
		return this.getStartDate().before(new Date(System.currentTimeMillis())) &&
				new Date(System.currentTimeMillis()).before(this.getEndDate());
	}
	
	public boolean isDone() {
		if (this.getEndDate() == null) return false;
		
		return this.getEndDate().before(new Date(System.currentTimeMillis()));
	}
	
	public List<Job> getEmployeeJob(Employee employee) {
		List<Job> results = new ArrayList<Job>();

		for (Job job : jobs) {
			if (job.getEmployee().getId() == employee.getId())
				results.add(job);
		}

		return results;
	}
	
	public void postpose(double effort) {
		for (Job job : this.jobs) {
			job.postposeStart((int)Math.rint(effort));
		}
	}

	public void removeSuccessor(Requirement req) {
		successors.remove(req);
	}

	public void removePredecessor(Requirement requirement) {
		predecessors.remove(requirement);
	}
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
		for (Job job : jobs) {
			job.setState(state);
		}
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}

	public void setSuccessors(List<Requirement> successors) {
		this.successors = successors;
	}

	public int getMaxEmployees() {
		return maxEmployees;
	}

	public void setMaxEmployees(int maxEmployees) {
		this.maxEmployees = maxEmployees;
	}
	
	public boolean equals (Object o2) {
		Requirement r2 = (Requirement)o2;
		return this.getId() == r2.getId();
	}
	

	public String toString() {
		String s = name + "\n";
		s += "Effort: " + effort + ". Criteria: " + criteria + ".\n";
		//s += "Start: " + this.getStartTime() + "\n";
		s += "Successors: \n";


		for (Requirement req : successors) {
			s += "\t- " + req.getName() + "\n";
		}

		s += "Skills: \n";
		for (Skill sk : skills) {
			s += "\t- " + sk.toString() + "\n";
		}

		return s;
	}
	
	public enum State {
	    TO_DO, IN_PROGRESS, DONE 
	}

	public void updateStartDate(Date releaseDate) {
		
		Calendar c = Calendar.getInstance();
		c.setTime(releaseDate);
		
		int initialTime = this.getStartTime();
		
		int hours = (int)Math.rint(initialTime) % 8;
		int days = (int)Math.rint(initialTime) / 8;
		
		c.add(Calendar.DAY_OF_MONTH, days);
		c.add(Calendar.HOUR_OF_DAY, hours);
		
		this.setStartDate(c.getTime());
		
		int endTime = (int)this.getEndTime();
		
		c.setTime(releaseDate);
		
		hours = (int)Math.rint(endTime) % 8;
		days = (int)Math.rint(endTime) / 8;
		
		c.add(Calendar.DAY_OF_MONTH, days);
		c.add(Calendar.HOUR_OF_DAY, hours);
		
		this.setEndDate(c.getTime());
		
		//this.setStartDate(jobs.get(0).getFromTime());
	}

	public boolean isPlanned() {
		return isPlanned;
	}

	public void setPlanned(boolean isPlanned) {
		this.isPlanned = isPlanned;
	}

	public void clearJobs() {
		this.jobs = new ArrayList<Job>();
	}

	public boolean containsJob(Employee employee) {
		for (Job job : jobs) {
			if (job.getEmployee().getName().equals(employee.getName())) return true;
		}
		
		return false;
	}

}
