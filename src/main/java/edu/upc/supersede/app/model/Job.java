package edu.upc.supersede.app.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import edu.upc.supersede.app.model.Requirement.State;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "JOBS")
public class Job extends AbstractEntity{

	private int internalId;

	private String name;
	
	private double devTime;
	
	private int earlyStart;
	private int latestStart;

	private Date fromTime;
	private Date toTime;
	
	private State state;
	private double dedication;
	
	@OneToOne
	private Requirement requirement;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Employee employee;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name="JobSuccessors", 
	                joinColumns={@JoinColumn(name="JobId")}, 
	                inverseJoinColumns={@JoinColumn(name="ParentId")})
	private List<Job> successors;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name="JobPredecessors", 
	                joinColumns={@JoinColumn(name="ParentId")}, 
	                inverseJoinColumns={@JoinColumn(name="JobId")})
	private List<Job> predecessors;

	
	Job() {}

	public Job (int internalId, String name) {
		this.internalId = internalId;
		this.name = name;
		
		this.successors = new ArrayList<Job>();
		this.predecessors = new ArrayList<Job>();
	}
		
	public Job(String name, double devTime, Requirement requirement, Long projectId) {
		this.name = name;
		
		this.requirement = requirement;
		this.devTime = devTime;
		
		this.successors = new ArrayList<Job>();
		this.predecessors = new ArrayList<Job>();
		
		this.setProjectId(projectId);
	}

	public Job(String name, double devTime, Requirement requirement, List<Job> successors, Long projectId) {
		this.name = name;
		
		this.successors = new ArrayList<Job>();
		this.predecessors = new ArrayList<Job>();
		
		this.setProjectId(projectId);
	}
	
	public Job (Job job) {
		this.internalId = job.getInternalId();
		this.name = job.getName();
		
		this.devTime = job.getDevTime();
		
		this.earlyStart = job.getEarlyStart();
		this.employee = job.getEmployee();
		
		this.successors = new ArrayList<Job>();
		this.predecessors = new ArrayList<Job>();
		
		this.fromTime = job.getFromTime();
		this.toTime = job.getToTime();
		
		this.setProjectId(job.getProjectId());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public double getDevTime() {
		return devTime;
	}

	public void setDevTime(double devTime) {
		this.devTime = devTime;
	}
	
	public int getEarlyStart() {
		return earlyStart;
	}

	public void setEarlyStart(int earlyStart) {
		this.earlyStart = earlyStart;
	}

	public int getLatestStart() {
		return latestStart;
	}

	public void setLatestStart(int latestStart) {
		this.latestStart = latestStart;
	}

	public Requirement getRequirement() {
		return requirement;
	}

	public void setRequirement(Requirement requirement) {
		this.requirement = requirement;
	}
	
	public int getInternalId() {
		return this.internalId;
	}

	public List<Job> getSuccessors() {
		return successors;
	}	
	
	public void setSuccessors(List<Job> successors) {
		this.successors = successors;
	}

	public List<Job> getPredecessors() {
		return predecessors;
	}

	public void setPredecessors(List<Job> jobs) {
		this.predecessors = jobs;
	}

	public void addPredecessor(Job job) {
		this.predecessors.add(job);
	}
	
	public void addPredecessors(List<Job> jobs) {
		this.predecessors.addAll(jobs);
	}
	
	public void addSuccessor(Job job) {
		this.successors.add(job);
	}
	
	public void addSuccessors(List<Job> jobs) {
		this.successors.addAll(jobs);
	}
	
	
	public void setInternalId(int internalId) {
		this.internalId = internalId;
	}

	public Date getFromTime() {
		return fromTime;
	}

	public void setFromTime(Date fromTime) {
		this.fromTime = fromTime;
		updateDate(8);
	}

	public Date getToTime() {
		return toTime;
	}

	public void setToTime(Date toTime) {
		this.toTime = toTime;
	}
	
	private void updateDate(int hoursPerDay) {
		//Add days to date.
		int days = (int)Math.rint(this.devTime) % hoursPerDay;
		
		Calendar c = Calendar.getInstance();
		c.setTime(this.fromTime);
		c.add(Calendar.DATE, days); 
		
		this.setToTime(c.getTime());
	}
	
	
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public void removeSuccessor(Job job) {
		successors.remove(job);
		//job.removePredecessor(this);
	}
	
	public void removePredecessor (Job job) {
		predecessors.remove(job);
	}
	
	public void purge() {
		int i = 0;
		while (i < predecessors.size()) {
			if (predecessors.get(i).getDevTime() == 0.0) {
				predecessors.remove(i);
			} else {
				i++;
			}
		}
		
		i = 0;
		while (i < successors.size()) {
			if (successors.get(i).getDevTime() == 0.0) {
				successors.remove(i);
			} else {
				i++;
			}
		}
	}
	
	public void updateTime(Date startTime) {
		int jobTime, maxTime;
		Calendar c = Calendar.getInstance();
		
		for (Job succ : this.getSuccessors()) {
			maxTime = this.earlyStart + (int)Math.rint(getDevTime());
			
			for (Job pred : succ.getPredecessors()) {
				jobTime = pred.getEarlyStart() + (int)pred.getDevTime(); 
				if (jobTime > maxTime) {
					maxTime = jobTime;
				}
			}
			
			succ.setEarlyStart(maxTime);
			succ.updateTime(startTime);
			
			c.setTime(startTime);
			int hours = maxTime %  (int)Math.rint(8 * 1.0);
			int days = maxTime / (int)Math.rint(8 * 1.0);
			c.add(Calendar.DAY_OF_MONTH, days); 
			c.add(Calendar.HOUR_OF_DAY, hours);
			
			succ.setFromTime(c.getTime());
			
			hours = (int)(Math.rint(getDevTime())) %  (int)Math.rint(8 * 1.0);
			days = (int)(Math.rint(getDevTime())) / (int)Math.rint(8 * 1.0);
			
			c.setTime(succ.getFromTime());
			c.add(Calendar.DAY_OF_MONTH, days); 
			c.add(Calendar.HOUR_OF_DAY, hours);
			
			succ.setToTime(c.getTime());
		}
	}
	
	public void postposeStart(int unitsTime) {
		int start = this.getEarlyStart();
		start += (int) Math.rint(unitsTime);
		this.setEarlyStart(start);
	
		int days = (int)(Math.rint(unitsTime)) %  (int)Math.rint(8 * 1.0);
		
		Calendar c = Calendar.getInstance();
		c.setTime(this.getFromTime());
		c.add(Calendar.DATE, days); 
		
		this.setFromTime(c.getTime());
		
		for (Job succ : this.getSuccessors()) {
			succ.postposeStart(unitsTime);
		} 
	}
	
	public State getState() {
		return this.state;
	}
	
	public void setState(State state) {
		this.state = state;
	}
	
	public double getDedication() {
		return dedication;
	}

	public void setDedication(double dedication) {
		this.dedication = dedication;
	}

	public boolean equals (Object o2) {
		Job j2 = (Job)o2;
		
		return this.getInternalId() == j2.getInternalId();
	}
	
	public String toString() {
		String s = "JOB " + requirement.getName() + "\n";
		s += "Start: " + earlyStart;
		
		if (employee != null) {
			s += "Employee: " + employee.getName();
		}
		return s;
	}

	public boolean isRunning(int t) {
		return t >= this.getEarlyStart() && t < (this.getEarlyStart() + this.devTime);
	}

	public void clearDependencies() {
		this.successors.clear();
		this.predecessors.clear();
	}
}
