package edu.upc.supersede.app.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import javax.persistence.*;

@Entity
@Table (name = "PROJECTS")
public class Project extends AbstractEntity {

	
	private String name;
	private String description;
	private Date dateCreation;
	private int defaultEmployees;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Release> releases;
	
	Project() {}

	public Project(String name, String description,  Date dateCreation, int maxEmployees) {

		this.name = name;
		this.description = description;
		this.dateCreation = dateCreation;

		this.releases = new ArrayList<Release>();
		
		Random r = new Random();
		this.setProjectId(r.nextLong());
		this.setId(this.getProjectId());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}


	public List<Release> getReleases() {
		return releases;
	}

	public void setReleases (List<Release> releases) {
		this.releases = releases;
	}

	public int getDefaultEmployees() {
		return defaultEmployees;
	}

	public void setDefaultEmployees(int defaultEmployees) {
		this.defaultEmployees = defaultEmployees;
	}
}
