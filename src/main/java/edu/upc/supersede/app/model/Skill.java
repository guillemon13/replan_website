package edu.upc.supersede.app.model;

import java.util.Random;

import javax.persistence.*;

@Entity
@Table (name = "SKILLS")
public class Skill extends AbstractEntity {

	
	private String name;
	private String abbr;
	private String description;
	
	Skill () { } //JPA

	public Skill (String name, String abbr, String description, Long projectId) {
		this.name = name;
		this.setAbbr(abbr);
		this.description = description;
		this.setProjectId(projectId);
	}
	
	public Skill (Long id, String name, String abbr, String description, Long projectId) {
		this.setId(id);
		
		this.name = name;
		this.setAbbr(abbr);
		this.description = description;
		this.setProjectId(projectId);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getAbbr() {
		return abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}
	
	public boolean equals (Skill s) {
		if (this.getId() == s.getId()) {
			return true; 
		} else if (this.getName().equals(s.getName()) && this.getAbbr().equals(s.getAbbr())){
			return true;
		} else return false;
	}
	
	public String toString() {
		return "Skill " + this.getId() + ". " + this.name;
	}
}
