package edu.upc.supersede.app.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TASKS")
public class Task extends AbstractEntity {
	private String idTask;
	private String name;
	private Date fromDate;
	private Date toDate;
	private int priority;
		
	@ElementCollection
	private List<String> dependencies;
	
	/*@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="TaskDependencies", 
    joinColumns={@JoinColumn(name="TaskId")}, 
    inverseJoinColumns={@JoinColumn(name="ParentId")})
	private List<Task> dependencies;*/
	
	Task () {}
	
	public Task (String id, String name, Date from, Date to, int priority, Long projectId) {
		this.idTask = id;
		this.name = name;
		this.fromDate = from;
		this.toDate = to;
		this.setPriority(priority);
		
		this.dependencies = new ArrayList<String>();
		
		this.setProjectId(projectId);
	}
	
	public Task (String name, Date from, Date to, int priority, List<String> dependencies, Long projectId) {
		this.name = name;
		this.fromDate = from;
		this.toDate = to;
		
		this.dependencies = dependencies;
		this.setProjectId(projectId);
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	public List<String> getDependencies() {
		return this.dependencies;
	}
	
	public void addDependency (String dep) {
		this.dependencies.add(dep);
	}

	public String getIdTask() {
		return idTask;
	}

	public void setId(String idTask) {
		this.idTask = idTask;
	}

}
