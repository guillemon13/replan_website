package edu.upc.supersede.app.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.*;

@Entity
@Table (name = "RELEASES")
public class Release extends AbstractEntity {

	private String name;
	
	private String description;
	
	private Date initialDate;
	private Date deadline;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Employee> resources;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Requirement> requirements;
	
	private boolean hasPlans;
	
	public Release() {}

	public Release(String name, String description, Date initialDate, 
			Date deadline, Long projectId) {

		this.name = name;
		this.description = description;
		this.setInitialDate(initialDate);
		this.deadline = deadline;
		
		this.resources = new ArrayList<Employee>();
		this.requirements = new ArrayList<Requirement>();
		
		this.setProjectId(projectId);
	}
	
	public Release(String name, String description, Date initialDate, 
			Date deadline, List<Employee> resources, 
			List<Requirement> requirements, Long projectId) {

		this.name = name;
		this.description = description;
		this.setInitialDate(initialDate);
		this.deadline = deadline;

		this.resources = resources;
		this.requirements = requirements;
		
		this.setProjectId(projectId);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public List<Employee> getResources() {
		return resources;
	}

	public void setResources (List<Employee> resources) {
		this.resources = resources;
	}
	
	public Requirement searchRequirement(Long id) {
		for (Requirement req : requirements) {
			if (req.getId() == id) return req;
		}
		
		return null;
	}
	
	public List<Requirement> getRequirements() {
		return requirements;
	}

	public void setRequirements(List<Requirement> requirements) {
		this.requirements = requirements;
	}

	public void extendWeeks(int numWeeks) {
		Date newDate = new Date(deadline.getTime());

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(newDate);
		calendar.add(Calendar.DATE, 7 * numWeeks);
		newDate.setTime(calendar.getTime().getTime());

		this.deadline = newDate;
	}

	public boolean isHasPlans() {
		return hasPlans;
	}

	public void setHasPlans(boolean hasPlans) {
		this.hasPlans = hasPlans;
	}

	public Requirement getRequirementByName(String name) {
		for (Requirement req : this.requirements) {
			if (req.getName().equals(name)) return req;
		}
		
		return null;
	}

	public Employee getEmployeeByName(String name) {
		for (Employee empl : this.resources) {
			if (empl.getName().equals(name)) return empl;
		}
		
		return null;
	}
}
