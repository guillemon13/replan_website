package edu.upc.supersede.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import edu.upc.supersede.app.dto.plan.TaskDTO;

@Entity
@Table(name = "DEVELOPERS")
public class Developer extends AbstractEntity {

	private String name;

	@ElementCollection
	private List<String> children;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Task> tasks;
	
	Developer() {}
	
	public Developer (String name, Long projectId) {
		this.name = name;
		this.tasks = new ArrayList<Task>();
		this.children = new ArrayList<String>();
		
		this.setProjectId(projectId);
	}
	
	public Developer (String name, List<Task> tasks, List<String> children, Long projectId) {
		this.name = name;
		this.tasks = tasks;
		this.children = children;
		this.setProjectId(projectId);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void addTask(Task task) {
		this.tasks.add(task);
	}
	
	public List<String> getChildren() {
		return children;
	}
	
	public void addChild(String child) {
		this.children.add(child);
	}
	
}
