package edu.upc.supersede.app.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * The User JPA entity.
 *
 */
@Entity
@Table(name = "USERS")
@NamedQueries({
        @NamedQuery(
                name = User.FIND_BY_USERNAME,
                query = "select u from User u where username = :username"
        )
})
public class User extends AbstractEntity {

    public static final String FIND_BY_USERNAME = "user.findByUserName";
    public static final String COUNT_TODAYS_CALORIES = "user.todaysCalories";

    private String username;
    private String passwordDigest;
    private String email;

    private long currentProject;
    private long selectedRelease;
    
    @OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    private List<Project> projects;
    
    public User() {

    }

    public User(String username, String passwordDigest, String email, Long projectId) {
        this.username = username;
        this.passwordDigest = passwordDigest;
        this.email = email;
        
        this.setCurrentProject(projectId);
        
        this.projects = new ArrayList<Project>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordDigest() {
        return passwordDigest;
    }

    public void setPasswordDigest(String passwordDigest) {
        this.passwordDigest = passwordDigest;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void addProject (Project project) {
    	this.projects.add(project);
    }
    
	public List<Project> getProjects() {
		return this.projects;
	}
	
	public long getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(long currentProject) {
		this.currentProject = currentProject;
	}
	
	public long getSelectedRelease() {
		return selectedRelease;
	}
	
	public void setSelectedRelease(long releaseId) {
		this.selectedRelease = releaseId;
	}

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", projectId=" + getProjectId() +
                '}';
    }




}
