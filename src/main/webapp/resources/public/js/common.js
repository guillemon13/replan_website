angular.module('UserApp', ['ngMessages', 'ngMaterial', 'ngAnimate', 'spring-security-csrf-token-interceptor'])
    .controller('BaseFormCtrl', ['$scope', '$http', function ($scope, $http, $mdDialog, $mdMedia) {

        var fieldWithFocus;

        $scope.vm = {
            submitted: false,
            errorMessages: []
        };

        $scope.focus = function (fieldName) {
            fieldWithFocus = fieldName;
        };

        $scope.blur = function (fieldName) {
            fieldWithFocus = undefined;
        };

        $scope.isMessagesVisible = function (fieldName) {
            return fieldWithFocus === fieldName || $scope.vm.submitted;
        };

        $scope.preparePostData = function () {
            var username = $scope.vm.username != undefined ? $scope.vm.username : '';
            var password = $scope.vm.password != undefined ? $scope.vm.password : '';
            var email = $scope.vm.email != undefined ? $scope.vm.email : '';
            var projectId = $scope.vm.projectId != undefined ? $scope.vm.projectId : '';

            return 'username=' + username + '&password=' + password + '&email=' + email;
        }


    }])
    .controller('NewUserCtrl', ['$scope', '$http', '$mdDialog', '$mdMedia', function ($scope, $http, $mdDialog, $mdMedia) {

        $scope.createUser = function () {
            console.log('Creating user with username ' + $scope.vm.username + ' and password ' + $scope.vm.password);

            $scope.vm.submitted = true;

            if ($scope.form.$invalid) {
                return;
            }

            var postData = {
                username: $scope.vm.username,
                plainTextPassword: $scope.vm.password,
                email: $scope.vm.email
            };

            $http({
                method: 'POST',
                url: '/user',
                data: postData,
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "text/plain"
                }
            })
            .then(function (response) {
                if (response.status == 200) {
                    $mdDialog.show(
                            $mdDialog.alert()
                              .parent(angular.element(document.querySelector('#mainScreen')))
                              .clickOutsideToClose(true)
                              .title('Registration OK')
                              .textContent("The user has been registered")
                              .ariaLabel('Alert')
                              .ok('Continue Login')
                              .targetEvent(event)
                        ).finally(function() {
                        	$scope.login($scope.vm.userName, $scope.vm.password);    	
                        });
                    
                }
                else {
                    $mdDialog.show(
                            $mdDialog.alert()
                              .parent(angular.element(document.querySelector('#mainScreen')))
                              .clickOutsideToClose(true)
                              .title('ERROR')
                              .textContent("Error at registering")
                              .ariaLabel('Alert')
                              .ok('OK')
                              .targetEvent(event)
                        );
                }
            });
        }
        

        $scope.login = function (username, password) {
            var postData = $scope.preparePostData();

            $http({
                method: 'POST',
                url: '/authenticate',
                data: postData,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "X-Login-Ajax-call": 'true'
                }
            })
            .then(function(response) {
                if (response.data == 'ok') {
                    window.location.replace('/resources/MainScreen_replan.html');
                }
                else {
                  $mdDialog.show(
                          $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#mainScreen')))
                            .clickOutsideToClose(true)
                            .title('ERROR at Authentication!')
                            .textContent("Password not correct")
                            .ariaLabel('Alert')
                            .ok('OK')
                            .targetEvent(event)
                      );

                }
            });
        };
        
    }])
    .controller('LoginCtrl', ['$scope', '$http', '$mdDialog', '$mdMedia', function ($scope, $http, $mdDialog, $mdMedia) {

        $scope.onLogin = function () {
            console.log('Attempting login with username ' + $scope.vm.username + ' and password ' + $scope.vm.password);

            $scope.vm.submitted = true;

            if ($scope.form.$invalid) {
                return;
            }

            $scope.login($scope.vm.userName, $scope.vm.password);

        };

        $scope.login = function (username, password) {
            var postData = $scope.preparePostData();

            $http({
                method: 'POST',
                url: '/authenticate',
                data: postData,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "X-Login-Ajax-call": 'true'
                }
            })
            .then(function(response) {
                if (response.data == 'ok') {
                    window.location.replace('/resources/MainScreen_replan.html');
                }
                else {
                  $mdDialog.show(
                          $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#mainScreen')))
                            .clickOutsideToClose(true)
                            .title('ERROR at Authentication!')
                            .textContent("Password not correct")
                            .ariaLabel('Alert')
                            .ok('OK')
                            .targetEvent(event)
                      );

                }
            });
        };

    }])
    .directive('checkPasswordsMatch', function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ngModel) {
                ngModel.$validators.checkPasswordsMatch = function (modelValue, viewValue) {
                    if (scope.vm && scope.vm.password && viewValue) {
                        return scope.vm.password === viewValue;
                    }
                    return true;
                };
            }
        };
    });
