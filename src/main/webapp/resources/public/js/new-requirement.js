angular.module('newRequirementApp', ['common', 'spring-security-csrf-token-interceptor', 'editableTableWidgets'])
    .controller('NewRequirementCtrl', ['$scope', '$http', function ($scope, $http) {

        $scope.createUser = function () {
            console.log('Creating requirement with name ' + $scope.vm.name);

            $scope.vm.submitted = true;

            if ($scope.form.$invalid) {
                return;
            }

            //TODO: Set POST data properly.
            var postData = {
                username: $scope.vm.username,
                plainTextPassword: $scope.vm.password,
                email: $scope.vm.email
            };

            $http({
                method: 'POST',
                url: '/requirement',
                data: postData,
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "text/plain"
                }
            })
            .then(function (response) {
                if (response.status == 200) {
                    $scope.login($scope.vm.userName, $scope.vm.password);
                }
                else {
                    $scope.vm.errorMessages = [];
                    $scope.vm.errorMessages.push({description: response.data});
                    console.log("failed user creation: " + response.data);
                }
            });
        }
    }]);