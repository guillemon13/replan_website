angular.module('skillCreatorApp', ['editableTableWidgets', 'frontendServices', 'spring-security-csrf-token-interceptor'])
    .controller('NewSkillCtrl', ['$scope' , 'SkillService', 'UserService', '$timeout',
        function ($scope, SkillService, UserService, $timeout) {

    	$scope.vm = {
                originalRelease: [],
                errorMessages: [],
                infoMessages: []
            };

    	
	        updateUserInfo();
	        
	        function updateUserInfo() {
	            UserService.getUserInfo()
	                .then(function (userInfo) {
	                    $scope.vm.userName = userInfo.userName;
	                    $scope.vm.projectId = userInfo.projectId;
	                },
	                function (errorMessage) {
	                    showErrorMessage(errorMessage);
	                });
	        }
	        
            function showErrorMessage(errorMessage) {
                clearMessages();
                $scope.vm.errorMessages.push({description: errorMessage});
            }

            function clearMessages() {
                $scope.vm.errorMessages = [];
                $scope.vm.infoMessages = [];
            }

            function showInfoMessage(infoMessage) {
                $scope.vm.infoMessages = [];
                $scope.vm.infoMessages.push({description: infoMessage});
                $timeout(function () {
                    $scope.vm.infoMessages = [];
                }, 1000);
            }

            $scope.delete = function () {
                var deletedSkillIds = _.chain($scope.vm.skills)
                    .filter(function (release) {
                        return skill.selected && !skill.new;
                    })
                    .map(function (skill) {
                        return skill.id;
                    })
                    .value();

                Skillervice.deleteSkill(deletedSkillId)
                    .then(function () {
                        clearMessages();
                        showInfoMessage("deletion successful.");
                    },
                    
                    function () {
                        clearMessages();
                        $scope.vm.errorMessages.push({description: "deletion failed."});
                    });
            };

            function prepareSkillDto() {                
            	return {
                    name: $scope.vm.nameSkill,
                    description: $scope.vm.description,
                };
            }

            $scope.createSkill = function () {
                var original = prepareSkillDto();

                $scope.vm.errorMessages = [];

                // save all new items plus the ones that where modified
                SkillService.saveSkill(original).then(function () {    
                        showInfoMessage("Changes saved successfully");
                    },
                    function (errorMessage) {
                        showErrorMessage(errorMessage);
                    });

            };
}]);

