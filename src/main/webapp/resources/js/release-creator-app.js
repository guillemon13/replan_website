angular.module('requirementCreatorApp', ['editableTableWidgets', 'frontendServices', 'spring-security-csrf-token-interceptor'])
    .controller('ReleaseController', ['$scope' , 'ReleaseService', 'UserService', 'RequirementService', '$timeout',
        function ($scope, ReleaseService, UserService, RequirementService, $timeout) {

    	$scope.vm = {
                originalRelease: [],
                errorMessages: [],
                infoMessages: []
            };

            function showErrorMessage(errorMessage) {
                clearMessages();
                $scope.vm.errorMessages.push({description: errorMessage});
            }

            function clearMessages() {
                $scope.vm.errorMessages = [];
                $scope.vm.infoMessages = [];
            }

            function showInfoMessage(infoMessage) {
                $scope.vm.infoMessages = [];
                $scope.vm.infoMessages.push({description: infoMessage});
                $timeout(function () {
                    $scope.vm.infoMessages = [];
                }, 1000);
            }

            updateUserInfo();

            function updateUserInfo() {
                UserService.getUserInfo()
                    .then(function (userInfo) {
                        $scope.vm.userName = userInfo.userName;
                        $scope.vm.projectId = userInfo.projectId;
                          $scope.vm.projects = userInfo.projects;
                    },
                    function (errorMessage) {
                        showErrorMessage(errorMessage);
                    });
            }

            $scope.extendDate = function () {
            	$scope.vm.deadline = Date.parse(scope.vm.deadline);
            	$scope.vm.deadline.setDate($scope.vm.deadline.getDate() + 365*4);
            }

            $scope.add = function () {
                $scope.vm.release.unshift({
                    name: null,
                    description: null,
                    date: null,
                    capacity: null,
                    effort: null
                });
            };

            $scope.delete = function () {
                var deletedReleaseIds = _.chain($scope.vm.meals)
                    .filter(function (release) {
                        return release.selected && !release.new;
                    })
                    .map(function (release) {
                        return release.id;
                    })
                    .value();

                ReleaseService.deleteRelease(deletedReleaseId)
                    .then(function () {
                        clearMessages();
                        showInfoMessage("deletion successful.");
                    },

                    function () {
                        clearMessages();
                        $scope.vm.errorMessages.push({description: "deletion failed."});
                    });
            };

            function prepareReleaseDto() {
            	return {
                    name: $scope.vm.nameRelease,
                    description: $scope.vm.description,
                    initDate: $scope.vm.initDate,
                    deadline: $scope.vm.deadline,
                    capacity: $scope.vm.capacityRelease,
                    effort: $scope.vm.effort
                };
            }

            $scope.createRelease = function () {
                var original = prepareReleaseDto();

                $scope.vm.errorMessages = [];

                // save all new items plus the ones that where modified
                ReleaseService.saveRelease(original).then(function () {
                        showInfoMessage("Changes saved successfully");
                    },
                    function (errorMessage) {
                        showErrorMessage(errorMessage);
                    });

            };

            $scope.logout = function () {
                UserService.logout();
            }
}]);
