angular.module('requirementCreatorApp', ['editableTableWidgets', 'frontendServices',
                                         'spring-security-csrf-token-interceptor', 'ui.multiselect'])
    .controller('RequirementCtrl', ['$scope' , 'RequirementService', 'UserService', 'SkillService', '$timeout','$stateParams',
        function ($scope, RequirementService, UserService, SkillService, $timeout, $stateParams) {

    		$scope.vm = {
                errorMessages: [],
                infoMessages: []
            };

	        updateUserInfo();

	        function loadComponents() {
            	SkillService.getProjectSkills($scope.vm.projectId)
	        		.then(function (components) {
	        			$scope.vm.availableComponents = components;
	        		},
	        		function (errorMessage) {
	        			showErrorMessage(errorMessage);
	        		});

	        }

	        function loadRequirements() {
            	RequirementService.getProjectRequirements($scope.vm.projectId)
	        		.then(function (requirements) {
	        			$scope.vm.availableRequirements = requirements;
	        		},
	        		function (errorMessage) {
	        			showErrorMessage(errorMessage);
	        		});

	        }

	        function updateUserInfo() {
	            UserService.getUserInfo()
	                .then(function (userInfo) {
	                    $scope.vm.userName = userInfo.userName;
	                    $scope.vm.projectId = userInfo.projectId;
	                    $scope.vm.projects = userInfo.projects;

                      if ($stateParams.releaseID > 0) {
                        RequirementService.getRequirementById($stateParams.releaseID)
                          .then(function (requirement) {
                              parseDtoModel(requirement);
                          },
                          function (errorMessage) {
                            showErrorMessage(errorMessage);
                          });
                      }

	                    loadRequirements();
	                    loadComponents();
	                },
	                function (errorMessage) {
	                    showErrorMessage(errorMessage);
	                });
	        }

            function showErrorMessage(errorMessage) {
                clearMessages();
                $scope.vm.errorMessages.push({description: errorMessage});
            }

            function clearMessages() {
                $scope.vm.errorMessages = [];
                $scope.vm.infoMessages = [];
            }

            function showInfoMessage(infoMessage) {
                $scope.vm.infoMessages = [];
                $scope.vm.infoMessages.push({description: infoMessage});
                $timeout(function () {
                    $scope.vm.infoMessages = [];
                }, 1000);
            }

            $scope.delete = function (deletedRequirementId) {
                RequirementService.deleteRequirement(deletedRequirementId)
                    .then(function () {
                        clearMessages();
                        showInfoMessage("deletion successful.");
                    },

                    function () {
                        clearMessages();
                        $scope.vm.errorMessages.push({description: "deletion failed."});
                    });
            };

            function prepareRequirementDto() {
            	return {
                    name: $scope.vm.nameRequirement,
                    effort: $scope.vm.description,
                    skills: $scope.vm.components,
                    deadline: $scope.vm.date,
                    predecessors: $scope.vm.dependencies,
                    resources: $scope.vm.resources,
                    maxEmployees : $scope.vm.maxEmployees
                };
            }

            function parseDtoModel(dto) {
              $scope.vm.nameRequirement = dto.name;
              $scope.vm.description = dto.description;
              $scope.vm.components = dto.skills;
              $scope.vm.date = dto.deadline;
              $scope.vm.dependencies = dto.predecessors;
              $scope.vm.maxEmployees = dto.maxEmployees;
            }

            $scope.createRequirement = function () {
                var original = prepareRequirementDto();

                $scope.vm.errorMessages = [];

                // save all new items plus the ones that where modified
                RequirementService.saveRequirement(original).then(function () {
                        showInfoMessage("Changes saved successfully");
                    },
                    function (errorMessage) {
                        showErrorMessage(errorMessage);
                    });

            };

            $scope.logout = function () {
                UserService.logout();
            }

}]);
