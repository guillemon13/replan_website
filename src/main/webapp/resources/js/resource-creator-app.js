angular.module('resourceCreatorApp', ['editableTableWidgets', 'frontendServices', 'spring-security-csrf-token-interceptor'])
    .filter('excludeDeleted', function () {
        return function (input) {
            return _.filter(input, function (item) {
                return item.deleted == undefined || !item.deleted;
            });
        }
    })
    .controller('ResourcesCtrl', ['$scope' , 'EmployeeService', 'UserService', 'SkillService', '$timeout',
        function ($scope, EmployeeService, UserService, SkillService, $timeout) {

            $scope.vm = {
                originalEmployees: [],
                employees: [],
                isSelectionEmpty: true,
                errorMessages: [],
                infoMessages: [],
                components: []
            };

            updateUserInfo();
            loadComponents();
            loadEmployeeData();
            
            $scope.diagComp = false;
            
            $scope.showAlert = function(){
                if ($scope.diagComp == false) {
                	$scope.diagComp = true;    
                } else {
                	$scope.diagComp = false;
                }
            };
            
            function showErrorMessage(errorMessage) {
                clearMessages();
                $scope.vm.errorMessages.push({description: errorMessage});
            }

            function updateUserInfo() {
                UserService.getUserInfo()
                    .then(function (userInfo) {
                        $scope.vm.userName = userInfo.userName;
                        $scope.vm.projectId = userInfo.projectId;
                    },
                    function (errorMessage) {
                        showErrorMessage(errorMessage);
                    });
            }

            function loadComponents() {
            	SkillService.getProjectSkills($scope.vm.projectId)
            		.then(function (components) {
            			$scope.vm.components = components;
            		},
            		function (errorMessage) {
            			showErrorMessage(errorMessage);
            		});
            }
            
            function markAppAsInitialized() {
                if ($scope.vm.appReady == undefined) {
                    $scope.vm.appReady = true;
                }
            }

            function loadEmployeeData() {
                EmployeeService.searchEmployeesByProjectId($scope.vm.projectId)
                	.then(function (employees) {
                        $scope.vm.errorMessages = [];
                        $scope.vm.originalEmployees = employees;

                        $scope.vm.employees = _.cloneDeep($scope.vm.originalEmployees);

                        _.each($scope.vm.employees, function (employee) {
                            employee.selected = false;
                        });

                        markAppAsInitialized();

                        if ($scope.vm.employees && $scope.vm.employees.length == 0) {
                            showInfoMessage("No results found.");
                        }
                    },
                    function (errorMessage) {
                        showErrorMessage(errorMessage);
                        markAppAsInitialized();
                    });
            }

            function clearMessages() {
                $scope.vm.errorMessages = [];
                $scope.vm.infoMessages = [];
            }

            function showInfoMessage(infoMessage) {
                $scope.vm.infoMessages = [];
                $scope.vm.infoMessages.push({description: infoMessage});
                $timeout(function () {
                    $scope.vm.infoMessages = [];
                }, 1000);
            }

            $scope.selectionChanged = function () {
                $scope.vm.isSelectionEmpty = !_.any($scope.vm.meals, function (meal) {
                    return meal.selected && !meal.deleted;
                });
            };

            $scope.pages = function () {
                return _.range(1, $scope.vm.totalPages + 1);
            };

            $scope.add = function () {
                $scope.vm.employees.unshift({
                    id: null,
                    name: null,
                    effort: null,
                    skill: null,
                    projectId : -1, 
                    selected: false,
                    new: true
                });
            };

            $scope.delete = function () {
                var deletedEmployeeIds = _.chain($scope.vm.employees)
                    .filter(function (employee) {
                        return employee.selected && !employee.new;
                    })
                    .map(function (employee) {
                        return employee.id;
                    })
                    .value();

                EmployeeService.deleteEmployees(deletedEmployeeIds)
                    .then(function () {
                        clearMessages();
                        showInfoMessage("deletion successful.");

                        _.remove($scope.vm.employees, function(employee) {
                            return employee.selected;
                        });

                        $scope.selectionChanged();
                        updateUserInfo();

                    },
                    function () {
                        clearMessages();
                        $scope.vm.errorMessages.push({description: "deletion failed."});
                    });
            };

            $scope.reset = function () {
                $scope.vm.employees = $scope.vm.originalEmployees;
            };

            function getNotNew(employees) {
                return  _.chain(employees)
                    .filter(function (employee) {
                        return !employee.new;
                    })
                    .value();
            }

            function prepareEmployeeDto(employees) {
                return  _.chain(employees)
                    .map(function (employee) {
                        return {
                            id: employee.id,
                            name: employee.name,
                            skill: employee.component,
                            availability: employee.availability
                        }
                    })
                    .value();
            }

            $scope.save = function () {

                var maybeDirty = prepareEmployeeDto(getNotNew($scope.vm.employees));

                var original = prepareEmployeesDto(getNotNew($scope.vm.originalEmployees));

                var dirty = _.filter(maybeDirty).filter(function (employee) {

                    var originalEmployee = _.filter(original, function (orig) {
                        return orig.id === employee.id;
                    });

                    if (originalEmployee.length == 1) {
                        originalEmployee = originalEmployee[0];
                    }

                    return originalEmployee && ( originalEmployee.name != employee.name ||
                        originalEmployee.description != employee.description || originalEmployee.skills != employee.skills)
                });

                var newItems = _.filter($scope.vm.employees, function (employee) {
                    return employee.new;
                });

                var saveAll = prepareEmployeesDto(newItems);
                saveAll = saveAll.concat(dirty);

                $scope.vm.errorMessages = [];

                // save all new items plus the ones that where modified
                EmployeeService.saveEmployees(saveAll).then(function () {
                        $scope.search();
                        showInfoMessage("Changes saved successfully");
                        updateUserInfo();
                    },
                    function (errorMessage) {
                        showErrorMessage(errorMessage);
                    });

            };

            $scope.logout = function () {
                UserService.logout();
            }


        }]);

