var projectModule = angular.module('ProjectApp', ["dndLists", "ui.router", "frontendServices", "spring-security-csrf-token-interceptor",
                                                  "ngMaterial", "ngSanitize", "ngAnimate", "ui.multiselect", "anim-in-out"]);

projectModule.config(function($stateProvider, $urlRouterProvider){
    $stateProvider
    	.state('editRelease', {
	        url: "/editRelease",
	        templateUrl: "/resources/release_setup.html",
	        params : { releaseID: -1, },
	        controller: 'ReleaseCtrl'
    	})
    	.state('releaseMgmt', {
	        url: "/releaseMgmt",
	        templateUrl: "/resources/release_mgmt.html",
	        controller: 'MainCtrl'
    	})
      .state('editRequirement', {
          url: "/editRequirement",
          templateUrl: "/resources/requirement_creation.html",
          params : { requirementID : 0},
          controller: 'RequirementCtrl'
      })
      .state('newProject', {
          url: "/newProject",
          templateUrl: "/resources/new-project.html",
          controller: 'ProjectCtrl'
      });
    
    $urlRouterProvider.otherwise("/releaseMgmt");
})

projectModule.controller('MainCtrl', ['$scope', '$filter', '$http',
                'ReleaseService', 'UserService', 'RequirementService', 'PlanService', '$timeout', '$mdDialog', '$mdMedia', '$window', '$state', 
    function ($scope, $filter, $http, ReleaseService, UserService, RequirementService, PlanService, $timeout, $mdDialog, $mdMedia, $window, $state) {

	$scope.vm = {
			  selected : null,
			  projectReleases : [],
        availableRequirements : []
	  };

	updateUserInfo();

  $scope.models = {
      selected: null,
      lists: {"A": [], "B": [], "C" : []},
      availableRequirements : []
  };

  function updateUserInfo() {
      UserService.getUserInfo()
          .then(function (userInfo) {
              $scope.vm.userName = userInfo.userName;
              $scope.vm.projectId = userInfo.projectId;
              $scope.vm.projects = userInfo.projects;
              
              if ($scope.vm.projectId == 0) {
            	  $state.go('newProject');
              } else {
  				angular.forEach ($scope.vm.projects, function(project) {
					if ($scope.vm.projectId === project.id) {
						$scope.vm.projectName = project.name;
					} 
				});
              }
              
              loadRequirements();
          },
          function (errorMessage) {
          	$mdDialog.show(
		    	      $mdDialog.alert()
		    	        .parent(angular.element(document.querySelector('#mainScreen')))
		    	        .clickOutsideToClose(true)
		    	        .title('ERROR LOADING USER')
		    	        .textContent(errorMessage)
		    	        .ariaLabel('Alert')
		    	        .ok('OK')
		    	        .targetEvent(event)
            	);
          });
  }

  $scope.changeUserProject = function(projectId) {
      UserService.changeUserProject(projectId)
	        .then(function (userInfo) {
	            $scope.vm.userName = userInfo.userName;
	            $scope.vm.projectId = userInfo.projectId;
	            $scope.vm.projects = userInfo.projects;

	            //loadReleases();
	            $window.location.reload();
	        },
      function (errorMessage) {
      	$mdDialog.show(
		    	      $mdDialog.alert()
		    	        .parent(angular.element(document.querySelector('#mainScreen')))
		    	        .clickOutsideToClose(true)
		    	        .title('ERROR!')
		    	        .textContent(errorMessage)
		    	        .ariaLabel('Alert')
		    	        .ok('OK')
		    	        .targetEvent(event)
          	);
      });
  }

	function loadRequirements() {
		RequirementService.getProjectAvailableRequirements($scope.vm.projectId)
			.then(function (requirements) {
		        for (var i = 0; i < requirements.length; i++) {
		        	$scope.models.availableRequirements.push({
		                id : requirements[i].id,
		                name : requirements[i].name
		              });
		        }
				loadReleases();
			},
			function (errorMessage) {
	        	$mdDialog.show(
			    	      $mdDialog.alert()
			    	        .parent(angular.element(document.querySelector('#mainScreen')))
			    	        .clickOutsideToClose(true)
			    	        .title('ERROR!')
			    	        .textContent(errorMessage)
			    	        .ariaLabel('Alert')
			    	        .ok('OK')
			    	        .targetEvent(event)
	            	);
			});
	};

	function loadReleases() {
		ReleaseService.getProjectReleases($scope.vm.projectId)
			.then(function (releases) {
				angular.forEach (releases, function(release) {
					$scope.vm.projectReleases.push({
						id : release.id,
						name : release.name,
						requirements: angular.copy(release.requirements),
						hasPlans: release.hasPlans
					});
				});
			},

			function (errorMessage) {
	        	$mdDialog.show(
			    	      $mdDialog.alert()
			    	        .parent(angular.element(document.querySelector('#mainScreen')))
			    	        .clickOutsideToClose(true)
			    	        .title('ERROR!')
			    	        .textContent(errorMessage)
			    	        .ariaLabel('Alert')
			    	        .ok('OK')
			    	        .targetEvent(event)
	            	);
			});
	};

	function prepareRequirementsIds (requirements) {
		var ids = [];

		angular.forEach(requirements, function(requirement) {
  		  this.push(requirement.id);
  	  }, ids);

		return ids;
	}

    $scope.deleteRequirement = function (deletedRequirementId) {
		var confirm = $mdDialog.confirm()
        .title('Features deletion')
        .textContent('Do you really want to delete the feature?')
        .ariaLabel('Features successors')
        .targetEvent(event)
        .ok('Do it!')
        .cancel('Cancel');

		 $mdDialog.show(confirm).then(function() {
			 RequirementService.deleteRequirement(deletedRequirementId)
	            .then(function () {
	            	
	            	 //Delete from list.
		   			 var reqIds = _.chain($scope.models.availableRequirements)
				        .map(function (requirement) {
				            return requirement.id;
				        })
				     .value();
		
					 var indexDeleted = reqIds.indexOf(deletedRequirementId);
					 $scope.models.availableRequirements.splice(indexDeleted, 1);
					 $scope.vm.selected = null;
					 
		        	$mdDialog.show(
				    	 $mdDialog.alert()
				    	        .parent(angular.element(document.querySelector('#mainScreen')))
				    	        .clickOutsideToClose(true)
				    	        .title('Feature deleted!')
				    	        .textContent(errorMessage)
				    	        .ariaLabel('You have just deleted the feature')
				    	        .ok('OK')
				    	        .targetEvent(event)
		            	);
	            },

	            function () {
		        	$mdDialog.show(
				    	      $mdDialog.alert()
				    	        .parent(angular.element(document.querySelector('#mainScreen')))
				    	        .clickOutsideToClose(true)
				    	        .title('ERROR!')
				    	        .textContent(errorMessage)
				    	        .ariaLabel('Error at deleting the feature!')
				    	        .ok('OK')
				    	        .targetEvent(event)
		            	);
	            }); 
		 });
    	
    	
    };
	
  $scope.moveReleaseRequirement = function(index, release) {
		var req = release.requirements[index];

		if (req.successors.length > 0) {
			var confirm = $mdDialog.confirm()
	        .title('Features update')
	        .textContent('This feature has successors. Do you want to move them all?')
	        .ariaLabel('Features successors')
	        .targetEvent(event)
	        .ok('Continue')
	        .cancel('Cancel');

			 $mdDialog.show(confirm).then(function() {
				     deleteSuccessors(req, release);
				     var deletedReq = release.requirements.splice(index, 1);
			 }, function() {
				 var reqIds = _.chain($scope.models.availableRequirements)
			        .map(function (requirement) {
			            return requirement.id;
			        })
			     .value();

				 var indexDeleted = reqIds.indexOf(req.id);
				 $scope.models.availableRequirements.splice(indexDeleted, 1);
			 });
		} else {
	      var deletedReq = release.requirements.splice(index, 1);
	    }
	}

	function deleteSuccessors (requirement, release) {
		angular.forEach(requirement.successors, function(succ) {

	        var reqIds = _.chain(release.requirements)
	            .map(function (requirement) {
	                return requirement.id;
	            })
	         .value();
	         
	        var indexSucc = reqIds.indexOf(succ.id);

	        if (indexSucc != -1) {
	  	    	deletedReq = release.requirements.splice(indexSucc, 1);
	  	    	$scope.models.availableRequirements.push(
	  	    			{
	  		                id : deletedReq[0].id,
	  		                name : deletedReq[0].name,
	  		                state : deletedReq[0].state
	  	    			}
	  	    	);

	  	    	if (succ.successors.length > 0) {
	  	    		deleteSuccessors(succ, release);
	  	    	}
	       }
		    });
	}

	$scope.seePlan = function (idRelease) {
		UserService.selectRelease(idRelease).then(function() {
			$window.location.href = "/resources/demo-gantt.html";			
		});
	}
	
	$scope.saveRequirements = function (release, event) {
    	ReleaseService.saveReleaseRequirements(release).then(function () {

        	$mdDialog.show(
		    	      $mdDialog.alert()
		    	        .parent(angular.element(document.querySelector('#mainScreen')))
		    	        .clickOutsideToClose(true)
		    	        .title('Operation Completed!')
		    	        .textContent('Features saved properly.')
		    	        .ariaLabel('Alert')
		    	        .ok('OK')
		    	        .targetEvent(event)
          	);
    	},
	    	function (errorMessage) {
	        	$mdDialog.show(
			    	      $mdDialog.alert()
			    	        .parent(angular.element(document.querySelector('#mainScreen')))
			    	        .clickOutsideToClose(true)
			    	        .title('ERROR!')
			    	        .textContent(errorMessage)
			    	        .ariaLabel('Alert')
			    	        .ok('OK')
			    	        .targetEvent(event)
	            	);
    	});
		
		
	}
	
    $scope.updateRequirements = function (release, event) {
	    	var reqIds = _.chain(release.requirements)
		        .map(function (requirement) {
		            return requirement.id;
		        })
	        .value();

	    	ReleaseService.saveReleaseRequirements(release).then(function () {
	    		var confirmRePlan = $mdDialog.confirm()
		        .title('Build Plan')
		        .textContent('Would you like to RePlan the release?')
		        .ariaLabel('Lucky day')
		        .targetEvent(event)
		        .ok('Do it!')
		        .cancel('Cancel');

		    	$mdDialog.show(confirmRePlan).then(function() {

		            PlanService.getPlanService(release.id).then(function () {
		            	$mdDialog.show(
				    	      $mdDialog.alert()
				    	        .parent(angular.element(document.querySelector('#mainScreen')))
				    	        .clickOutsideToClose(true)
				    	        .title('Operation Completed!')
				    	        .textContent('Plan created successfully.')
				    	        .ariaLabel('Alert')
				    	        .ok('Show it!')
				    	        .targetEvent(event)
		            	).finally(function() {
		    	        	UserService.selectRelease(release.id).then(function() {
			        			$window.location.href = "/resources/demo-gantt.html";			
			        		});
		    	        });
					},
					function (errorMessage) {
			        	$mdDialog.show(
					    	      $mdDialog.alert()
					    	        .parent(angular.element(document.querySelector('#mainScreen')))
					    	        .clickOutsideToClose(true)
					    	        .title('ERROR!')
					    	        .htmlContent(errorMessage[0])
					    	        .ariaLabel('Alert')
					    	        .ok('OK')
					    	        .targetEvent(event)
			            	);
					});

				  }, function() {
				    $mdDialog.show(
				    	      $mdDialog.alert()
				    	        .parent(angular.element(document.querySelector('#mainScreen')))
				    	        .clickOutsideToClose(true)
				    	        .title('Operation Canceled!')
				    	        .textContent('You have cancelled the operation!')
				    	        .ariaLabel('Alert')
				    	        .ok('OK')
				    	        .targetEvent(event)
				    	    );
				  });
	    	},

	    	function (errorMessage) {
	        	$mdDialog.show(
			    	      $mdDialog.alert()
			    	        .parent(angular.element(document.querySelector('#mainScreen')))
			    	        .clickOutsideToClose(true)
			    	        .title('ERROR!')
			    	        .textContent(errorMessage)
			    	        .ariaLabel('Alert')
			    	        .ok('OK')
			    	        .targetEvent(event)
	            	);
	    	});



    	}

        $scope.logout = function () {
            UserService.logout();
        }
      }]);



projectModule.controller('ReleaseCtrl',
		['$scope', '$filter', '$http', 'UserService', 'EmployeeService', 'ReleaseService', '$mdDialog', '$mdMedia', '$stateParams',
    function ($scope, $filter, $http, UserService, EmployeeService, ReleaseService, $mdDialog, $mdMedia, $stateParams) {
      $scope.showModal = false;

      $scope.toggleModal = function(){
    	  var releaseEmployeesIds = _.chain($scope.vm.releaseEmployees)
	          .map(function (employee) {
	              return employee.id;
	          })
	          .value();

			$scope.vm.availableEmployees = _.chain($scope.vm.availableEmployees )
	          .filter(function (employee) {
	              return releaseEmployeesIds.indexOf(employee.id) == -1;
	          }).value();


			if ($scope.vm.availableEmployees.length > 0) {
				$scope.selectedEmployee = $scope.vm.availableEmployees[0];
			}

          $scope.showModal = !$scope.showModal;
      };

  	  $scope.vm = {
  			 deadline : null,
  			 initialdate : null,
  			 originalEmployees : [],
  			 availableEmployees : [],
  			 selected : null,
  			 release : null
      };

  	  $scope.selectedEmployee = {
  			  id: -1,
  			  name : '',
  			  skills : [],
  			  effort : 0
  	  }

      updateUserInfo();

      function updateUserInfo() {
          UserService.getUserInfo()
              .then(function (userInfo) {
                  $scope.vm.userName = userInfo.userName;
                  $scope.vm.projectId = userInfo.projectId;
                  $scope.vm.projects = userInfo.projects;

                  loadAllResources();
                  loadReleaseResources();
              },
              function (errorMessage) {
              	$mdDialog.show(
  		    	      $mdDialog.alert()
  		    	        .parent(angular.element(document.querySelector('#mainScreen')))
  		    	        .clickOutsideToClose(true)
  		    	        .title('ERROR!')
  		    	        .textContent(errorMessage)
  		    	        .ariaLabel('Alert')
  		    	        .ok('OK')
  		    	        .targetEvent(event)
              	);
              });
      }

      $scope.freeEmployee = function (index) {
    	  var removedEmployee = $scope.vm.releaseEmployees.splice(index, 1);
    	  //$scope.vm.availableEmployees.push(removedEmployee);
      }

  	   function loadAllResources() {
  		   EmployeeService.searchEmployeesByProjectId($scope.vm.projectId)
  			.then(function (employees) {
  				$scope.vm.availableEmployees = employees;
  			},
  			function (errorMessage) {
  	        	$mdDialog.show(
  		    	      $mdDialog.alert()
  		    	        .parent(angular.element(document.querySelector('#mainScreen')))
  		    	        .clickOutsideToClose(true)
  		    	        .title('ERROR!')
  		    	        .textContent(errorMessage)
  		    	        .ariaLabel('Alert')
  		    	        .ok('OK')
  		    	        .targetEvent(event)
              	);
  			});
  	   }

  	   function loadReleaseResources() {
  		   if ($stateParams.releaseID > 0) {
  			 ReleaseService.getReleaseInfo($stateParams.releaseID)
				.then(function (release) {
					$scope.vm.release = angular.copy(release);
					
					$scope.vm.release.initialDate = new Date($scope.vm.release.initialDate);
					$scope.vm.release.deadline = new Date($scope.vm.release.deadline);
					
					$scope.vm.releaseEmployees = $scope.vm.release.resources;
				},
				function (errorMessage) {
		        	$mdDialog.show(
				    	      $mdDialog.alert()
				    	        .parent(angular.element(document.querySelector('#mainScreen')))
				    	        .clickOutsideToClose(true)
				    	        .title('ERROR!')
				    	        .textContent(errorMessage)
				    	        .ariaLabel('Alert')
				    	        .ok('OK')
				    	        .targetEvent(event)
		            	);
				});
  		 	}
  		  };

          function prepareEmployeesIds(employees) {
        	  var ids = [];

        	  angular.forEach(employees, function(employee) {
        		  this.push(employee.id);
        	  }, ids);

        	  return ids;
          }

          $scope.saveRelease = function () {

              //var selectedEmployees = prepareEmployeesDto($scope.vm.originalEmployees);
              $scope.vm.release.resources = $scope.vm.releaseEmployees;
              $scope.vm.release.deadline = $scope.vm.deadline;
              $scope.vm.release.initialDate = $scope.vm.initialdate;
              $scope.vm.errorMessages = [];

                // save all new items plus the ones that where modified
	          	ReleaseService.saveRelease($scope.vm.release).then(function () {
	    		    $mdDialog.show(
	  		    	      $mdDialog.alert()
	  		    	        .parent(angular.element(document.querySelector('#nrs_screen')))
	  		    	        .clickOutsideToClose(true)
	  		    	        .title('Operation completed!')
	  		    	        .textContent('You have saved the release!')
	  		    	        .ariaLabel('Alert')
	  		    	        .ok('OK')
	  		    	        .targetEvent(event)
	  		    	    );

	    		    $window.location.href = '/MainScreen_replan.html';
	          	},
	          	function (errorMessage) {
	            	$mdDialog.show(
	  		    	      $mdDialog.alert()
	  		    	        .parent(angular.element(document.querySelector('#mainScreen')))
	  		    	        .clickOutsideToClose(true)
	  		    	        .title('ERROR!')
	  		    	        .textContent(errorMessage)
	  		    	        .ariaLabel('Alert')
	  		    	        .ok('OK')
	  		    	        .targetEvent(event)
	              	);
	          	});
          }

          function extendDeadLine (deadline) {
        	  $scope.vm.deadline.add(4, 'years');
          }

      	  $scope.showSkills = function(skills) {
        		var display = '';
        		angular.forEach(skills, function(skill) {
        		   display = display + skill.abbr + " ";
        		});

        	    return skills.length ? display : 'Not set';
      	  };

          $scope.addEmployee = function() {
        	  $scope.vm.releaseEmployees.push($scope.selectedEmployee)
          }

          $scope.logout = function () {
              UserService.logout();
          }

        }]);

projectModule.controller('RequirementCtrl',
		['$scope' , 'RequirementService', 'UserService', 'SkillService', '$timeout','$stateParams', '$mdDialog', '$mdMedia', '$window', '$stateParams', '$state', 
		function ($scope, RequirementService, UserService, SkillService, $timeout, $stateParams, $mdDialog, $mdMedia, $window,$stateParams, $state) {

			updateUserInfo();
			
			$scope.vm = {
				nameRequirement : '',
				effort : '',
				components : [],
				deadline : '',
				dependencies : [],
				priority : '',
				maxEmployees : ''
			};
			
			
	        function updateUserInfo() {
	            UserService.getUserInfo()
	                .then(function (userInfo) {
	                    $scope.vm.userName = userInfo.userName;
	                    $scope.vm.projectId = userInfo.projectId;
	                    $scope.vm.projects = userInfo.projects;

	                    if ($scope.vm.projectId == 0) {
	                  	  $state.go('newProject');
	                    }
	                    
                      if ($stateParams.requirementID > 0) {
                        RequirementService.getRequirementById($stateParams.requirementID)
                          .then(function (requirement) {
                              parseDtoModel(requirement);
                          },
                          function (errorMessage) {
                          	$mdDialog.show(
              		    	      $mdDialog.alert()
              		    	        .parent(angular.element(document.querySelector('#mainScreen')))
              		    	        .clickOutsideToClose(true)
              		    	        .title('ERROR at loading Feature')
              		    	        .textContent(errorMessage)
              		    	        .ariaLabel('Alert')
              		    	        .ok('OK')
              		    	        .targetEvent(event)
                          	);
                          });
                       } 
	                    loadRequirements();
	                    loadComponents();
	                },
	                function (errorMessage) {
	                		
	                });
	        }


			function loadComponents() {
            	SkillService.getProjectSkills($scope.vm.projectId)
	        		.then(function (components) {
	        			$scope.vm.availableComponents = components;
	        		},
	        		function (errorMessage) {
	                	$mdDialog.show(
	      		    	      $mdDialog.alert()
	      		    	        .parent(angular.element(document.querySelector('#mainScreen')))
	      		    	        .clickOutsideToClose(true)
	      		    	        .title('ERROR LOADING SKILLS!')
	      		    	        .textContent(errorMessage)
	      		    	        .ariaLabel('Alert')
	      		    	        .ok('OK')
	      		    	        .targetEvent(event)
	                  	);
	        		});

	        }

	        function loadRequirements() {
            	RequirementService.getProjectRequirements($scope.vm.projectId)
	        		.then(function (requirements) {
	        			   $scope.vm.availableRequirements = angular.copy(_.chain(requirements)
          			        .map(function (requirement) {
          			            return requirement.id;
          			        })
          			     .value());
	        		},
	        		function (errorMessage) {
	                	$mdDialog.show(
	      		    	      $mdDialog.alert()
	      		    	        .parent(angular.element(document.querySelector('#mainScreen')))
	      		    	        .clickOutsideToClose(true)
	      		    	        .title('ERROR LOADING FEATURES')
	      		    	        .textContent(errorMessage)
	      		    	        .ariaLabel('Alert')
	      		    	        .ok('OK')
	      		    	        .targetEvent(event)
	                  	);
	        		});

	        }

            function prepareRequirementDto() {
            	return {
                    name: $scope.vm.nameRequirement,
                    effort: $scope.vm.effort,
                    skills: $scope.vm.components === undefined ? [] : $scope.vm.components,
                    deadline: $scope.vm.deadline,
                    predecessors: $scope.vm.dependencies === undefined ? [] : $scope.vm.dependencies,
                    maxEmployees : $scope.vm.maxEmployees
                };
            }

            function parseDtoModel(dto) {
              $scope.vm.nameRequirement = dto.name;
              $scope.vm.effort = dto.effort;
              $scope.vm.components = dto.skills;
              $scope.vm.deadline = dto.deadline;
              $scope.vm.dependencies = dto.predecessors;
              $scope.vm.priority = dto.criteria;
              $scope.vm.maxEmployees = dto.maxEmployees;
            }

            $scope.createRequirement = function () {
                var original = prepareRequirementDto();

                $scope.vm.errorMessages = [];

                // save all new items plus the ones that where modified
                RequirementService.saveRequirement(original).then(function () {
	            		$mdDialog.show(
				    	      $mdDialog.alert()
				    	        .parent(angular.element(document.querySelector('#mainScreen')))
				    	        .clickOutsideToClose(true)
				    	        .title('Operation Completed!')
				    	        .textContent('Feature created successfully!')
				    	        .ariaLabel('Alert')
				    	        .ok('OK')
				    	        .targetEvent(event)
		            	).finally(function() {
		            		$window.location.href = "/releaseMgmt";
		            	});
	            		
	            		
                    },
                    function (errorMessage) {
		            	$mdDialog.show(
					    	      $mdDialog.alert()
					    	        .parent(angular.element(document.querySelector('#mainScreen')))
					    	        .clickOutsideToClose(true)
					    	        .title('ERROR!')
					    	        .textContent(errorMessage)
					    	        .ariaLabel('Alert')
					    	        .ok('OK')
					    	        .targetEvent(event)
			            	);
                    });

            };

            $scope.logout = function () {
                UserService.logout();
            }
}]);

projectModule.controller('ProjectCtrl', ['$scope', '$http', '$mdDialog', '$mdMedia', 'ProjectService','$state', function ($scope, $http, $mdDialog, $mdMedia, ProjectService, $state) {
	
    $scope.createProject = function () {
        $scope.vm.submitted = true;

        if ($scope.form.$invalid) {
            return;
        }

        var postData = {
            name: $scope.vm.nameProject,
            description: $scope.vm.description,
            defaultEmployees: 1
        };

        ProjectService.saveProject(postData).then(function (response) {
            
            $mdDialog.show(
                    $mdDialog.alert()
                      .parent(angular.element(document.querySelector('#mainScreen')))
                      .clickOutsideToClose(true)
                      .title('Project Creation OK')
                      .textContent("The project has been created")
                      .ariaLabel('Alert')
                      .ok('Continue to Main Screen')
                      .targetEvent(event)
             ).finally(function() {
            	 $state.go("releaseMgmt");
             });
        }, function (errorMessage) {
            $mdDialog.show(
                    $mdDialog.alert()
                      .parent(angular.element(document.querySelector('#mainScreen')))
                      .clickOutsideToClose(true)
                      .title('ERROR')
                      .textContent(errorMessage)
                      .ariaLabel('Alert')
                      .ok('OK')
                      .targetEvent(event)
                );
        });
    }
}]);

projectModule.directive('modal', function () {
    return {
      template: '<div class="modal fade">' +
          '<div class="modal-dialog">' +
            '<div class="modal-content">' +
              '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title }}</h4>' +
              '</div>' +
              '<div class="modal-body" ng-transclude></div>' +
            '</div>' +
          '</div>' +
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });
