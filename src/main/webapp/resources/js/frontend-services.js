

angular.module('frontendServices', [])
    .service('MealService', ['$http', '$q', function($http, $q) {
        return {
            searchMeals: function(fromDate, fromTime, toDate, toTime, pageNumber) {
                var deferred = $q.defer();

                function prepareTime(time) {
                    return time ? '1970/01/01 ' + time : null;
                }

                $http.get('/meal/',{
                    params: {
                        fromDate: fromDate,
                        toDate: toDate,
                        fromTime: prepareTime(fromTime),
                        toTime: prepareTime(toTime),
                        pageNumber: pageNumber
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve(response.data);
                    }
                    else {
                        deferred.reject('Error retrieving list of meals');
                    }
                });

                return deferred.promise;
            },

            deleteMeals: function(deletedMealIds) {
                var deferred = $q.defer();

                $http({
                    method: 'DELETE',
                    url: '/meal',
                    data: deletedMealIds,
                    headers: {
                        "Content-Type": "application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                        deferred.reject('Error deleting meals');
                    }
                });

                return deferred.promise;
            },

            saveMeals: function(dirtyMeals) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: '/meal',
                    data: dirtyMeals,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                    deferred.reject("Error saving meals: " + response.data);
                    }
                });

                return deferred.promise;
            }
        }
    }])
    .service('UserService', ['$http','$q', function($http, $q) {
        return {
            getUserInfo: function() {
                var deferred = $q.defer();

                $http.get('/user')
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            changeUserProject: function(pId) {
                var deferred = $q.defer();

                $http.get('/user/changeProject', {
                    params: {
                        projectId: pId
                    }
	            })
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            selectRelease: function(rId) {
                var deferred = $q.defer();

                $http.get('/user/selectRelease', {
                    params: {
                        releaseId: rId
                    }
	            })
                    .then(function (response) {
                        if (response.status == 200) {
                            deferred.resolve(response.data);
                        }
                        else {
                            deferred.reject('Error retrieving user info');
                        }
                });

                return deferred.promise;
            },
            logout: function () {
                $http({
                    method: 'POST',
                    url: '/logout'
                })
                .then(function (response) {
                    if (response.status == 200) {
                    window.location.reload();
                    }
                    else {
                        console.log("Logout failed!");
                    }
                });
            }
        };
    }])
	.service('RequirementService', ['$http','$q', function($http, $q) {
	    return {
          getRequirementById: function(reqId) {
            var deferred = $q.defer();

              $http.get('/requirements', {
                    params: {
                        requirementId: reqId
                    }
              }).then(function (response) {
                      if (response.status == 200) {
                          deferred.resolve(response.data);
                      }
                      else {
                          deferred.reject('Error retrieving user info');
                      }
              });

              return deferred.promise;
          },
	        getProjectRequirements: function(pId) {
	            var deferred = $q.defer();

	            $http.get('/requirements/search/', {
                    params: {
                        projectId: pId
                    }
	            }).then(function (response) {
	                    if (response.status == 200) {
	                        deferred.resolve(response.data);
	                    }
	                    else {
	                        deferred.reject('Error retrieving user info');
	                    }
	            });

	            return deferred.promise;
	        },
	        getProjectAvailableRequirements: function(pId) {
	            var deferred = $q.defer();

	            $http.get('/requirements/searchAvailable/', {
                    params: {
                        projectId: pId
                    }
	            }).then(function (response) {
	                    if (response.status == 200) {
	                        deferred.resolve(response.data);
	                    }
	                    else {
	                        deferred.reject('Error retrieving user info');
	                    }
	            });

	            return deferred.promise;
	        },
	        saveRequirement: function(dirtyRequirement) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: '/requirements',
                    data: dirtyRequirement,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                    deferred.reject("Error saving release: " + response.data);
                    }
                });

                return deferred.promise;
            },
            deleteRequirement: function(deletedRequirementId) {
                var deferred = $q.defer();

                $http.delete('/requirements/', {params: {reqId: deletedRequirementId}})
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                        deferred.reject('Error deleting meals');
                    }
                });

                return deferred.promise;
            },
	    };
	}])
	.service('EmployeeService', ['$http','$q', function($http, $q) {
	    return {
	    	searchEmployeesByProjectId: function(projectId) {
	            var deferred = $q.defer();

	            $http.get('/employees/search/',{
                    params: {
                        projectId: projectId
                    }
                }).then(function (response) {
	                    if (response.status == 200) {
	                        deferred.resolve(response.data);
	                    }
	                    else {
	                        deferred.reject('Error retrieving user info');
	                    }
	            });

	            return deferred.promise;
	        },
	        saveEmployees: function(dirtyEmployees) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: '/employees',
                    data: dirtyEmployees,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                    deferred.reject("Error saving release: " + response.data);
                    }
                });

                return deferred.promise;
            },
            deleteEmployee: function(deletedEmployeeIds) {
                var deferred = $q.defer();

                $http({
                    method: 'DELETE',
                    url: '/employees',
                    data: deletedEmployeeIds,
                    headers: {
                        "Content-Type": "application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                        deferred.reject('Error deleting meals');
                    }
                });

                return deferred.promise;
            },
	    };
	}])
	.service('SkillService', ['$http','$q', function($http, $q) {
	    return {
	        getProjectSkills: function(projectId) {
	            var deferred = $q.defer();

	            $http.get('/skills/search/', {
                    params: {
                        projectId: projectId
                    }
	            }).then(function (response) {
	                    if (response.status == 200) {
	                        deferred.resolve(response.data);
	                    }
	                    else {
	                        deferred.reject('Error retrieving user info');
	                    }
	            });

	            return deferred.promise;
	        },
	        saveSkill: function(dirtySkill) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: '/skills',
                    data: dirtySkill,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                    deferred.reject("Error saving release: " + response.data);
                    }
                });

                return deferred.promise;
            },
            deleteSkills: function(deletedSkillId) {
                var deferred = $q.defer();

                $http({
                    method: 'DELETE',
                    url: '/skills',
                    data: deletedSkillId,
                    headers: {
                        "Content-Type": "application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                        deferred.reject('Error deleting meals');
                    }
                });

                return deferred.promise;
            },
	    };
	}])
	.service('ReleaseService', ['$http','$q', function($http, $q) {
	    return {
	    	getProjectReleases: function(pId) {
	    		var deferred = $q.defer();

	            $http.get('/releases/', {
                    params: {
                        projectId: pId
                    }
	              })
	                .then(function (response) {
	                    if (response.status == 200) {
	                        deferred.resolve(response.data);
	                    }
	                    else {
	                        deferred.reject('Error retrieving user info');
	                    }
	            });

	            return deferred.promise;
	    	},
	        getReleaseInfo: function(rId) {
	            var deferred = $q.defer();

	            $http.get('/releases/search/', {
                    params: {
                        releaseId: rId
                    }
	              })
	                .then(function (response) {
	                    if (response.status == 200) {
	                        deferred.resolve(response.data);
	                    }
	                    else {
	                        deferred.reject('Error retrieving user info');
	                    }
	            });

	            return deferred.promise;
	        },
            saveRelease: function(dirtyRelease) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: '/releases',
                    data: dirtyRelease,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                    deferred.reject("Error saving release: " + response.data);
                    }
                });

                return deferred.promise;
            },
            saveReleaseRequirements: function(dirtyRelease) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: '/releases/saveReqs',
                    data: dirtyRelease,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                    deferred.reject("Error saving release: " + response.data);
                    }
                });

                return deferred.promise;
            },
            setRequirements: function(idRelease, reqs) {
                var deferred = $q.defer();

                $http({
                    method: 'PUT',
                    url: '/releases/updateRequirements/' + idRelease,
                    data: { requirements: reqs},
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                    deferred.reject("Error saving release: " + response.data);
                    }
                });

                return deferred.promise;
            },
            deleteRelease: function(deletedReleaseId) {
                var deferred = $q.defer();

                $http({
                    method: 'DELETE',
                    url: '/releases',
                    data: deletedReleaseId,
                    headers: {
                        "Content-Type": "application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                        deferred.reject('Error deleting meals');
                    }
                });

                return deferred.promise;
            }
	    };
	}])
	.service('LoremIpsumService', ['$http','$q', function($http, $q) {
	    return {
	        getReleaseInfo: function(rId) {
	            var deferred = $q.defer();

	            $http.get('/plans/generatePlan/', {
                    params: {
                        releaseId: rId
                    }
	              })
	                .then(function (response) {
	                    if (response.status == 200) {
	                        deferred.resolve(response.data);
	                    }
	                    else {
	                        deferred.reject('Error retrieving user info');
	                    }
	            });

	            return deferred.promise;
	        },
            saveRelease: function(dirtyRelease) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: '/releases',
                    data: dirtyRelease,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                    deferred.reject("Error saving release: " + response.data);
                    }
                });

                return deferred.promise;
            },
            setRequirements: function(idRelease, requirements) {
                var deferred = $q.defer();

                $http({
                    method: 'PUT',
                    url: '/releases/updateRequirements/' + idRelease,
                    data: requirements,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                    deferred.reject("Error saving release: " + response.data);
                    }
                });

                return deferred.promise;
            },
            deleteRelease: function(deletedReleaseId) {
                var deferred = $q.defer();

                $http({
                    method: 'DELETE',
                    url: '/releases',
                    data: deletedReleaseId,
                    headers: {
                        "Content-Type": "application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                        deferred.reject('Error deleting meals');
                    }
                });

                return deferred.promise;
            }
	    };
	}])
  .service('ProjectService', ['$http','$q', function($http, $q) {
      return {
          getProjectInfo: function(id) {
              var deferred = $q.defer();

              $http({
                  url: '/projects/id/',
                  method: "GET",
                  params: {projectId: id}
               })
              .then(function (response) {
                  if (response.status == 200) {
                      deferred.resolve(response.data);
                  }
                  else {
                      deferred.reject('Error retrieving user info');
                  }
              });

              return deferred.promise;
          },
            saveProject: function(dirtyProject) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: '/projects',
                    data: dirtyProject,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "text/plain, application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                    deferred.reject("Error saving project: " + response.data);
                    }
                });

                return deferred.promise;
            },
            deleteProject: function(deletedProjectId) {
                var deferred = $q.defer();

                $http({
                    method: 'DELETE',
                    url: '/projects',
                    data: deletedProjectId,
                    headers: {
                        "Content-Type": "application/json"
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        deferred.resolve();
                    }
                    else {
                        deferred.reject('Error deleting project');
                    }
                });

                return deferred.promise;
            }
      };
  }])
  .service('PlanService', ['$http','$q', function($http, $q) {
      return {
    	  getPlanService: function(rId) {
              var deferred = $q.defer();

              $http.get('/plans/generatePlan/', {
                  params: {
                      releaseId: rId
                  }
	           }).then(function (response) {
	                if (response.status == 200) {
	                    deferred.resolve(response.data);
	                }
	                else {
	                    deferred.reject(response.data);
	                }
	            });

              return deferred.promise;
          },
    	  getGeneratedPlans: function() {
              var deferred = $q.defer();

              $http.get('/plans/').then(function (response) {
	                if (response.status == 200) {
	                    deferred.resolve(response.data);
	                }
	                else {
	                    deferred.reject(response.data);
	                }
	            });

              return deferred.promise;
          },
          rollbackPlans: function(idPlans) {
              var deferred = $q.defer();

              $http({
                  method: 'DELETE',
                  url: '/plans',
                  data: idPlans,
                  headers: {
                      "Content-Type": "application/json",
                      "Accept": "text/plain, application/json"
                  }
              })
              .then(function (response) {
                  if (response.status == 200) {
                      deferred.resolve();
                  }
              });

              return deferred.promise;
          },

          getSampleData: function() {
              return [
                      // Order is optional. If not specified it will be assigned automatically
                      {name: 'Milestones', height: '3em', sortable: false, classes: 'gantt-row-milestone', color: '#45607D', tasks: [
                          // Dates can be specified as string, timestamp or javascript date object. The data attribute can be used to attach a custom object
                          {name: 'Kickoff', color: '#93C47D', from: '2013-10-07T09:00:00', to: '2013-10-07T10:00:00', data: 'Can contain any custom data or object'},
                          {name: 'Concept approval', color: '#93C47D', from: new Date(2013, 9, 18, 18, 0, 0), to: new Date(2013, 9, 18, 18, 0, 0), est: new Date(2013, 9, 16, 7, 0, 0), lct: new Date(2013, 9, 19, 0, 0, 0)},
                          {name: 'Development finished', color: '#93C47D', from: new Date(2013, 10, 15, 18, 0, 0), to: new Date(2013, 10, 15, 18, 0, 0)},
                          {name: 'Shop is running', color: '#93C47D', from: new Date(2013, 10, 22, 12, 0, 0), to: new Date(2013, 10, 22, 12, 0, 0)},
                          {name: 'Go-live', color: '#93C47D', from: new Date(2013, 10, 29, 16, 0, 0), to: new Date(2013, 10, 29, 16, 0, 0)}
                      ], data: 'Can contain any custom data or object'},
                      {name: 'Status meetings', tasks: [
                          {name: 'Demo #1', color: '#9FC5F8', from: new Date(2013, 9, 25, 15, 0, 0), to: new Date(2013, 9, 25, 18, 30, 0)},
                          {name: 'Demo #2', color: '#9FC5F8', from: new Date(2013, 10, 1, 15, 0, 0), to: new Date(2013, 10, 1, 18, 0, 0)},
                          {name: 'Demo #3', color: '#9FC5F8', from: new Date(2013, 10, 8, 15, 0, 0), to: new Date(2013, 10, 8, 18, 0, 0)},
                          {name: 'Demo #4', color: '#9FC5F8', from: new Date(2013, 10, 15, 15, 0, 0), to: new Date(2013, 10, 15, 18, 0, 0)},
                          {name: 'Demo #5', color: '#9FC5F8', from: new Date(2013, 10, 24, 9, 0, 0), to: new Date(2013, 10, 24, 10, 0, 0)}
                      ]},
                      {name: 'Kickoff', movable: {allowResizing: false}, tasks: [
                          {name: 'Day 1', color: '#9FC5F8', from: new Date(2013, 9, 7, 9, 0, 0), to: new Date(2013, 9, 7, 17, 0, 0),
                              progress: {percent: 100, color: '#3C8CF8'}, movable: false},
                          {name: 'Day 2', color: '#9FC5F8', from: new Date(2013, 9, 8, 9, 0, 0), to: new Date(2013, 9, 8, 17, 0, 0),
                              progress: {percent: 100, color: '#3C8CF8'}},
                          {name: 'Day 3', color: '#9FC5F8', from: new Date(2013, 9, 9, 8, 30, 0), to: new Date(2013, 9, 9, 12, 0, 0),
                              progress: {percent: 100, color: '#3C8CF8'}}
                      ]},
                      {name: 'Create concept', tasks: [
                          {name: 'Create concept', priority: 20, content: '<i class="fa fa-cog" ng-click="scope.handleTaskIconClick(task.model)"></i> {{task.model.name}}', color: '#F1C232', from: new Date(2013, 9, 10, 8, 0, 0), to: new Date(2013, 9, 16, 18, 0, 0), est: new Date(2013, 9, 8, 8, 0, 0), lct: new Date(2013, 9, 18, 20, 0, 0),
                              progress: 100}
                      ]},
                      {name: 'Finalize concept', tasks: [
                          {id: 'Finalize concept', name: 'Finalize concept', priority: 10, color: '#F1C232', from: new Date(2013, 9, 17, 8, 0, 0), to: new Date(2013, 9, 18, 18, 0, 0),
                              progress: 100}
                      ]},
                      {name: 'Development', children: ['Sprint 1', 'Sprint 2', 'Sprint 3', 'Sprint 4'], content: '<i class="fa fa-file-code-o" ng-click="scope.handleRowIconClick(row.model)"></i> {{row.model.name}}'},
                      {name: 'Sprint 1', tooltips: false, tasks: [
                          {id: 'Product list view', name: 'Product list view', color: '#F1C232', from: new Date(2013, 9, 21, 8, 0, 0), to: new Date(2013, 9, 25, 15, 0, 0),
                              progress: 25, dependencies: [{to: 'Order basket'}, {from: 'Finalize concept'}]}
                      ]},
                      {name: 'Sprint 2', tasks: [
                          {id: 'Order basket', name: 'Order basket', color: '#F1C232', from: new Date(2013, 9, 28, 8, 0, 0), to: new Date(2013, 10, 1, 15, 0, 0),
                              dependencies: {to: 'Checkout'}}
                      ]},
                      {name: 'Sprint 3', tasks: [
                          {id: 'Checkout', name: 'Checkout', color: '#F1C232', from: new Date(2013, 10, 4, 8, 0, 0), to: new Date(2013, 10, 8, 15, 0, 0),
                              dependencies: {to: 'Login & Signup & Admin Views'}}
                      ]},
                      {name: 'Sprint 4', tasks: [
                          {id: 'Login & Signup & Admin Views', name: 'Login & Signup & Admin Views', color: '#F1C232', from: new Date(2013, 10, 11, 8, 0, 0), to: new Date(2013, 10, 15, 15, 0, 0),
                              dependencies: [{to: 'HW'}, {to: 'SW / DNS/ Backups'}]}
                      ]},
                      {name: 'Hosting'},
                      {name: 'Setup', tasks: [
                          {id: 'HW', name: 'HW', color: '#F1C232', from: new Date(2013, 10, 18, 8, 0, 0), to: new Date(2013, 10, 18, 12, 0, 0)}
                      ]},
                      {name: 'Config', tasks: [
                          {id: 'SW / DNS/ Backups', name: 'SW / DNS/ Backups', color: '#F1C232', from: new Date(2013, 10, 18, 12, 0, 0), to: new Date(2013, 10, 21, 18, 0, 0)}
                      ]},
                      {name: 'Server', parent: 'Hosting', children: ['Setup', 'Config']},
                      {name: 'Deployment', parent: 'Hosting', tasks: [
                          {name: 'Depl. & Final testing', color: '#F1C232', from: new Date(2013, 10, 21, 8, 0, 0), to: new Date(2013, 10, 22, 12, 0, 0), 'classes': 'gantt-task-deployment'}
                      ]},
                      {name: 'Workshop', tasks: [
                          {name: 'On-side education', color: '#F1C232', from: new Date(2013, 10, 24, 9, 0, 0), to: new Date(2013, 10, 25, 15, 0, 0)}
                      ]},
                      {name: 'Content', tasks: [
                          {name: 'Supervise content creation', color: '#F1C232', from: new Date(2013, 10, 26, 9, 0, 0), to: new Date(2013, 10, 29, 16, 0, 0)}
                      ]},
                      {name: 'Documentation', tasks: [
                          {name: 'Technical/User documentation', color: '#F1C232', from: new Date(2013, 10, 26, 8, 0, 0), to: new Date(2013, 10, 28, 18, 0, 0)}
                      ]}
                  ];
          },
          getSampleTimespans: function() {
              return [
                      {
                          from: new Date(2013, 9, 21, 8, 0, 0),
                          to: new Date(2013, 9, 25, 15, 0, 0),
                          name: 'Sprint 1 Timespan'
                          //priority: undefined,
                          //classes: [],
                          //data: undefined
                      }
                  ];
          }
      };
  }]);
