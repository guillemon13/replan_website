var resourcesModule = angular.module('ReleaseResourcesApp',
		["frontendServices", "spring-security-csrf-token-interceptor", "dndLists", "ngMaterial"]);


resourcesModule.controller('MainCtrl',
		['$scope', '$filter', '$http', 'UserService', 'EmployeeService', 'ReleaseService', '$mdDialog', '$mdMedia',
    function ($scope, $filter, $http, UserService, EmployeeService, ReleaseService, $mdDialog, $mdMedia) {
      $scope.showModal = false;
      $scope.toggleModal = function(){
    	  var releaseEmployeesIds = _.chain($scope.vm.releaseEmployees)
	          .map(function (employee) {
	              return employee.id;
	          })
	          .value();

			$scope.vm.availableEmployees = _.chain($scope.vm.availableEmployees )
	          .filter(function (employee) {
	              return releaseEmployeesIds.indexOf(employee.id) == -1;
	          }).value();


			if ($scope.vm.availableEmployees.length > 0) {
				$scope.selectedEmployee = $scope.vm.availableEmployees[0];
			}

          $scope.showModal = !$scope.showModal;
      };

  	  $scope.vm = {
  			 deadline : null,
  			 originalEmployees : [],
  			 availableEmployees : [],
  			 selected : null,
  			 release : null
      };

  	  $scope.selectedEmployee = {
  			  id: -1,
  			  name : '',
  			  skills : [],
  			  effort : 0
  	  }

      updateUserInfo();

      function updateUserInfo() {
          UserService.getUserInfo()
              .then(function (userInfo) {
                  $scope.vm.userName = userInfo.userName;
                  $scope.vm.projectId = userInfo.projectId;
  								$scope.vm.projects = userInfo.projects;
									
                  loadAllResources();
                  loadReleaseResources();
              },
              function (errorMessage) {
                  showErrorMessage(errorMessage);
              });
      }

      $scope.freeEmployee = function (index) {
    	  var removedEmployee = $scope.vm.releaseEmployees.splice(index, 1);
    	  //$scope.vm.availableEmployees.push(removedEmployee);
      }

  	   function loadAllResources() {
  		   EmployeeService.searchEmployeesByProjectId($scope.vm.projectId)
  			.then(function (employees) {
  				$scope.vm.availableEmployees = employees;
  			},
  			function (errorMessage) {
  				showErrorMessage(errorMessage);
  			});
  	   }

  	   function loadReleaseResources() {
  		    $scope.id = 234;
  		    ReleaseService.getReleaseInfo($scope.id)
				.then(function (releases) {
					$scope.vm.release = releases[0];
					$scope.vm.releaseEmployees = $scope.vm.release.resources;
				},
				function (errorMessage) {
					showErrorMessage(errorMessage);
				});
  		  };

          function prepareEmployeesIds(employees) {
        	  var ids = [];

        	  angular.forEach(employees, function(employee) {
        		  this.push(employee.id);
        	  }, ids);

        	  return ids;
          }

          $scope.saveRelease = function () {

              //var selectedEmployees = prepareEmployeesDto($scope.vm.originalEmployees);
              $scope.vm.release.resources = $scope.vm.releaseEmployees;

              $scope.vm.errorMessages = [];

                // save all new items plus the ones that where modified
	          	ReleaseService.saveRelease($scope.vm.release).then(function () {
	    		    $mdDialog.show(
	  		    	      $mdDialog.alert()
	  		    	        .parent(angular.element(document.querySelector('#nrs_screen')))
	  		    	        .clickOutsideToClose(true)
	  		    	        .title('Operation completed!')
	  		    	        .textContent('You have saved the release!')
	  		    	        .ariaLabel('Alert')
	  		    	        .ok('OK')
	  		    	        .targetEvent(event)
	  		    	    );

	    		    $window.location.href = '/MainScreen_replan.html';
	          	},
	          	function (errorMessage) {
	              	showErrorMessage(errorMessage);
	          	});
          }

          function extendDeadLine (deadline) {
        	  $scope.vm.deadline.add(4, 'years');
          }

      	  $scope.showSkills = function(skills) {
        		var display = '';
        		angular.forEach(skills, function(skill) {
        		   display = display + skill.abbr + " ";
        		});

        	    return skills.length ? display : 'Not set';
      	  };

          $scope.addEmployee = function() {
        	  $scope.vm.releaseEmployees.push($scope.selectedEmployee);
          }

          $scope.logout = function () {
              UserService.logout();
          }

        }]);

resourcesModule.controller('NextReleaseCtrl',
		['$scope', '$filter', '$http', 'UserService', 'EmployeeService', 'ReleaseService', '$mdDialog', '$mdMedia',
    function ($scope, $filter, $http, UserService, EmployeeService, ReleaseService, $mdDialog, $mdMedia) {
      $scope.showModal = false;
      $scope.toggleModal = function(){
    	  var releaseEmployeesIds = _.chain($scope.vm.releaseEmployees)
	          .map(function (employee) {
	              return employee.id;
	          })
	          .value();

			$scope.vm.availableEmployees = _.chain($scope.vm.availableEmployees )
	          .filter(function (employee) {
	              return releaseEmployeesIds.indexOf(employee.id) == -1;
	          }).value();


			if ($scope.vm.availableEmployees.length > 0) {
				$scope.selectedEmployee = $scope.vm.availableEmployees[0];
			}

          $scope.showModal = !$scope.showModal;
      };

  	  $scope.vm = {
  			 deadline : null,
  			 originalEmployees : [],
  			 availableEmployees : [],
  			 selected : null,
  			 release : null
      };

  	  $scope.selectedEmployee = {
  			  id: -1,
  			  name : '',
  			  skills : [],
  			  effort : 0
  	  }

      updateUserInfo();

      function updateUserInfo() {
          UserService.getUserInfo()
              .then(function (userInfo) {
                  $scope.vm.userName = userInfo.userName;
                  $scope.vm.projectId = userInfo.projectId;

                  loadAllResources();
                  loadReleaseResources();
              },
              function (errorMessage) {
                  showErrorMessage(errorMessage);
              });
      }

      $scope.freeEmployee = function (index) {
    	  var removedEmployee = $scope.vm.releaseEmployees.splice(index, 1);
    	  //$scope.vm.availableEmployees.push(removedEmployee);
      }

  	   function loadAllResources() {
  		   EmployeeService.searchEmployeesByProjectId($scope.vm.projectId)
  			.then(function (employees) {
  				$scope.vm.availableEmployees = employees;
  			},
  			function (errorMessage) {
  				showErrorMessage(errorMessage);
  			});
  	   }

  	   function loadReleaseResources() {
  		    $scope.id = 234;
  		    ReleaseService.getReleaseInfo($scope.id)
				.then(function (releases) {
					$scope.vm.release = releases[0];
					$scope.vm.releaseEmployees = $scope.vm.release.resources;
				},
				function (errorMessage) {
					showErrorMessage(errorMessage);
				});
  		  };

          function prepareEmployeesIds(employees) {
        	  var ids = [];

        	  angular.forEach(employees, function(employee) {
        		  this.push(employee.id);
        	  }, ids);

        	  return ids;
          }

          $scope.saveRelease = function () {

              //var selectedEmployees = prepareEmployeesDto($scope.vm.originalEmployees);
              $scope.vm.release.resources = $scope.vm.releaseEmployees;

              $scope.vm.errorMessages = [];

                // save all new items plus the ones that where modified
	          	ReleaseService.saveRelease($scope.vm.release).then(function () {
	    		    $mdDialog.show(
	  		    	      $mdDialog.alert()
	  		    	        .parent(angular.element(document.querySelector('#nrs_screen')))
	  		    	        .clickOutsideToClose(true)
	  		    	        .title('Operation completed!')
	  		    	        .textContent('You have saved the release!')
	  		    	        .ariaLabel('Alert')
	  		    	        .ok('OK')
	  		    	        .targetEvent(event)
	  		    	    );
	          	},
	          	function (errorMessage) {
	              	showErrorMessage(errorMessage);
	          	});
          }

          function extendDeadLine (deadline) {
        	  $scope.vm.deadline.add(4, 'years');
          }

      	  $scope.showSkills = function(skills) {
        		var display = '';
        		angular.forEach(skills, function(skill) {
        		   display = display + skill.abbr + " ";
        		});

        	    return skills.length ? display : 'Not set';
      	  };

          $scope.addEmployee = function() {
        	  $scope.vm.releaseEmployees.push($scope.selectedEmployee)
          }

          $scope.logout = function () {
              UserService.logout();
          }

        }]);


resourcesModule.directive('modal', function () {
    return {
      template: '<div class="modal fade">' +
          '<div class="modal-dialog">' +
            '<div class="modal-content">' +
              '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title }}</h4>' +
              '</div>' +
              '<div class="modal-body" ng-transclude></div>' +
            '</div>' +
          '</div>' +
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });
