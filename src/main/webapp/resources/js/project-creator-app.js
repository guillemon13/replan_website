angular.module('projectCreatorApp', ['editableTableWidgets', 'frontendServices', 'spring-security-csrf-token-interceptor'])
    .controller('ProjectCtrl', ['$scope' , 'ProjectService', 'UserService', '$timeout',
        function ($scope, ProjectService, UserService, $timeout) {

    		$scope.vm = {
                errorMessages: [],
                infoMessages: []
            };

	        updateUserInfo();

	        function updateUserInfo() {
	            UserService.getUserInfo()
	                .then(function (userInfo) {
	                    $scope.vm.userName = userInfo.userName;
	                    $scope.vm.projectId = userInfo.projectId;
                        $scope.vm.projects = userInfo.projects;
	                },
	                function (errorMessage) {
	                    showErrorMessage(errorMessage);
	                });
	        }

            function showErrorMessage(errorMessage) {
                clearMessages();
                $scope.vm.errorMessages.push({description: errorMessage});
            }

            function clearMessages() {
                $scope.vm.errorMessages = [];
                $scope.vm.infoMessages = [];
            }

            function showInfoMessage(infoMessage) {
                $scope.vm.infoMessages = [];
                $scope.vm.infoMessages.push({description: infoMessage});
                $timeout(function () {
                    $scope.vm.infoMessages = [];
                }, 1000);
            }


            $scope.add = function () {
                $scope.vm.project.unshift({
                    name: null,
                    description: null,
                });
            };

            $scope.delete = function (deletedProjectId) {
                ProjectService.deleteProject(deletedProjectId)
                    .then(function () {
                        clearMessages();
                        showInfoMessage("deletion successful.");
                    },

                    function () {
                        clearMessages();
                        $scope.vm.errorMessages.push({description: "deletion failed."});
                    });
            };

            function prepareProjectDto() {

            	return {
                    name: $scope.vm.nameProject,
                    effort: $scope.vm.description,
                };
            }

            $scope.createProject = function () {
                var original = prepareProjectDto();

                $scope.vm.errorMessages = [];

                // save all new items plus the ones that where modified
                ProjectService.saveProject(original).then(function () {
                        showInfoMessage("Changes saved successfully");
                    },
                    function (errorMessage) {
                        showErrorMessage(errorMessage);
                    });

            };

}]);
