angular.module("ProjectApp", ["frontendServices", "spring-security-csrf-token-interceptor", "ngMaterial", "ngAnimate"])
	.controller('ProjectCtrl', ['$scope', '$http', '$mdDialog', '$mdMedia', function ($scope, $http, $mdDialog, $mdMedia) {
		
        $scope.createProject = function () {
            $scope.vm.submitted = true;

            if ($scope.form.$invalid) {
                return;
            }

            var postData = {
                name: $scope.vm.username,
                description: $scope.vm.description,
            };

            $http({
                method: 'POST',
                url: '/projects',
                data: postData,
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "text/plain"
                }
            })
            .then(function (response) {
                if (response.status == 200) {
                    $mdDialog.show(
                            $mdDialog.alert()
                              .parent(angular.element(document.querySelector('#mainScreen')))
                              .clickOutsideToClose(true)
                              .title('Project Creation OK')
                              .textContent("The project has been created")
                              .ariaLabel('Alert')
                              .ok('Continue Login')
                              .targetEvent(event)
                        );
                    $scope.login($scope.vm.userName, $scope.vm.password);
                }
                else {
                    $mdDialog.show(
                            $mdDialog.alert()
                              .parent(angular.element(document.querySelector('#mainScreen')))
                              .clickOutsideToClose(true)
                              .title('ERROR')
                              .textContent("Error at registering")
                              .ariaLabel('Alert')
                              .ok('OK')
                              .targetEvent(event)
                        );
                }
            });
        }
	}]);