var resourcesModule = angular.module('ProjectResourcesApp', ["ui.multiselect", "xeditable", "frontendServices", 
                                                             "spring-security-csrf-token-interceptor", "ui.select2", "ngMaterial", "ngAnimate"]);


resourcesModule.controller('MainCtrl',
		['$scope', '$filter', '$http', 'UserService', 'SkillService', 'EmployeeService', '$mdDialog', '$mdMedia',
    function ($scope, $filter, $http, UserService, SkillService, EmployeeService, $mdDialog, $mdMedia) {
      $scope.showModal = false;
      $scope.toggleModal = function(){
          $scope.showModal = !$scope.showModal;
      };

  	  $scope.vm = {
		  originalEmployees: [],
          employees: [],
          deletedEmployees: [],
          selected : {}
      };


		  $scope.filterComponents = function (components) {
			  var comps = _.chain(components)
					.map(function (component) {
						return {
								id: component.id,
								text: component.name,
								abbr: component.abbr,
								description: component.description
						}
				})
				.value(); 
			  
			  return comps;
			}

			$scope.multi = {
					multiple: true,
					query: function (query) {
							query.callback({ results: $scope.filterComponents($scope.vm.components) });
					},
					initSelection: function (element, callback) {
							var val = $(element).select2('val'),
								results = [];
							for (var i=0; i<val.length; i++) {
									results.push(val[i]);
							}
							callback(results);
					}
			};

  	  $scope.selectedComponent = {
  			  id: -1,
  			  name : '',
  			  abbr : '',
  			  description : ''
  	  }

      updateUserInfo();

      function updateUserInfo() {
          UserService.getUserInfo()
              .then(function (userInfo) {
                  $scope.vm.userName = userInfo.userName;
                  $scope.vm.projectId = userInfo.projectId;
				  $scope.vm.projects = userInfo.projects;

                  loadResources();
                  loadComponents();
              },
              function (errorMessage) {
                  showErrorMessage(errorMessage);
              });
      }

  	function loadResources() {
  		EmployeeService.searchEmployeesByProjectId($scope.vm.projectId)
  			.then(function (employees) {
  					$scope.vm.originalEmployees = employees;
		            $scope.vm.employees = _.chain($scope.vm.originalEmployees)
						.map(function (employee) {
								return {
										id: employee.id,
										name: employee.name,
										effort: employee.effort,
										skills: employee.skills,
										editableComponents:  $scope.filterComponents(employee.skills)
								}
						})
						.value();

						//_.cloneDeep($scope.vm.originalEmployees);
  			},
  			function (errorMessage) {
  				showErrorMessage(errorMessage);
  			});
  	};


  	function loadComponents() {
      	SkillService.getProjectSkills($scope.vm.projectId)
      		.then(function (components) {
      			$scope.vm.components = components;
      		},
      		function (errorMessage) {
      			showErrorMessage(errorMessage);
   		});
  	}

  	  $scope.showSkills = function(skills) {
  		var display = '';
  		angular.forEach(skills, function(skill) {
  		   display = display + skill.abbr + " ";
  		});
  		
  	    return (skills !== undefined && skills.length > 0) ? display : 'Not set';
  	  };

  	  $scope.checkNotEmpty = function(data) {
  	    if (data === '') {
  	      return "The employee must have a name!";
  	    }
  	  };

  	  $scope.checkAvailability = function(data) {
  		if (data < 0 || data > 100) {
  			return "The availability must be a 0-100 %";
  		}
  	  }

  	  $scope.saveEmployee = function(data, id) {
  	    //$scope.user not updated yet
  	    angular.extend(data, {id: id});

  	    //return $http.post('/saveUser', data);
  	  };

  	  // remove user
  	  $scope.removeEmployee = function(index, data) {
  	    $scope.vm.employees.splice(index, 1);
  	    if ($scope.vm.deletedEmployees.push.indexOf(data.id) != -1) {
  	    	$scope.vm.deletedEmployees.push(data.id);
  	    }
  	  };

  	// gets the template to ng-include for a table row / item
      $scope.getTemplate = function (employee) {
          if (employee.id === $scope.vm.selected.id) return 'edit';
          else return 'display';
      };

      $scope.editEmployee = function (employee) {
          $scope.vm.selected = angular.copy(employee);
      };

      $scope.saveEmployee = function (idx) {
          $scope.vm.employees[idx] = angular.copy($scope.vm.selected);
          
          $scope.vm.employees[idx].skills =  _.chain($scope.vm.selected.editableComponents)
	          .map(function (skill) {
	              return {
	                  id: skill.id,
	                  name: skill.text,
	                  abbr: skill.abbr,
	                  description: skill.description
	              }
	          })
	          .value();
          $scope.reset();
      };

      $scope.reset = function () {
          $scope.vm.selected = {};
      };
  	  
  	  // add user
  	  $scope.addEmployee = function() {
  	    $scope.inserted = {
  	      id: $scope.vm.employees.length+1,
  	      name: '',
  	      skills: null,
  	      effort: 0
  	    };
  	    $scope.vm.employees.push($scope.inserted);
  	    $scope.reset();
  	  };


          function getNotNew(employees) {
              return  _.chain(employees)
                  .filter(function (employee) {
                      return !employee.new;
                  })
                  
                  .value();
          }

          function prepareEmployeesDto(employees) {
              return  _.chain(employees)
                  .map(function (employee) {
                      return {
                          id: employee.id,
                          name: employee.name,
                          effort: employee.effort,
                          skills: employee.skills
                      }
                  })
                  .value();
          }

          $scope.cancel = function () {
        	  var confirm = $mdDialog.confirm()
		        .title('Rollback changes!')
		        .textContent('Would you like to roll back all your operations?')
		        .ariaLabel('Lucky day')
		        .targetEvent(event)
		        .ok('Do it!')
		        .cancel('Cancel');

		    	$mdDialog.show(confirm).then(function() {
		    		$window.location.reload();
		    	});
        	  
          }
          
          $scope.saveEmployees = function () {

        	  var confirm = $mdDialog.confirm()
		        .title('Confirm Employees')
		        .textContent('Would you like to confirm the changes')
		        .ariaLabel('Lucky day')
		        .targetEvent(event)
		        .ok('Do it!')
		        .cancel('Cancel');

		    	$mdDialog.show(confirm).then(function() {
		    		var maybeDirty = prepareEmployeesDto(getNotNew($scope.vm.employees));

		              var original = prepareEmployeesDto(getNotNew($scope.vm.originalEmployees));

		              var dirty = _.filter(maybeDirty).filter(function (employee) {

		                  var originalEmployee = _.filter(original, function (orig) {
		                      return orig.id === employee.id;
		                  });

		                  if (originalEmployee.length == 1) {
		                  	originalEmployee = originalEmployee[0];
		                  }

		                  return originalEmployee && ( originalEmployee.name != employee.name ||
		                  		originalEmployee.effort != employee.effort || originalEmployee.skills != employee.skills)
		              });

		              var newItems = _.filter($scope.vm.employees, function (employee) {
		                  return employee.new;
		              });

		              var saveAll = prepareEmployeesDto(newItems);
		              saveAll = saveAll.concat(dirty);

		              $scope.vm.errorMessages = [];

		              // save all new items plus the ones that where modified
		              EmployeeService.saveEmployees(saveAll).then(function () {
			            	$mdDialog.show(
						    	      $mdDialog.alert()
						    	        .parent(angular.element(document.querySelector('#mainScreen')))
						    	        .clickOutsideToClose(true)
						    	        .title('Operation Completed!')
						    	        .textContent('Changes saved successfully')
						    	        .ariaLabel('Alert')
						    	        .ok('OK')
						    	        .targetEvent(event)
				            	);  
		              },
		              function (errorMessage) {
		                  	showErrorMessage(errorMessage);
		              });
				});
          }


          $scope.saveComponent = function(component) {
	          SkillService.saveSkill(component).then(function () {
                  loadComponents();
	          },
	          	function (errorMessage) {
	              	showErrorMessage(errorMessage);
	          	});
          };

          $scope.logout = function () {
              UserService.logout();
          }
        }]);

resourcesModule.directive('modal', function () {
    return {
      template: '<div class="modal fade">' +
          '<div class="modal-dialog">' +
            '<div class="modal-content">' +
              '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title }}</h4>' +
              '</div>' +
              '<div class="modal-body" ng-transclude></div>' +
            '</div>' +
          '</div>' +
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });
