var resourcesModule = angular.module('ProjectResourcesApp', ["ui.multiselect", "xeditable", "frontendServices", "spring-security-csrf-token-interceptor", "ui.select2"]);


resourcesModule.controller('MainCtrl',
		['$scope', '$filter', '$http', 'UserService', 'SkillService', 'EmployeeService',
    function ($scope, $filter, $http, UserService, SkillService, EmployeeService) {
      $scope.showModal = false;
      $scope.toggleModal = function(){
          $scope.showModal = !$scope.showModal;
      };

  	  $scope.vm = {
		  originalEmployees: [],
          employees: [],
          deletedEmployees: []
      };


		  $scope.filterComponents = function (components) {
				return  _.chain(components)
					.map(function (component) {
							return {
									id: component.id,
									text: component.name
							}
					})
					.value();
			}

			$scope.multi = {
					multiple: true,
					query: function (query) {
							query.callback({ results: $scope.filterComponents($scope.vm.components) });
					},
					initSelection: function (element, callback) {
							var val = $(element).select2('val'),
								results = [];
							for (var i=0; i<val.length; i++) {
									results.push(val[i]);
							}
							callback(results);
					}
			};

  	  $scope.selectedComponent = {
  			  id: -1,
  			  name : '',
  			  abbr : '',
  			  description : ''
  	  }

      updateUserInfo();

      function updateUserInfo() {
          UserService.getUserInfo()
              .then(function (userInfo) {
                  $scope.vm.userName = userInfo.userName;
                  $scope.vm.projectId = userInfo.projectId;
									$scope.vm.projects = userInfo.projects;

                  loadResources();
                  loadComponents();
              },
              function (errorMessage) {
                  showErrorMessage(errorMessage);
              });
      }

      $scope.users = [
  	      {id: 1, name: 'awesome user1', status: 2, group: 4, groupName: 'admin'},
  	      {id: 2, name: 'awesome user2', status: undefined, group: 3, groupName: 'vip'},
  	      {id: 3, name: 'awesome user3', status: 2, group: null}
      ];

      $scope.statuses = [
  	    {value: 1, text: 'status1'},
  	    {value: 2, text: 'status2'},
  	    {value: 3, text: 'status3'},
  	    {value: 4, text: 'status4'}
  	];

  	$scope.groups = [
  	    {value: 1, text: 'filldeputa'}
  	];

  	$scope.loadGroups = function() {
  	    return $scope.groups.length ? null : $http.get('/groups').success(function(data) {
  	      $scope.groups = data;
  	    });
  	};

  	function loadResources() {
  		EmployeeService.searchEmployeesByProjectId($scope.vm.projectId)
  			.then(function (employees) {
  					$scope.vm.originalEmployees = employees;
		            $scope.vm.employees = _.chain($scope.vm.originalEmployees)
						.map(function (employee) {
								return {
										id: employee.id,
										name: employee.name,
										effort: employee.effort,
										skills: employee.skills,
										editableComponents:  $scope.filterComponents(employee.skills)
								}
						})
						.value();

						//_.cloneDeep($scope.vm.originalEmployees);
  			},
  			function (errorMessage) {
  				showErrorMessage(errorMessage);
  			});
  	};

  	function loadComponents() {
      	SkillService.getProjectSkills($scope.vm.projectId)
      		.then(function (components) {
      			$scope.vm.components = components;
      		},
      		function (errorMessage) {
      			showErrorMessage(errorMessage);
   		});
  	}

  	  $scope.showGroup = function(user) {
  	    if(user.group && $scope.groups.length) {
  	      var selected = $filter('filter')($scope.groups, {id: user.group});
  	      return selected.length ? selected[0].text : 'Not set';
  	    } else {
  	      return user.groupName || 'Not set';
  	    }
  	  };

  	  $scope.showSkills = function(skills) {
  		var display = '';
  		angular.forEach(skills, function(skill) {
  		   display = display + skill.abbr + " ";
  		});

  	    return skills.length ? display : 'Not set';
  	  };

  	  $scope.checkNotEmpty = function(data) {
  	    if (data === '') {
  	      return "The employee must have a name!";
  	    }
  	  };

  	  $scope.checkAvailability = function(data) {
  		if (data < 0 || data > 100) {
  			return "The availability must be a 0-100 %";
  		}
  	  }

  	  $scope.saveEmployee = function(data, id) {
  	    //$scope.user not updated yet
  	    angular.extend(data, {id: id});

  	    //return $http.post('/saveUser', data);
  	  };

  	  // remove user
  	  $scope.removeEmployee = function(index, data) {
  	    $scope.vm.employees.splice(index, 1);
  	    if ($scope.vm.deletedEmployees.push.indexOf(data.id) != -1) {
  	    	$scope.vm.deletedEmployees.push(data.id);
  	    }
  	  };

  	  // add user
  	  $scope.addEmployee = function() {
  	    $scope.inserted = {
  	      id: $scope.users.length+1,
  	      name: '',
  	      skills: null,
  	      effort: 0
  	    };
  	    $scope.vm.employees.push($scope.inserted);
  	  };


  	  /*$scope.addEmployee = function () {
  		    $scope.vm.employees.unshift({
  		        id: $scope.employees.length+1,
  		        name: '',
  		        effort: 0,
  		        skills: null,
  		        new: true
  		    });
  		};*/

          function getNotNew(employees) {
              return  _.chain(employees)
                  .filter(function (employee) {
                      return !employee.new;
                  })
                  .value();
          }

          function prepareEmployeesDto(employees) {
              return  _.chain(employees)
                  .map(function (employee) {
                      return {
                          id: employee.id,
                          name: employee.name,
                          effort: employee.effort,
                          skills: employee.skills
                      }
                  })
                  .value();
          }

          $scope.saveEmployees = function () {

              var maybeDirty = prepareEmployeesDto(getNotNew($scope.vm.employees));

              var original = prepareEmployeesDto(getNotNew($scope.vm.originalEmployees));

              var dirty = _.filter(maybeDirty).filter(function (employee) {

                  var originalEmployee = _.filter(original, function (orig) {
                      return orig.id === employee.id;
                  });

                  if (originalEmployee.length == 1) {
                  	originalEmployee = originalEmployee[0];
                  }

                  return originalEmployee && ( originalEmployee.name != employee.name ||
                  		originalEmployee.effort != employee.effort || originalEmployee.skills != employee.skills)
              });

              var newItems = _.filter($scope.vm.employees, function (employee) {
                  return employee.new;
              });

              var saveAll = prepareEmployeesDto(newItems);
              saveAll = saveAll.concat(dirty);

              $scope.vm.errorMessages = [];

              // save all new items plus the ones that where modified
              EmployeeService.saveEmployees(saveAll).then(function () {
                      showInfoMessage("Changes saved successfully");
              },
              function (errorMessage) {
                  	showErrorMessage(errorMessage);
              });

              // delete then all the employees selected in the table.
              EmployeeService.saveEmployees(saveAll).then(function () {
                  showInfoMessage("Changes saved successfully");
	          },
	          function (errorMessage) {
	              	showErrorMessage(errorMessage);
	          });

          }


          $scope.saveComponent = function(component) {
	          SkillService.saveSkill(component).then(function () {
                  loadComponents();
	          },
	          	function (errorMessage) {
	              	showErrorMessage(errorMessage);
	          	});
          };

          $scope.logout = function () {
              UserService.logout();
          }
        }]);

resourcesModule.directive('modal', function () {
    return {
      template: '<div class="modal fade">' +
          '<div class="modal-dialog">' +
            '<div class="modal-content">' +
              '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title }}</h4>' +
              '</div>' +
              '<div class="modal-body" ng-transclude></div>' +
            '</div>' +
          '</div>' +
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });
